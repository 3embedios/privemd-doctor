//
//  BookingHistoryViewController.m
//  Roadyo
//
//  Created by Rahul Sharma on 07/05/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "BookingHistoryViewController.h"
#import "CustomNavigationBar.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import <AXRatingView/AXRatingView.h>
#import "TELogger.h"

@interface BookingHistoryViewController ()<CustomNavigationBarDelegate>
@property(nonatomic,strong)AXRatingView *ratingViewStars;
@property(nonatomic,strong)IBOutlet UIView *ratingView;
@property(nonatomic,assign) float rating;
@end

@implementation BookingHistoryViewController
@synthesize passDetail;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    self.title = @"REVIEW";
    self.navigationItem.hidesBackButton = YES;
    
    self.view.backgroundColor = UIColorFromRGB(0xe9e9e8);
    
    [Helper setToLabel:passngerName Text:[NSString stringWithFormat:@"%@ %@",passDetail[@"fName"],passDetail[@"lName"]] WithFont:Robot_CondensedLight FSize:15 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:lblPickUpTitle Text:@"Pickup Location" WithFont:Robot_CondensedRegular FSize:11 Color:UIColorFromRGB(0x6e6e6e)];
    [Helper setToLabel:lblDropTitle Text:@"DropOff Loaction" WithFont:Robot_CondensedRegular FSize:11 Color:UIColorFromRGB(0x6e6e6e)];
    [Helper setToLabel:lblPickUp Text:passDetail[@"addr1"] WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:lblDropOff Text:passDetail[@"dropAddr1"] WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:LblDistancetitle Text:@"Distance" WithFont:Robot_CondensedRegular FSize:11 Color:UIColorFromRGB(0x6e6e6e)];
    [Helper setToLabel:LblDistance Text:passDetail[@"dis"] WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:lblpickUpTimeTtl Text:@"Time" WithFont:Robot_CondensedRegular FSize:11 Color:UIColorFromRGB(0x6e6e6e)];
    [Helper setToLabel:lblDropOffTimeTtl Text:@"Time" WithFont:Robot_CondensedRegular FSize:11 Color:UIColorFromRGB(0x6e6e6e)];
    [Helper setToLabel:lblTotalTimeTitle Text:@"Duration" WithFont:Robot_CondensedRegular FSize:11 Color:UIColorFromRGB(0x6e6e6e)];
   
    
    [Helper setToLabel:lblRateYourRide Text:@"RATE YOUR RIDE" WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:lbljournyTitle Text:@"JOURNEY DETAILS" WithFont:Robot_Regular FSize:15 Color:UIColorFromRGB(0x333333)];
    [Helper setButton:btnComplete Text:@"FINISHED" WithFont:Robot_Bold FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    [btnComplete setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    

    
    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForXXHDPIImage,passDetail[@"pPic"]];
    
    [imgPassnegr setImageWithURL:[NSURL URLWithString:strImageUrl]
            placeholderImage:[UIImage imageNamed:@"doctor_image_thumbnail.png"]
                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                   }];
    
    [self sendRequestForAppointmentInvoice];
    

    
    _rating = 5.0;
    
    _ratingViewStars = [[AXRatingView alloc] initWithFrame:CGRectMake(70, 0,CGRectGetWidth(_ratingView.frame), CGRectGetHeight(_ratingView.frame))];
    // _ratingViewStars.markImage = [UIImage imageNamed:@"Icon"];
    _ratingViewStars.stepInterval = 0.0;
    _ratingViewStars.highlightColor = [UIColor colorWithRed:0.044 green:0.741 blue:1.000 alpha:1.000];
    _ratingViewStars.value = _rating;
    _ratingViewStars.userInteractionEnabled = YES;
    [_ratingViewStars addTarget:self action:@selector(ratingChanged:) forControlEvents:UIControlEventValueChanged];
    //[_ratingViewStars sizeToFit];
    [_ratingView addSubview:_ratingViewStars];
    
    //[self addCustomNavigationBar];
	// Do any additional setup after loading the view.
}

- (void) addCustomNavigationBar{
    
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    //customNavigationBarView.backgroundColor = [UIColor redColor];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:@"REVIEW"];
    [customNavigationBarView setLeftBarButtonTitle:@"Back"];
    
    [self.view addSubview:customNavigationBarView];
    
    
    //--------------****-----------***********-------------*******--------------*****--------------------//
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -leftbar and righbar action

-(void)rightBarButtonClicked:(UIButton *)sender{
    
    //[self gotolistViewController];
}

-(void)leftBarButtonClicked:(UIButton *)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)buttonAction:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)buttonActionForRation:(id)sender
{
    UIButton * btn = (UIButton*)sender;
    
    if (btn.tag==100) {
        if ([btn isSelected])//yes
        {
            [btn setBackgroundImage:[UIImage imageNamed:@"summary_btn_ratingstar_on.png"] forState:UIControlStateSelected];
        }
        else{//no
            
        }
    }
    
}
- (void)ratingChanged:(AXRatingView *)sender
{
    _rating = sender.value;
    
}

#pragma mark - WebRequest
-(void)sendRequestForAppointmentInvoice {
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:@"Please wait.."];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
   
    NSString  *deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    NSDictionary * dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"PUSH"];
    NSDictionary * dictP = [dict objectForKey:@"aps"];
    NSString *docEmail = dictP[@"e"];
    NSString *appointmntDate = dictP[@"dt"];
    NSString *currentDate = [Helper getCurrentDateTime];
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_email":docEmail,
                             @"ent_user_type":@"1",
                             @"ent_appnt_dt":appointmntDate,
                             @"ent_date_time":currentDate,
                             };
    
    TELogInfo(@"kSMGetAppointmentDetialINVOICE %@",params);
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMGetAppointmentDetial
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       //TELogInfo(@"response %@",response);
                                       [self parseAppointmentDetailResponse:response];
                                   }
                               }];
}

-(void)parseAppointmentDetailResponse:(NSDictionary*)response{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil) {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
    }
    else
    {
        response  =    response[@"data"];
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            TELogInfo(@"parseAppointmentDetailResponse %@",response);
            
            
            
            [Helper setToLabel:lblAmount Text:[NSString stringWithFormat:@"$%@",response[@"fare"]] WithFont:Robot_Bold FSize:30 Color:UIColorFromRGB(0x000000)];
            
            NSInteger mint = [response[@"dur"]integerValue];
            NSInteger durations = 0;
            if (mint >= 60) {
                durations = mint / 60;
                mint = mint % 60;
            }
            [Helper setToLabel:lblTotalTime Text:[NSString stringWithFormat:@"%ldH : %ldM ",durations,mint] WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x000000)];
            
            
            //pickuptime
            NSDateFormatter *df = [self serverDateFormatter];
            NSDate *pickupTime = [df dateFromString:response[@"pickupDt"]];
            df = [self formatter];
            NSString *time = [df stringFromDate:pickupTime];
            //NSString *time = [pickupTime description];
            
            [Helper setToLabel:lblPickUpTime Text:time WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
            
            
            //pickupdate
            df = [self bookingDateFormat];
            time = [df stringFromDate:pickupTime];
            [Helper setToLabel:lblCurrentDateTime Text:time WithFont:Robot_Regular FSize:15 Color:UIColorFromRGB(0x333333)];
            
            
            df = [self serverDateFormatter];
            NSDate *dropOffTime = [df dateFromString:response[@"dropDt"]];
            df = [self formatter];
            time = [df stringFromDate:dropOffTime];
            
            
            [Helper setToLabel:lblDropOffTime Text:time WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
            
           [Helper setToLabel:LblDistance Text:passDetail[@"dis"] WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
           
           
           /*
            [Helper setToLabel:_pickupLocationLabel Text:flStrForObj(response[@"addr1"]) WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0x000000)];
            
            [Helper setToLabel:_dropLabel Text:@"DROP OFF" WithFont:Roboto_Light FSize:11 Color:UIColorFromRGB(0x969797)];
            [Helper setToLabel:_dropDateLabel Text:flStrForObj(response[@"dropDt"]) WithFont:Roboto_Light FSize:11 Color:UIColorFromRGB(0x969797)];
            _dropDateLabel.textAlignment = NSTextAlignmentLeft;
            [Helper setToLabel:_dropLocationLabel Text:flStrForObj(response[@"dropAddr1"]) WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0x000000)];
            
            [Helper setToLabel:_durationLabel Text:@"DURATION" WithFont:Roboto_Light FSize:11 Color:UIColorFromRGB(0x969797)];
            [Helper setToLabel:_totalDurationLabel Text:[NSString stringWithFormat:@"%@ hours",flStrForObj(response[@"dur"])] WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0x000000)];
            
            [Helper setToLabel:_distanceLabel Text:@"DISTANCE" WithFont:Roboto_Light FSize:11 Color:UIColorFromRGB(0x969797)];
            [Helper setToLabel:_totalDistanceLabel Text:[NSString stringWithFormat:@"%@ Miles",flStrForObj(response[@"dis"])] WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0x000000)];
            [Helper setToLabel:_ratejourneyLabel Text:@"Please rate your journey" WithFont:Roboto_Regular FSize:15 Color:UIColorFromRGB(0x000000)];*/
            
            
//            _starRatingView = [[TQStarRatingView alloc] initWithFrame:CGRectMake(0, 0,CGRectGetWidth(_ratingView.frame), CGRectGetHeight(_ratingView.frame))
//                                                         numberOfStar:5];
//            _starRatingView.delegate = self;
//            [_ratingView addSubview:_starRatingView];
        }
        else
        {
            [Helper showAlertWithTitle:@"Message" Message:@"errmsg"];
        }
        
    }
}
- (NSDateFormatter *)serverDateFormatter {
    
    //EEE - day(eg: Thu)
    //MMM - month (eg: Nov)
    // dd - date (eg 01)
    // z - timeZone
    
    //eg : @"EEE MMM dd HH:mm:ss z yyyy"
   
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    });
    return formatter;
}
- (NSDateFormatter *)formatter {
    
    //EEE - day(eg: Thu)
    //MMM - month (eg: Nov)
    // dd - date (eg 01)
    // z - timeZone
    
    //eg : @"EEE MMM dd HH:mm:ss z yyyy"
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"hh:mm a";
    });
    return formatter;
}

- (NSDateFormatter *)bookingDateFormat {
    
    //EEE - day(eg: Thu)
    //MMM - month (eg: Nov)
    // dd - date (eg 01)
    // z - timeZone
    
    //eg : @"EEE MMM dd HH:mm:ss z yyyy"
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd MMM yyyy";
    });
    return formatter;
}
@end
