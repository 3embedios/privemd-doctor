//
//  UpdateBookingStatus.m
//  Roadyo
//
//  Created by Surender Rathore on 07/08/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "UpdateBookingStatus.h"
#import "PatientPubNubWrapper.h"
#import "LocationTracker.h"
#import <GoogleMaps/GoogleMaps.h>

static UpdateBookingStatus *updateBookingStatus;

@implementation UpdateBookingStatus
{
     GMSGeocoder *geocoder_;
}
@synthesize statusUpdateTimer;
@synthesize driverState;
@synthesize dictPatientDetails;

+ (id)sharedInstance {
	if (!updateBookingStatus) {
		updateBookingStatus  = [[self alloc] init];
	}
	
	return updateBookingStatus;
}
-(void)startUpdatingStatus{
    if(![statusUpdateTimer isValid]){
        statusUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(updateToPassengerForDriverState) userInfo:nil repeats:YES];
    }
}
-(void)stopUpdatingStatus{
    if ([statusUpdateTimer isValid]) {
        [statusUpdateTimer invalidate];
    }
}
-(void)updateToPassengerForDriverState{
    
   // NSLog(@"updateToPassengerForDriverState :%d",driverState);
    
    PatientPubNubWrapper *pubNub = [PatientPubNubWrapper sharedInstance];
    
    NSString *passengerChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientPubNubChannelkey];
   // NSDictionary * dictP = [[NSUserDefaults standardUserDefaults] objectForKey:@"PUSH"];
    
    NSString *driverChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSURoadyoPubNubChannelkey];
    
    NSString *driverEmail = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDoctorEmailAddressKey];
    
    
    LocationTracker *locaitontraker = [LocationTracker sharedInstance];
    
    float latitude = locaitontraker.lastLocaiton.coordinate.latitude;
    float longitude = locaitontraker.lastLocaiton.coordinate.longitude;
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    NSString *patientLatLong  = [NSString stringWithFormat:@"%@,%@",dictPatientDetails[@"apptLat"],dictPatientDetails[@"apptLong"]];
    
    NSDictionary *message;
    //if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isBooked"]) {
        
        message =  @{@"a":[NSNumber numberWithInt:driverState],
                     @"e": driverEmail,
                     @"plt":dictPatientDetails[@"apptLat"],
                     @"lt": [NSNumber numberWithDouble:latitude],
                     @"lg": [NSNumber numberWithDouble:longitude],
                     @"plg": dictPatientDetails[@"apptLong"],
                     @"chn":driverChannel,
                     @"d":dictPatientDetails[@"apntDt"],
                     @"ph":[ud valueForKey:kNSUDoctorPhonekey],
                     @"pic":[ud valueForKey:kNSUDoctorProfilePicKey],
                     @"n":[ud valueForKey:kNSUDoctorNameKey],
                     @"ltg":patientLatLong
                     };
        
   // }
    
    [pubNub sendMessageAsDictionary:message toChannel:passengerChannel];
    
}



@end
