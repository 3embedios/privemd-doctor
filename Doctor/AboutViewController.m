//
//  AboutViewController.m
//  Roadyo
//
//  Created by Rahul Sharma on 25/06/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "AboutViewController.h"
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import "WebViewController.h"

@interface AboutViewController () <CustomNavigationBarDelegate>
{
    NSArray *links;
    BOOL isOpeningWebViewVC;
}
@end

@implementation AboutViewController
@synthesize topView;
@synthesize topLabel;
@synthesize bottomLabel;
@synthesize likeButton;
@synthesize rateButton;
@synthesize legalButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self addCustomNavigationBar];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [Helper setToLabel:topLabel Text:@"'EVERYONE'S PRIVATE DOCTOR'" WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:bottomLabel Text:@"'privemd.com'" WithFont:Robot_Regular FSize:11 Color:UIColorFromRGB(0x3399ff)];
    
    
    /*
    [Helper setButton:rateButton Text:@"RATE US IN THE APP STORE" WithFont:Robot_Regular FSize:15 TitleColor:UIColorFromRGB(0x000000) ShadowColor:nil];
    rateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    rateButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [rateButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    // [rateButton setBackgroundImage:[UIImage imageNamed:@"about_cell_selector"] forState:UIControlStateHighlighted];
    
    [Helper setButton:likeButton Text:@"LIKE US ON FACEBOOK" WithFont:Robot_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    likeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    likeButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [likeButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    // [likeButton setBackgroundImage:[UIImage imageNamed:@"about_cell_selector"] forState:UIControlStateHighlighted];
    
    [Helper setButton:legalButton Text:@"LEGAL" WithFont:Robot_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    legalButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    legalButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [legalButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    // [legalButton setBackgroundImage:[UIImage imageNamed:@"about_cell_selector"] forState:UIControlStateHighlighted];
    */
    
    
    self.navigationController.navigationBarHidden = YES;
    
    links = @[@"Like us on Facebook",@"Like us on Google+",@"Like us on Twitter",@"Rate us in the AppStore",@"Legal"];
}

- (void)viewWillDisappear:(BOOL)animated{
    if (isOpeningWebViewVC) {
        isOpeningWebViewVC = NO;
        self.navigationController.navigationBarHidden = NO;
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Custom Methods -

- (void) addCustomNavigationBar{
    
    
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    
    float deviceWidth = [UIScreen mainScreen].bounds.size.width;
    UIImageView *logoImage = [[UIImageView alloc]initWithFrame:CGRectMake(deviceWidth/2 - 123/2, 64/2 - 31/2, 123, 31)];
    logoImage.image = [UIImage imageNamed:@"home_navigationbarlogo.png"];
    //[customNavigationBarView addSubview:logoImage];

    
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:@"About"];
    [self.view addSubview:customNavigationBarView];
    
    
    
}
-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonPressedAccount];
}

- (void)menuButtonPressedAccount
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


/*
 - (IBAction)rateButtonClicked:(id)sender {
 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms://itunes.com/apps/Alpha"]];
 }
 - (IBAction)likeonFBButtonClicked:(id)sender {
 
 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/pages/Privemd/679858052097055"]];
 }
 
 - (IBAction)legalButtonClicked:(id)sender {
 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.privemd.com/privacy_policy.php"]];
 }
 */


#pragma mark UITableView Delegate -
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 3) {
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/gb/app/privemd-mobile-doctors/id931344934?mt=8"]];
    }else{
    isOpeningWebViewVC = YES;
    [self performSegueWithIdentifier:@"gotoWebView" sender:indexPath];
    }

}

#pragma mark UITableview DataSource -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return links.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==nil)
    {
        cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        
        cell.selectionStyle=UITableViewCellAccessoryNone;
        
        cell.backgroundColor=[UIColor clearColor];
        
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(10, 39.5, 300, .5)];
        img.image = [UIImage imageNamed:@"signup_bg_namedetails_line.png"];
        [cell.contentView addSubview:img];
    }

   
    
    cell.textLabel.text = links[indexPath.row];
    
       
    return cell;
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"gotoWebView"])
    {
        NSIndexPath *iPath = (NSIndexPath*)sender;
        WebViewController *webView = (WebViewController*)[segue destinationViewController];
        webView.title = links[iPath.row];
        
        if (iPath.row == 0) {
            webView.weburl = @"https://www.facebook.com/pages/Privemd/679858052097055";
          //  webView.title = @"PriveMd";
        }
        else if (iPath.row == 1) {
            webView.weburl = @"https://plus.google.com/b/117541847588704007552/104171992931774173193/about";
        }else if (iPath.row == 2){
            webView.weburl = @"https://twitter.com/PriveMD";
            
        }else if (iPath.row == 3){
              //webView.weburl = @"itms://itunes.com/apps/Alpha";
        }
        else{
            webView.weburl = @"http://www.privemd.com/privacy_policy.php";
        }
        
       webView.title = @"PriveMd";
    }
}


@end
