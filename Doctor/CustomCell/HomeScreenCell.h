//
//  HomeScreenCell.h
//  ServodoPro
//
//  Created by Rahul Sharma on 1/30/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HomeScreenCell;

@protocol HomeScreenCellDelegate <NSObject>

@optional
- (void)refreshAppointmentList:(HomeScreenCell *)cell;
- (void)phoneNumberTapped:(HomeScreenCell *)cell;

@end

@interface HomeScreenCell : UITableViewCell




@property (strong, nonatomic) IBOutlet UIImageView *customerImage;
@property (strong, nonatomic) IBOutlet UILabel *appointmentTime;
@property (strong, nonatomic) IBOutlet UILabel *customerName;
@property (strong, nonatomic) IBOutlet UILabel *customerAddress;
@property (strong, nonatomic) IBOutlet UIView *colorView;
@property (strong, nonatomic) IBOutlet UILabel *timerLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewPhoneIcon;
@property (strong, nonatomic) IBOutlet UIButton *btnPhoneTapped;
@property (strong, nonatomic) IBOutlet UILabel *labelMin;
@property (weak, nonatomic) IBOutlet UILabel *lblNoAppointments;
@property (weak, nonatomic) IBOutlet UIImageView *imgNext;


@property (weak, nonatomic) id<HomeScreenCellDelegate> delegate;
@property (assign, nonatomic) BOOL isAppointmentExpired;
@property (weak, nonatomic) IBOutlet UIImageView *circularSmallView;

@property (weak, nonatomic) IBOutlet UIView *customView;
@property (nonatomic) NSDate *startTime;
@property (strong, nonatomic) NSTimer *timer;
- (void) startTimer;
-(void) startTimerAgain;
@property int timeSec, timeMin;

- (IBAction)phoneNoTapped;

@end
