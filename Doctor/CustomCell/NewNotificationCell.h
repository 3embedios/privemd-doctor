//
//  NewNotificationCell.h
//  Roadyo
//
//  Created by Rahul Sharma on 21/07/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundedImageView.h"

@interface NewNotificationCell : UITableViewCell
@property(strong, nonatomic)RoundedImageView *profileImage;
@property(strong, nonatomic)UILabel *lblAppointmentTime;
@property(strong, nonatomic)UILabel *lblPatientName;
@property(strong, nonatomic)UILabel *lblAddress;
@property(strong, nonatomic)UILabel *lblDistance;
@property(strong, nonatomic)UIImageView *imgBaloon;
@property(strong, nonatomic)  UIActivityIndicatorView *activityIndicator;
@property(strong, nonatomic) UIImageView *imgSeprator;
@property (strong, nonatomic) UIButton *buttonAccept;
@property (strong, nonatomic) UIButton *buttonReject;

@end
