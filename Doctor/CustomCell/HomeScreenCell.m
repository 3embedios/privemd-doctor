//
//  HomeScreenCell.m
//  ServodoPro
//
//  Created by Rahul Sharma on 1/30/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import "HomeScreenCell.h"

@implementation HomeScreenCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)startTimer
{
    
    
    // create a new timer
    if (!self.timer) {
     
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTick1:) userInfo:nil repeats:YES];
        
        [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    }
    
    //[self.timer fire];
}

- (void)calculateTimer
{
    NSTimeInterval interval = [self.startTime timeIntervalSinceNow];
    interval = (-1 * interval);
    
    NSString *intervalString = [NSString stringWithFormat:@"%f", interval];
    
    self.timerLabel.text = intervalString;
}
- (void)timerTick1:(NSTimer *)timer
{
    if (self.timeSec > 0 || self.timeMin > 0) {
        if(self.timeSec==0)
        {
            self.timeMin--;
            self.timeSec=59;
        }
        else if(self.timeSec>0)
        {
            self.timeSec--;
        }
        
    }
 // NSLog(@"min:%d sec:%d",self.timeMin,self.timeSec);
    
    if (self.timeSec ==0 && self.timeMin == 0) {
        [self.timer invalidate];
        
        [self.delegate refreshAppointmentList:self];
    }
    NSString* timeNow = [NSString stringWithFormat:@"%02d",self.timeMin];
    //Display on your label
    //[timeLabel setStringValue:timeNow];
    self.timerLabel.text = timeNow;

}
-(void)startTimerAgain {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTick1:) userInfo:nil repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];

}

- (void)phoneNoTapped{
    

    [self.delegate phoneNumberTapped:self];
}

@end
