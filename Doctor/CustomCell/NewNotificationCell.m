//
//  NewNotificationCell.m
//  Roadyo
//
//  Created by Rahul Sharma on 21/07/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "NewNotificationCell.h"

@implementation NewNotificationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
       
        
        if (!self.lblAppointmentTime) {
            self.lblAppointmentTime = [[UILabel alloc]initWithFrame:CGRectMake(62, 17, 170, 15)];
            [Helper setToLabel:self.lblAppointmentTime Text:@"" WithFont:HELVETICANEUE_LIGHT FSize:10 Color:BLACK_COLOR];
            [self addSubview:self.lblAppointmentTime];
        }
        
        if (!self.lblPatientName) {
            self.lblPatientName = [[UILabel alloc]initWithFrame:CGRectMake(62,84, 170, 15)];
            [Helper setToLabel:self.lblPatientName Text:@"" WithFont:HELVETICANEUE_LIGHT FSize:10 Color:BLACK_COLOR];
            [self addSubview:self.lblPatientName];
        }
        
        if (!self.lblAddress) {
            self.lblAddress = [[UILabel alloc]initWithFrame:CGRectMake(62,108, 170, 15)];
            [Helper setToLabel:self.lblAddress Text:@"" WithFont:HELVETICANEUE_LIGHT FSize:10 Color:BLACK_COLOR];
            [self addSubview:self.lblAddress];
            
        }
        
        if (!self.lblDistance) {
            self.lblDistance = [[UILabel alloc]initWithFrame:CGRectMake(62,44,170, 15)];
            [Helper setToLabel:self.lblDistance Text:@"" WithFont:HELVETICANEUE_LIGHT FSize:10 Color:BLACK_COLOR];
            [self addSubview:self.lblDistance];
        }
        
        
        if (!self.buttonAccept) {
            self.buttonAccept = [UIButton buttonWithType:UIButtonTypeCustom];
            self.buttonAccept.frame = CGRectMake(7, 98, 50, 50);
            [self.buttonAccept setBackgroundImage:[UIImage imageNamed:@"newnotification_accept_btn@2x.png"] forState:UIControlStateNormal];
            [self addSubview:self.buttonAccept];
        }
        
        if (!self.buttonReject) {
            self.buttonReject = [UIButton buttonWithType:UIButtonTypeCustom];
            self.buttonReject.frame = CGRectMake(261, 98, 50, 50);
            [self.buttonReject setBackgroundImage:[UIImage imageNamed:@"newnotification_reject_btn@2x.png"] forState:UIControlStateNormal];
            [self addSubview:self.buttonReject];
        }
        
        self.activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(60/2-20/2, 60/2-20/2, 20, 20)];
        [self.profileImage addSubview:self.activityIndicator];
        // activityIndicator.backgroundColor=[UIColor greenColor];
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;

    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
