//
//  PendingNotificationCell.h
//  Doctor
//
//  Created by Rahul Sharma on 24/07/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundedImageView.h"

@interface PendingNotificationCell : UITableViewCell
@property(strong, nonatomic)RoundedImageView *profileImage;
@property(strong, nonatomic)UILabel *lblAppointmentTime;
@property(strong, nonatomic)UILabel *lblPatientName;
@property(strong, nonatomic)UIButton *lblPhoneNo;
@property(strong, nonatomic)UILabel *lblDistance;
@property(strong, nonatomic)UIImageView *imgBaloon;
@property(strong, nonatomic)  UIActivityIndicatorView *activityIndicator;
@property(strong, nonatomic) UIImageView *imgSeprator;
@property (strong, nonatomic) UIButton *buttonAccept;
@property (strong, nonatomic) UIButton *buttonReject;
@property(strong, nonatomic) UIImageView *distanceBaloon;
@property(strong, nonatomic) UIImageView *sepratorImage;
@property(strong, nonatomic)UILabel *lblAddress;
@property(strong, nonatomic)UILabel *notificationType;
@property(strong, nonatomic)UILabel *labelappointmentDate;



@end
