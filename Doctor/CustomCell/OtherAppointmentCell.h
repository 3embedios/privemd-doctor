//
//  OtherAppointmentCell.h
//  Doctor
//
//  Created by Guddu on 25/04/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherAppointmentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *lblPatientName;
@property (weak, nonatomic) IBOutlet UILabel *lblAppointmentTime;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;
@property (weak, nonatomic) IBOutlet UIImageView *distanceBaloon;
@property (weak, nonatomic) IBOutlet UIImageView *sepratorImage;
@property (weak, nonatomic) IBOutlet UILabel *labelNoAppointment;

@end
