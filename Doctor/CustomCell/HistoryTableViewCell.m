//
//  HistoryTableViewCell.m
//  Roadyo
//
//  Created by Rahul Sharma on 23/07/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "HistoryTableViewCell.h"

@implementation HistoryTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        if (!self.lblAppointmentDate) {
            self.lblAppointmentDate = [[UILabel alloc]initWithFrame:CGRectMake(10,22 - 7, 270, 15)];
            [Helper setToLabel:self.lblAppointmentDate Text:@"" WithFont:HELVETICANEUE_LIGHT FSize:15 Color:BLACK_COLOR];
            [self addSubview:self.lblAppointmentDate];
        }
        
      
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
