//
//  HistoryTableViewCell.h
//  Roadyo
//
//  Created by Rahul Sharma on 23/07/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableViewCell : UITableViewCell
@property(strong, nonatomic)UILabel *lblAppointmentDate;

@end
