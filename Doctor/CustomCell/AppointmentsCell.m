//
//  AppointmentsCell.m
//  Roadyo
//
//  Created by Rahul Sharma on 17/07/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "AppointmentsCell.h"
#import "RoundedImageView.h"
@implementation AppointmentsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.profileImage = [[RoundedImageView alloc]initWithFrame:CGRectMake(23, 17, 74, 74)];
        self.profileImage.image = [UIImage imageNamed:@"notificationlistview_image_boader.png"];
        [self addSubview:self.profileImage];
        
        if (!self.lblAppointmentTime) {
            self.lblAppointmentTime = [[UILabel alloc]initWithFrame:CGRectMake(118, 15, 170, 15)];
            [Helper setToLabel:self.lblAppointmentTime Text:@"" WithFont:ZURICH_LIGHTCONDENSED FSize:17 Color:UIColorFromRGB(0x006699)];
            [self addSubview:self.lblAppointmentTime];
        }
        
        if (!self.lblPatientName) {
            self.lblPatientName = [[UILabel alloc]initWithFrame:CGRectMake(118,32, 170, 17)];
             [Helper setToLabel:self.lblPatientName Text:@"" WithFont:ZURICH_LIGHTCONDENSED FSize:17 Color:UIColorFromRGB(0x333333)];
            [self addSubview:self.lblPatientName];
        }
       
        if (!self.lblPhoneNo) {
            self.lblPhoneNo = [UIButton buttonWithType:UIButtonTypeCustom];
            self.lblPhoneNo.frame = CGRectMake(118,50, 85, 15);
           // self.lblPhoneNo.backgroundColor = [UIColor redColor];
            [Helper setButton:self.lblPhoneNo Text:@"" WithFont:ZURICH_LIGHTCONDENSED FSize:16 TitleColor:UIColorFromRGB(0x006699) ShadowColor:nil];
            self.lblPhoneNo.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            //self.lblPhoneNo = [[UILabel alloc]initWithFrame:CGRectMake(118,50, 170, 15)];
            //[Helper setToLabel:self.lblPhoneNo Text:@"" WithFont:ZURICH_LIGHTCONDENSED FSize:16 Color:UIColorFromRGB(0x333333)];
            [self addSubview:self.lblPhoneNo];

        }
        
        if (!self.distanceBaloon) {
            self.distanceBaloon = [[UIImageView alloc]initWithFrame:CGRectMake(116, 81, 10, 10)];
           // self.distanceBaloon.image = [UIImage imageNamed:@"homescreenmapview_current_appointment_indicator.png"];
            [self addSubview:self.distanceBaloon];
        }
        
        if (!self.lblDistance) {
            self.lblDistance = [[UILabel alloc]initWithFrame:CGRectMake(130,80,120, 15)];
            [Helper setToLabel:self.lblDistance Text:@"" WithFont:ZURICH_LIGHTCONDENSED FSize:10 Color:UIColorFromRGB(0x333333)];
            [self addSubview:self.lblDistance];
        }
       
        if (!self.lblAddress) {
            self.lblAddress = [[UILabel alloc]initWithFrame:CGRectMake(118,65,170, 15)];
            [Helper setToLabel:self.lblAddress Text:@"" WithFont:ZURICH_LIGHTCONDENSED FSize:14 Color:UIColorFromRGB(0x333333)];
            [self addSubview:self.lblAddress];
        }

        
        if (!self.sepratorImage) {
            self.sepratorImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 119, 300, 1)];
            [self addSubview:self.sepratorImage];
        }
        if (!self.buttonAccept) {
            self.buttonAccept = [UIButton buttonWithType:UIButtonTypeCustom];
            self.buttonAccept.frame = CGRectMake(225, 15, 40, 40);
            [self.buttonAccept setBackgroundImage:[UIImage imageNamed:@"notificationlistview_accept_btn@2x.png"] forState:UIControlStateNormal];
            //[self addSubview:self.buttonAccept];
        }
        
        if (!self.buttonReject) {
            self.buttonReject = [UIButton buttonWithType:UIButtonTypeCustom];
            self.buttonReject.frame = CGRectMake(self.buttonAccept.frame.origin.x+35, 15, 40, 40);
            [self.buttonReject setBackgroundImage:[UIImage imageNamed:@"notificationlistview_reject_btn@2x.png"] forState:UIControlStateNormal];
            //[self addSubview:self.buttonReject];
        }
        
        if (!self.lblStatus) {
            self.lblStatus = [[UILabel alloc]initWithFrame:CGRectMake(118,98,150, 15)];
            [Helper setToLabel:self.lblStatus Text:@"" WithFont:Robot_Bold FSize:12 Color:UIColorFromRGB(0x000000)];
            [self addSubview:self.lblStatus];
        }

        
        if (!self.lblAmount) {
            self.lblAmount = [[UILabel alloc]initWithFrame:CGRectMake(40,98,120, 15)];
            [Helper setToLabel:self.lblAmount Text:@"" WithFont:Robot_Bold FSize:12 Color:UIColorFromRGB(0x000000)];
            [self addSubview:self.lblAmount];
        }
        
    
        
        
        self.activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(60/2-20/2, 60/2-20/2, 20, 20)];
        [self.profileImage addSubview:self.activityIndicator];
        // activityIndicator.backgroundColor=[UIColor greenColor];
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        
        // self.imgSeprator = [[UIImageView alloc]initWithFrame:CGRectMake(0, 119, 320, <#CGFloat height#>)];
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
   
}

@end
