//
//  PendingNotificationDetailController.m
//  Doctor
//
//  Created by Rahul Sharma on 23/07/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "PendingNotificationDetailController.h"
#import "AppointmentsCell.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "RoundedImageView.h"
#import <CoreLocation/CoreLocation.h>
#import "DirectionService.h"
#import <MessageUI/MessageUI.h>
#import "TELogger.h"
#import "OtherAppointmentCell.h"


#define mapZoomLevel 17
#define NO_APPOINTMENT_IMAGE 400

@interface PendingNotificationDetailController ()<CLLocationManagerDelegate,UIGestureRecognizerDelegate,MFMessageComposeViewControllerDelegate>
{
    GMSMapView *mapView_;
    NSMutableArray *waypoints_;
    NSMutableArray *waypointStrings_;
    GMSGeocoder *geocoder_;
    UIView *customMapView;
    
    NSString *selectedPhoneNO;
    
    NSString *appointmentActionKey;
}

@property(nonatomic,assign) float currentLatitude;
@property(nonatomic,assign) float currentLongitude;
@property(nonatomic,assign)BOOL isUpdatedLocation;
@property(nonatomic,strong)NSString *startLocation;
@property(nonatomic,strong)NSString *destinationLocation;
@property(nonatomic,assign)CLLocationCoordinate2D destinationCoordinates;
@property(nonatomic,assign)CLLocationCoordinate2D pickupCoordinates;
@property(strong,nonatomic) IBOutlet UIView *customMapView;
@property(nonatomic,strong)GMSMarker *destinationMarker;
//@property(nonatomic,assign) float currentLatitude;
//@property(nonatomic,assign) float currentLongitude;
//@property(nonatomic,assign) BOOL isUpdatedLocation;
@property(nonatomic,assign)CLLocationCoordinate2D previouCoord;
@property(nonatomic,strong)NSMutableDictionary *allMarkers;
@property(nonatomic,strong)CLLocationManager *locationManager;
@property(nonatomic,assign)BOOL isPathPlotted;
@property(nonatomic,assign)BOOL isDriverArrived;

@property(nonatomic,strong) GMSMarker *currentLocationMarker;

@end

@implementation PendingNotificationDetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark Webservice Request -
- (void)sendServicegetPatientAppointment
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
    NSArray *arrayAppointmentDate = [self.dictAppointmentDetails[@"apntDt"] componentsSeparatedByString:@" "];
    NSString *appointmentDate;
    if (arrayAppointmentDate.count > 0) {
        appointmentDate = arrayAppointmentDate[0];
    }
    
    NSDictionary *parameters = @{kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId:[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 @"ent_appnt_dt":appointmentDate,
                                 @"ent_date_time":[Helper getCurrentDateTime]};
    
    
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient postPath:MethodAppointments parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getPatientAppointmentResponse:operation.responseString.JSONValue];
        // NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        //NSLog(@"Request Successful, response '%@'", responseStr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [Helper showAlertWithTitle:@"Error!" Message:error.localizedDescription];
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
    }];
    
}

-(void)getPatientAppointmentResponse:(NSDictionary *)response
{
    [self.view.subviews setValue:@NO forKeyPath:@"hidden"];
    NSLog(@"response:%@",response);
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            
            self.appointmentsArr = [dictResponse objectForKey:@"appointments"];
            
            float height =  self.lblPatientMessage.frame.size.height + self.lblPatientMessage.frame.origin.y;
            
            CGRect rectBottom = self.bottomView.frame;
            rectBottom.size.height = height + [self.appointmentsArr count] * 110 + 20;
            self.bottomView.frame = rectBottom;
            
            CGRect rect = self.tblView.frame;
            rect.origin.y = height ;
            rect.size.height = [self.appointmentsArr count] * 110 + 10;
            self.tblView.frame = rect;
            
            CGRect rectScreen = [[UIScreen mainScreen] bounds];
            self.scrollView.contentSize = CGSizeMake(rectScreen.size.width, self.tblView.frame.size.height + self.tblView.frame.origin.y + 110);
            
            self.tblView.hidden = NO;
            [self.tblView reloadData];
            
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
        }
        else
        {
            //self.appointmentsArr = [[dictResponse objectForKey:@"appointments"] mutableCopy];
            self.tblView.hidden = YES;
            
            UIImageView *imgVW = [[UIImageView alloc]initWithFrame:CGRectMake(320/2-100, self.bottomView.frame.origin.y + 50, 200, 200)];
            imgVW.image = [UIImage imageNamed:@"no_appointment_logo.png"];
            imgVW.tag = NO_APPOINTMENT_IMAGE;
            [self.view addSubview:imgVW];
            
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
            //[Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
            
        }
    }
    
}

- (void)getCurrentAppointmentDetail:(NSString *)appointmentDate patientEmail:(NSString *)email{
    
    TELogInfo(@"getAppointmentDetails");
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
    
    NSDictionary *parameters = @{kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId:[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 @"ent_appnt_dt":self.appointmentTime,
                                 @"ent_email":self.patientEmail,
                                 @"ent_date_time":[Helper getCurrentDateTime]};
    
    TELogInfo(@"getAppointmentDetails Parameters :%@", parameters);
    
    
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient postPath:MethodGetAppointmentDetails parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getCurentPatientDetailsResponse:operation.responseString.JSONValue];
        
        //NSLog(@"Request Successful, response '%@'", responseStr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [Helper showAlertWithTitle:@"Error!" Message:error.localizedDescription];
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
    }];
    
    
    
    
}

- (void)getCurentPatientDetailsResponse:(NSDictionary *)response
{
    
    NSLog(@"getCurentPatientDetailsResponse:%@",response);
    
    [self.view.subviews setValue:@NO forKeyPath:@"hidden"];
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            
            self.dictAppointmentDetails = response;
            [self setupUI];
            
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
            
        }
        else
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
    
}


- (void)requestAcceptRejectBooking:(BOOL)isAccept details:(NSDictionary *)dict{
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
    NSString *appointmentAction;
    if (isAccept) {
        appointmentAction = @"2";
        appointmentActionKey = @"8";
    }else{
        appointmentAction = @"3";
           appointmentActionKey = @"9";
    }
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                                [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                                dict[@"email"],kSMPRespondPassengerEmail,
                                dict[@"apntDt"], kSMPRespondBookingDateTime,
                                appointmentAction,kSMPRespondResponse,
                                dict[@"bookType"],kSMPRespondBookingType,
                                [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    
    
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient postPath:MethodRespondToAppointMent parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self acceptRejectResponse:operation.responseString.JSONValue];
        
        //NSLog(@"Request Successful, response '%@'", responseStr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [Helper showAlertWithTitle:@"Error!" Message:error.localizedDescription];
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
    }];
    
    
}

#pragma AcceptReject Response -
- (void)acceptRejectResponse:(NSDictionary *)response
{
    
    NSLog(@"response:%@",response);
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            NSString *patientName = [[NSUserDefaults standardUserDefaults] valueForKey:kNSUDoctorNameKey] ;
                       
            /*Send message using PubNub */
            
            NSDictionary *dict = @{@"a":appointmentActionKey,
                                   @"e":self.dictAppointmentDetails[@"email"],
                                   @"d":self.dictAppointmentDetails[@"apntDt"],
                                   @"n":patientName
                                   };
            [self sendPubNubMessage:dict];
            
            //----------****end****--------****end****--------****end****---------------
            
            [self.navigationController popViewControllerAnimated:YES];
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
        }
        else
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
    
}

#pragma mark PUBNUB -

- (void)sendPubNubMessage:(NSDictionary *)message{
    
    NSString *patientChannel = self.dictAppointmentDetails[kNSUPatientPubNubChannelkey] ;//[[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientPubNubChannelkey];
    
     NSLog(@"pubnub Dict %@: channel :%@",message,patientChannel);
     PatientPubNubWrapper *pubNub = [PatientPubNubWrapper sharedInstance];
     [pubNub sendMessageAsDictionary:message toChannel:patientChannel];
    
}

#pragma mark UIView Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Appointment";
    
    [self.view.subviews setValue:@YES forKeyPath:@"hidden"];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:37.778376 longitude:-122.409853 zoom:mapZoomLevel];
    
    
    customMapView.backgroundColor = [UIColor clearColor];
    CGRect rectMap = CGRectMake(0, self.view.bounds.size.height , 320, self.view.bounds.size.height);
    mapView_ = [GMSMapView mapWithFrame:rectMap camera:camera];
    mapView_.delegate = self;
    mapView_.myLocationEnabled = YES;
    mapView_.settings.myLocationButton = YES;
    mapView_.settings.compassButton = YES;
    customMapView = mapView_;
    [self.view addSubview:customMapView];
    
    
    waypoints_ = [[NSMutableArray alloc]init];
    waypointStrings_ = [[NSMutableArray alloc]initWithCapacity:2];
    
    
    CGRect rect = self.tblView.frame;
    rect.size.height = self.bottomView.frame.origin.y ;
    self.tblView.frame = rect;
    
    
    
    [self addArrowButtonToMapview];
    [self addCustomNavRightButtons];
    
    
    [Helper setButton:self.btnReject Text:@"CANCEL" WithFont:LATO_BOLD FSize:18 TitleColor:WHITE_COLOR ShadowColor:nil];
    [Helper setButton:self.btnAccept Text:@"ACCEPT" WithFont:LATO_BOLD FSize:18 TitleColor:WHITE_COLOR ShadowColor:nil];
    
    /* get patient details if open from Push */
    
    if (self.isOpenFromPush) {
        
        [self getCurrentAppointmentDetail:self.appointmentTime patientEmail:self.patientEmail];
        
    }else{
        [self setupUI];
        
    }
    
    PriveMdAppDelegate *delegate = (PriveMdAppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate customizeNavigationLeftButton:self];
    
    [self getCurrentLocation];
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    
    
    self.navigationController.navigationBarHidden = NO;
    
    [mapView_ addObserver:self forKeyPath:@"myLocation" options:NSKeyValueObservingOptionNew context:NULL];
    
    mapView_.myLocationEnabled = YES;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newBookingCame:) name:@"NewBooking" object:nil];
    
}

- (void) viewDidAppear:(BOOL)animated{
    
    // [self addgestureToMap];
    
    if (IS_SIMULATOR)
    {
        _currentLatitude = 13.028866;
        _currentLongitude = 77.589760;
        
        
        CLLocationCoordinate2D locationCord = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
        mapView_.camera = [GMSCameraPosition cameraWithTarget:locationCord
                                                         zoom:14];
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
        marker.title = @"It's Me";
        marker.snippet = @"Current Location";
        marker.map = mapView_;
        
        //subscribe to pubnub to get stream of doctors location
        //[self startPubNubStream];
        
    }
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
    tapGesture.delegate = self;
    tapGesture.numberOfTapsRequired = 1;
    [self.lblPhone addGestureRecognizer:tapGesture];
    
    self.lblPhone.userInteractionEnabled = YES;
    
    CGRect rect = [[UIScreen mainScreen] bounds];
    
    self.scrollView.contentSize = CGSizeMake(rect.size.width, self.tblView.contentSize.height);
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [mapView_ removeObserver:self forKeyPath:@"myLocation"];
    self.navigationController.navigationBarHidden = YES;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewBooking" object:nil];
}

#pragma mark Handle TapGesture -
- (void)handleTapGesture:(UIGestureRecognizer *)gesture{
    
    // NSDictionary *dictAppointment = self.lblPhone.text;
    
    selectedPhoneNO = self.lblPhone.text;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Call" message:@"Do you want to make a call on this number?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
    alert.tag = 300;
    [alert show];
    
}


#pragma mark - CLLocationManager Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    
    if (!_isUpdatedLocation) {
        
        [_locationManager stopUpdatingLocation];
        
        //change flag that we have updated location once and we don't need it again
        _isUpdatedLocation = YES;
        
        [self setStartLocationCoordinates:newLocation.coordinate.latitude Longitude:newLocation.coordinate.longitude];
        
        
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"locationManager failed to update location : %@",[error localizedDescription]);
}


#pragma mark - Map direction

-(void)getCurrentLocation{
    
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled]) {
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            
            [_locationManager requestAlwaysAuthorization];
        }
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [_locationManager startUpdatingLocation];
        
    }
    else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Service" message:@"Unable to find your location,Please enable location services." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
}


/**
 *  adds direction on Map
 *
 *  @param json representaion of pathpoints
 */
- (void)addDirections:(NSDictionary *)json {
    
    if ([json[@"routes"] count]>0) {
        NSDictionary *routes = [json objectForKey:@"routes"][0];
        
        NSDictionary *route = [routes objectForKey:@"overview_polyline"];
        NSString *overview_route = [route objectForKey:@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        polyline.map = mapView_;
    }
    
    
}

/**
 *  Sets the start location of Path
 *
 *  @param latitude  start point latitude
 *  @param longitude start point longitude
 */
-(void)setStartLocationCoordinates:(float)latitude Longitude:(float)longitude{
    //change map camera postion to current location
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:15];
    [mapView_ setCamera:camera];
    
    
    //add marker at current location
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    marker.map = mapView_;
    marker.icon = [UIImage imageNamed:@"map_icn_location.png"];
    [waypoints_ addObject:marker];
    
    
    //save current location to plot direciton on map
    _startLocation = [NSString stringWithFormat:@"%.6f,%.6f",latitude, longitude];
    [waypointStrings_ insertObject:_startLocation atIndex:0];
    
    
    if (_isDriverArrived) {
        
        //[self changeDropAddressOnUI];
        [waypoints_ removeLastObject];
        [waypointStrings_ removeLastObject];
        _isPathPlotted = NO;
        
        
        
        
        
        
        //[self updateDestinationLocationWithLatitude:[self.dictAppointmentDetails[@"apptLat"] doubleValue]Longitude:[self.dictAppointmentDetails[@"apptLong"] doubleValue]];
    }
    else {
        
        
        
        
        
        // [self updateDestinationLocationWithLatitude:[self.dictAppointmentDetails[@"apptLat"] doubleValue]Longitude:[self.dictAppointmentDetails[@"apptLong"] doubleValue]];
    }
    
    
    
    
}

/**
 * sets destination point of path
 *
 *  @param latitude  destination point latitude
 *  @param longitude destination point longitude
 */
-(void)updateDestinationLocationWithLatitude:(float)latitude Longitude:(float)longitude{
    
    
    if (!_isPathPlotted) {
        
        
        
        NSLog(@"pathplotted");
        _isPathPlotted = YES;
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(
                                                                     latitude,
                                                                     longitude);
        
        _previouCoord = position;
        
        _destinationMarker = [GMSMarker markerWithPosition:position];
        _destinationMarker.map = mapView_;
        _destinationMarker.flat = YES;
        //_destinationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        //_destinationMarker.icon = [UIImage imageNamed:@"car.png"];
        
        [waypoints_ addObject:_destinationMarker];
        NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",
                                    latitude,longitude];
        
        [waypointStrings_ addObject:positionString];
        
        if([waypoints_ count]>1){
            NSString *sensor = @"false";
            NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                                   nil];
            NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
            NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                              forKeys:keys];
            DirectionService *mds=[[DirectionService alloc] init];
            SEL selector = @selector(addDirections:);
            [mds setDirectionsQuery:query
                       withSelector:selector
                       withDelegate:self];
        }
        
    }
    else {
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        CLLocationDirection heading = GMSGeometryHeading(_previouCoord, position);
        _previouCoord = position;
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        _destinationMarker.position = position;
        [CATransaction commit];
        if (_destinationMarker.flat) {
            _destinationMarker.rotation = heading;
        }
        
    }
    
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark- GMSMapviewDelegate

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    CGRect frame = customMapView.frame;
    if (frame.origin.y == 500 || frame.origin.y == 412) {
        frame.origin.y = self.bottomView.frame.origin.y + 3;
        frame.origin.x = 0;
        
        UIButton *btn = (UIButton *)[customMapView viewWithTag:100];
        btn.tag = 200;
        [UIView animateWithDuration:0.4
                              delay:0.1
                            options: UIViewAnimationOptionCurveEaseIn
                         animations:^
         {
             
             UIImageView *img= (UIImageView *)[self.view viewWithTag:NO_APPOINTMENT_IMAGE];
             if (img !=nil) {
                 
                 img.hidden = YES;
             }
             customMapView.frame = frame;
             
         }
                         completion:^(BOOL finished)
         {
             
             /*clear mapview with old marker */
             [mapView clear];
             /* add marker on map */
             [self customMarkers];
             
             NSLog(@"Completed");
             //[acceptRejectBtnView setHidden:YES];
             UIButton *btn = (UIButton *)[customMapView viewWithTag:200];
             [btn setHidden:NO];
             
             
             
             
             
             
         }];
        
    }
    
}
- (void) mapView:(GMSMapView *) mapView willMove:(BOOL)gesture{
    
    NSLog(@"willMove");
    
}

- (void) mapView:(GMSMapView *) mapView didChangeCameraPosition:(GMSCameraPosition *) position{
    
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
    NSLog(@"idleAtCameraPosition");
    
}
- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    
    //[self performSegueWithIdentifier:@"doctorDetails" sender:marker];
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    
    NSLog(@"marker userData %@",marker.userData);
    
    
    return nil;
}
- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker{
    NSLog(@"didBeginDraggingMarker");
}
- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    NSLog(@"didEndDraggingMarker");
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    if (!_isUpdatedLocation) {
        // If the first location update has not yet been recieved, then jump to that
        // location.
        // _isUpdatedLocation = YES;
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                         zoom:14];
    }
    else {
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        _currentLocationMarker.position = position;
    }
    
    
    
    
}


#pragma mark UIButton Action -

- (void)setupUI{
    
    [Helper setToLabel:self.lblAppointTime Text:self.dictAppointmentDetails[@"apntTime"] WithFont:ZURICH_LIGHTCONDENSED FSize:17 Color:UIColorFromRGB(0x006699)];
    [Helper setToLabel:self.lblPhone Text:self.dictAppointmentDetails[@"phone"] WithFont:ZURICH_LIGHTCONDENSED FSize:17 Color:UIColorFromRGB(0x006699)]; //0xBDBDBD
    
    // NSString *fullAddrss = [NSString stringWithFormat:@"%@ %@",self.dictAppointmentDetails[@"addrLine1"],self.dictAppointmentDetails[@"addrLine2"]];
    
    [Helper setToLabel:self.lblAdress Text:self.dictAppointmentDetails[@"addrLine2"] WithFont:ZURICH_LIGHTCONDENSED FSize:17 Color:UIColorFromRGB(0x333333)];
    
    [Helper setToLabel:self.labelAppointmentDate Text:self.dictAppointmentDetails[@"apntDate"] WithFont:ZURICH_LIGHTCONDENSED FSize:17 Color:UIColorFromRGB(0x333333)];
    if (self.isOpenFromPush) {
        [Helper setToLabel:self.lblName Text:self.dictAppointmentDetails[@"fName"] WithFont:ZURICH_LIGHTCONDENSED FSize:17 Color:UIColorFromRGB(0x333333)];
    }else{
        
        [Helper setToLabel:self.lblName Text:self.dictAppointmentDetails[@"fname"] WithFont:ZURICH_LIGHTCONDENSED FSize:17 Color:UIColorFromRGB(0x333333)];
    }
    
    self.lblDetailAddress.text = [NSString stringWithFormat:@"%@%@",self.dictAppointmentDetails[@"addrLine1"],self.dictAppointmentDetails[@"addrLine2"]];
    
    [self adjustTopAndBottomViewFrame:self.lblDetailAddress];
    
    if (self.dictAppointmentDetails[@"notes"] != nil && [self.dictAppointmentDetails[@"notes"] length] > 0) {
        // [Helper setToLabel:self.labelNotes Text:self.dictAppointmentDetails[@"notes"] WithFont:ZURICH_LIGHTCONDENSED FSize:17 Color:UIColorFromRGB(0x333333)];
        self.labelNotes.text = self.dictAppointmentDetails[@"notes"];
    }
    
    
    NSString *imageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForThumbnailImage,self.dictAppointmentDetails[@"pPic"]] ;
    [self.profileImg setImageWithURL:[NSURL URLWithString:imageUrl]
                    placeholderImage:[UIImage imageNamed:@"notificationlistview_image_boader@2x.png"]
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                               
                               [self setProfileImage:image];
                           }];
    
    
    
    
    if ([self.dictAppointmentDetails[@"bookType"] intValue] == 1) {
        
        [Helper setToLabel:self.lblAppointTime Text:self.dictAppointmentDetails[@"apntTime"] WithFont:ZURICH_LIGHTCONDENSED FSize:17 Color:UIColorFromRGB(0x006699)];
        
    }else{
        
        [Helper setToLabel:self.lblAppointTime Text:[NSString stringWithFormat:@"%@ %@",self.dictAppointmentDetails[@"apntTime"],self.dictAppointmentDetails[@"apntDate"]] WithFont:ZURICH_LIGHTCONDENSED FSize:20 Color:UIColorFromRGB(0x006699)];
        
        
    }
    
    
    [self sendServicegetPatientAppointment];
    
    
}

- (void)adjustTopAndBottomViewFrame:(UILabel *)addressLabel{
    
    addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(310, 9999);
    CGSize expectSize = [addressLabel sizeThatFits:maximumLabelSize];
    
    CGRect rect = self.lblDetailAddress.frame;
    rect.size.height = expectSize.height;
    self.lblDetailAddress.frame = rect;
    
    rect = self.lblPatientMessage.frame;
    rect.origin.y = self.lblDetailAddress.frame.size.height + self.lblDetailAddress.frame.origin.y + 10;
    self.lblPatientMessage.frame = rect;
    
    
    rect = self.tblView.frame;
    rect.origin.y = self.lblPatientMessage.frame.size.height + self.lblPatientMessage.frame.origin.y;
    // rect.size.height = [self.appointmentsArr count] * 110;
    self.tblView.frame = rect;
    
    //rect = self.bottomView.frame;
    //rect.origin.y = self.topView.frame.origin.y + self.topView.frame.size.height;
    //self.bottomView.frame = rect;
    
    
    
}

- (void)addArrowButtonToMapview{
    
    UIButton *buttonArrow = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonArrow.frame = CGRectMake(320/2-15,0, 30, 30);
    buttonArrow.tag =  100;
    [buttonArrow setImage:[UIImage imageNamed:@"homescreenmapview_down_arrow_off.png"] forState:UIControlStateNormal];
    [buttonArrow setImage:[UIImage imageNamed:@"homescreenmapview_down_arrow_on.png"] forState:UIControlStateHighlighted];
    [buttonArrow addTarget:self action:@selector(moveMapViewUPAndDOWN:) forControlEvents:UIControlEventTouchUpInside];
    [buttonArrow setHidden:YES];
    [customMapView addSubview:buttonArrow];
}
- (void)moveMapViewUPAndDOWN:(UIButton *)sender{
    
    sender.enabled = NO;
    CGRect frame = customMapView.frame;
    if (sender.tag == 100) {
        sender.tag = 200;
        frame.origin.y = 64;
        frame.origin.x = 0;
        
        frame.size.height = self.view.bounds.size.height - 64;
        
        /*clear mapview with old marker */
        [mapView_ clear];
        /* add marker on map */
        [self customMarkers];
        
    }else{
        sender.tag = 100;
        frame.origin.y = self.view.bounds.size.height ;
    }
    
    
    
    
    [UIView animateWithDuration:0.4
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^
     {
         
         
         customMapView.frame = frame;
         
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         sender.enabled = YES;
         
         UIImageView *img= (UIImageView *)[self.view viewWithTag:NO_APPOINTMENT_IMAGE];
         if (img !=nil) {
             
             img.hidden = NO;
         }
         
         if (sender.tag == 200) {
             //   sender.hidden = NO;
             
             [sender setBackgroundImage:[UIImage imageNamed:@"cross_icon_off.png"] forState:UIControlStateNormal];
             [sender setBackgroundImage:[UIImage imageNamed:@"cross_icon_on.png"] forState:UIControlStateHighlighted];
             
             
             
         }else{
             //  sender.hidden = YES;
             [sender setBackgroundImage:[UIImage imageNamed:@"doctor_location_pin_icon_off.png"] forState:UIControlStateNormal];
             [sender setBackgroundImage:[UIImage imageNamed:@"doctor_location_pin_icon_on.png"] forState:UIControlStateHighlighted];
             
         }
         
     }];
}

- (void)addCustomNavRightButtons{
    
    
    UIButton *buttonMap = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonMap.frame = CGRectMake(0, 0, 40, 40);
    buttonMap.tag = 100;
    [buttonMap addTarget:self action:@selector(moveMapViewUPAndDOWN:) forControlEvents:UIControlEventTouchUpInside];
    [buttonMap setBackgroundImage:[UIImage imageNamed:@"doctor_location_pin_icon_off.png"] forState:UIControlStateNormal];
    [buttonMap setBackgroundImage:[UIImage imageNamed:@"doctor_location_pin_icon_on.png"] forState:UIControlStateHighlighted];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonMap];
    /*
     UIButton *buttonReject = [UIButton buttonWithType:UIButtonTypeCustom];
     buttonReject.frame = CGRectMake(0, 0, 40, 40);
     [buttonReject addTarget:self action:@selector(rejectAction:) forControlEvents:UIControlEventTouchUpInside];
     [buttonReject setBackgroundImage:[UIImage imageNamed:@"notificationlistview_reject_btn@2x.png"] forState:UIControlStateNormal];
     
     UIBarButtonItem *barButtonAccept = [[UIBarButtonItem alloc]initWithCustomView:buttonAccept];
     UIBarButtonItem *barButtonReject = [[UIBarButtonItem alloc]initWithCustomView:buttonReject];
     
     NSArray *buttons = @[barButtonReject,barButtonAccept];
     self.navigationItem.rightBarButtonItems = buttons;
     */
    
    
    
}

- (IBAction)acceptAction:(UIButton *)sender{
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Appointment!" message:@"Are you sure you want to accept this appointment?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alertView.tag = 600;
    [alertView show];
    
    //[self requestAcceptRejectBooking:YES details:self.dictAppointmentDetails];
}

- (IBAction)rejectAction:(UIButton *)sender{
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Appointment!" message:@"Are you sure you want to reject this appointment?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alertView.tag = 700;
    [alertView show];
    
    // [self requestAcceptRejectBooking:NO details:self.dictAppointmentDetails];
}
- (void)setProfileImage:(UIImage *)img{
    self.profileImg.image = img;
}

- (void)buttonBackTapped{
    NSLog(@"button back taped");
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)newBookingCame:(NSNotification*)notification{
    NSLog(@"userinfo : %@",notification.userInfo);
    
    NSDictionary *dict = notification.userInfo;
    self.appointmentTime = dict[@"aps"][@"dt"];
    self.patientEmail =  dict[@"aps"][@"e"];
    
    [self getCurrentAppointmentDetail:self.appointmentTime patientEmail:self.patientEmail];
    
    
    
}


- (IBAction)buttonPhoneTapped:(UIButton *)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Call" message:@"Do you want to make a call on this number?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
    alert.tag = 500;
    [alert show];
    
}

- (IBAction)buttonMessageTapped:(UIButton *)sender {
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    // NSString *test = @"Test";
    
    NSArray *recipents = @[self.dictAppointmentDetails[@"phone"]];
    NSString *message = @""; //[NSString stringWithFormat:@"Just sent the %@ file to your email. Please check!", test];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
    
}



#pragma mark UIButton Action -

/*
 - (void)customMarkers{
 
 NSArray *arrayAppointments = self.appointmentsArr[0];
 
 int markerCount = 0;
 for (int i = 0; i <= arrayAppointments.count; i++) {
 
 
 
 markerCount++;
 
 UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,60,60)];
 UIImageView *pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homescreenmapview_next_appointment_indicator.png"]];
 
 UILabel *label;
 
 if (markerCount > 9) {
 label = [[UILabel alloc] initWithFrame:CGRectMake(5, 1, 60, 18)];
 label.font = [UIFont fontWithName:HELVETICANEUE_LIGHT size:12];
 }else{
 label = [[UILabel alloc] initWithFrame:CGRectMake(8, 1, 60, 18)];
 label.font = [UIFont fontWithName:HELVETICANEUE_LIGHT size:14];
 }
 //label.textAlignment = NSTextAlignmentCenter;
 label.text = [NSString stringWithFormat:@"%d",markerCount];
 
 
 [view addSubview:pinImageView];
 [view addSubview:label];
 //i.e. customize view to get what you need
 
 UIImage *markerIcon = [self imageFromView:view];
 NSDictionary *dict;
 
 if (i == arrayAppointments.count) {
 
 dict = self.dictAppointmentDetails;
 
 }else{
 dict = arrayAppointments[i];
 }
 
 
 float latitude = [dict[@"apptLat"] floatValue];
 float longitude = [dict[@"apptLong"] floatValue];
 
 
 
 
 GMSMarker *marker = [[GMSMarker alloc] init];
 marker.position = CLLocationCoordinate2DMake(latitude, longitude);
 marker.tappable = YES;
 marker.map = mapView_;
 marker.title = dict[@"apntTime"];
 marker.snippet = dict[@"addrLine2"];
 marker.icon = markerIcon;
 
 
 // mapView_.selectedMarker = marker;
 
 }
 
 
 }
 */


- (void)customMarkers{
    NSArray *arrayAppointments = self.appointmentsArr;
    
    int markerCount = 0;
    for (int i = 0; i <= arrayAppointments.count; i++) {
        
        markerCount++;
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,62,77)];
        UIImageView *pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"circular_map_icon.png"]];
        
        
        
        
        
        //[label sizeToFit];
        
        //  [view addSubview:pinImageView];
        //[view addSubview:label];
        
        NSDictionary *dict;
        
        if (i == arrayAppointments.count) {
            
            dict = self.dictAppointmentDetails;
            
        }else{
            dict = arrayAppointments[i];
        }
        
        
        
        
        NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForThumbnailImage,dict[@"pPic"]];
        
        __block RoundedImageView *patientImg = [[RoundedImageView alloc]initWithFrame:CGRectMake(4, 3.5, 55, 55)];
        
        [patientImg sd_setImageWithURL:[NSURL URLWithString:strImageUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            if (image == nil) {
                patientImg.image = [UIImage imageNamed:@"doctor_image_thumbnail.png"];
                
            }else{
                patientImg.image = image;
            }
        }];
        
        
        [view addSubview:pinImageView];
        [view addSubview:patientImg];
        //i.e. customize view to get what you need
        
        UIImage *markerIcon = [self imageFromView:view];
        
        
        
        //  dict = arrayAppointments[i];
        float latitude = [dict[@"apptLat"] floatValue];
        float longitude = [dict[@"apptLong"] floatValue];
        
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(latitude, longitude);
        marker.tappable = YES;
        marker.map = mapView_;
        marker.title = dict[@"apntTime"];
        marker.snippet = dict[@"addrLine2"];
        marker.icon = markerIcon;
        
        // mapView_.selectedMarker = marker;
        
        
        
    }
    // add auto ZoomIn if there is more than 2 markers
    
    if (arrayAppointments.count > 2) {
        GMSCameraUpdate *zoomCamera = [GMSCameraUpdate zoomTo:mapZoomLevel+1];
        [mapView_ animateWithCameraUpdate:zoomCamera];
    }
    
    
    
}



- (UIImage *)imageFromView:(UIView *) view
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


#pragma mark UITableView Delegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 110.0f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // NSArray *arrayAppointments = self.appointmentsArr[pagerView.page][@"appt"];
    //self.dictAppointments = arrayAppointments[indexPath.row];
    
    // [self performSegueWithIdentifier:@"appointmentDetail" sender:self];
    
}
#pragma mark UITableview DataSource -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // NSLog(@"total count :%d",[self.appointmentsArr count]);
    
    if ([self.appointmentsArr count] > 0) {
        
        // UIImageView *img= (UIImageView *)[self.view viewWithTag:NO_APPOINTMENT_IMAGE];
        // [img removeFromSuperview];
        
        return [self.appointmentsArr count];
    }
    return 1;
    
    // return [self.appointmentsArr[pagerView.page][@"appt"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"OtherAppointmentCell";
    OtherAppointmentCell *cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    /*
     if(cell==nil)
     {
     cell =[[OtherAppointmentCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
     
     
     cell.selectionStyle=UITableViewCellAccessoryNone;
     
     cell.backgroundColor=[UIColor clearColor];
     //  cell.buttonReject.hidden = YES;
     // cell.buttonAccept.hidden = YES;
     }
     
     */
    
    if (self.appointmentsArr.count > 0) {
        
        
        cell.labelNoAppointment.hidden = YES;
        NSDictionary *dictAppointDetail = self.appointmentsArr[indexPath.row];
        
        cell.profileImage.image = [UIImage imageNamed:@"notificationlistview_image_boader.png"];
        
        //[cell.activityIndicator startAnimating];
        
        NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForThumbnailImage,dictAppointDetail[@"pPic"]];
        NSLog(@"strUrl :%@",strImageUrl);
        
        __weak OtherAppointmentCell *cellWeak = cell;
        
        [cellWeak.profileImage setImageWithURL:[NSURL URLWithString:strImageUrl]
                              placeholderImage:[UIImage imageNamed:@"notificationlistview_image_boader.png"]
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                                         //[cellWeak.activityIndicator stopAnimating];
                                     }];
        
        
        // NSLog(@"stopAnimating");
        
        //[cell.docApptName setText:dictForOneDocDetails[@"name"]];
        [cell.lblPatientName setText:dictAppointDetail[@"fname"]];
        [cell.lblAppointmentTime setText:dictAppointDetail[@"apntDt"]];
        // [cell.lblPhoneNo setText:dictAppointDetail[@"phone"]];
        // [cell.lblPhoneNo setTitle:dictAppointDetail[@"phone"] forState:UIControlStateNormal];
        [cell.lblAddress setText:dictAppointDetail[@"addrLine2"]];
        
        cell.lblDistance.text = [NSString stringWithFormat:@"%@ KMs",dictAppointDetail[@"distance"]];
        cell.sepratorImage.image = [UIImage imageNamed:@"appclndrscreen_blue_line@2x.png"];
        cell.distanceBaloon.image = [UIImage imageNamed:@"homescreenmapview_current_appointment_indicator@2x.png"];
        cell.btnCall.tag = indexPath.row;
        [cell.btnCall addTarget:self action:@selector(phoneTapped:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        
        cell.labelNoAppointment.hidden = NO;
    }
    return cell;
    
}
#pragma mark UIAlertView Delegate -
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 600) {
        if (buttonIndex == 1) {
            [self requestAcceptRejectBooking:YES details:self.dictAppointmentDetails];
        }
    }else if (alertView.tag == 700){
        if (buttonIndex == 1) {
            [self requestAcceptRejectBooking:NO details:self.dictAppointmentDetails];
        }
        
    }else if (alertView.tag == 500){
        
        NSString *phoneNumber = self.dictAppointmentDetails[@"phone"];  // dynamically assigned
        NSLog(@"phone no %@ %@",self.lblPhone.text , self.dictAppointmentDetails[@"phone"]);
        /*
         NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", phoneNumber];
         NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
         [[UIApplication sharedApplication] openURL:phoneURL];
         */
        
        NSString *cleanedString = [[phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
        [[UIApplication sharedApplication] openURL:telURL];
    }
}

- (void)phoneTapped:(UIButton *)sender{
    
    
    NSDictionary *dict = self.appointmentsArr[sender.tag];
    NSString *phoneNumber =  dict[@"phone"];
    
    NSString *cleanedString = [[phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
    [[UIApplication sharedApplication] openURL:telURL];
    
}

#pragma mark MFMessageview Controller delegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}




@end
