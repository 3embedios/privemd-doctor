//
//  PendingNotificationDetailController.h
//  Doctor
//
//  Created by Rahul Sharma on 23/07/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoundedImageView.h"
#import <GoogleMaps/GoogleMaps.h>

@interface PendingNotificationDetailController : UIViewController<GMSMapViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblAppointTime;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblAdress;
@property (weak, nonatomic) IBOutlet UILabel *lblPatientMessage;
@property (weak, nonatomic) IBOutlet UITableView *tblView;

@property (strong, nonatomic) NSArray *appointmentsArr;
@property(strong, nonatomic) NSDictionary *dictAppointmentDetails;

@property(assign, nonatomic) BOOL isOpenFromPush;
@property(strong, nonatomic) NSString *patientEmail , *appointmentTime;

@property (weak, nonatomic) IBOutlet UILabel *labelAppointmentDate;
@property (weak, nonatomic) IBOutlet UILabel *labelNotes;
@property (weak, nonatomic) IBOutlet RoundedImageView *profileImg;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *btnAccept;
@property (weak, nonatomic) IBOutlet UIButton *btnReject;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailAddress;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
//@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
//@property (weak, nonatomic) IBOutlet UILabel *nameLabel;


- (IBAction)buttonPhoneTapped:(UIButton *)sender;
- (IBAction)buttonMessageTapped:(UIButton *)sender;


- (IBAction)acceptAction:(UIButton *)sender;
- (IBAction)rejectAction:(UIButton *)sender;
@end
