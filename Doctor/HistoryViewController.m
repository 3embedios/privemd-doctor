//
//  HistoryViewController.m
//  Roadyo
//
//  Created by Rahul Sharma on 23/07/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "HistoryViewController.h"
#import "HistoryTableViewCell.h"
@interface HistoryViewController ()

@end

@implementation HistoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark Webservice Request -
-(void)requestHistory
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
  
    NSDictionary *parameters = @{kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId:[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 @"ent_pat_email":self.patientEmail,
                                 @"ent_date_time":[Helper getCurrentDateTime],
                                 @"ent_page":@"1"};
    
    
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient postPath:MethodAppointmentsHistoryWithPatients parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getHistoryResponse:operation.responseString.JSONValue];
        // NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        //NSLog(@"Request Successful, response '%@'", responseStr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];
    
}

- (void)getHistoryResponse:(NSDictionary *)response{
    NSLog(@"response:%@",response);
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
            self.historys = response[@"history"];
            [self.tblView reloadData];
            
        }
        else
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
    
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"History";
    
    PriveMdAppDelegate *delegate = (PriveMdAppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate customizeNavigationLeftButton:self];
    
    [self requestHistory];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)buttonBackTapped{
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark UITableView Delegate -
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self popUp:indexPath.row];
}

#pragma mark UITableview DataSource -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.historys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    HistoryTableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==nil)
    {
        cell =[[HistoryTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        
        cell.selectionStyle=UITableViewCellAccessoryNone;
        
        cell.backgroundColor=[UIColor clearColor];
    }
    
    NSDictionary *dictAppointment = self.historys[indexPath.row];
    
    cell.lblAppointmentDate.text = dictAppointment[@"apptDt"];
    
    /*
    
    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForThumbnailImage,dictAppointment[@"pPic"]];
    NSLog(@"strUrl :%@",strImageUrl);
    
    [cell.profileImage setImageWithURL:[NSURL URLWithString:strImageUrl]
                      placeholderImage:[UIImage imageNamed:@"notificationlistview_image_boader@2x.png"]
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                                 
                             }];
    
    [cell.activityIndicator stopAnimating];
    NSLog(@"stopAnimating");
    
    
    [cell.buttonAccept addTarget:self action:@selector(acceptBooking:) forControlEvents:UIControlEventTouchUpInside];
    cell.buttonAccept.tag = indexPath.row;
    [cell.buttonReject addTarget:self action:@selector(acceptBooking:) forControlEvents:UIControlEventTouchUpInside];
    cell.buttonReject.tag = indexPath.row;
    
    [cell.lblPatientName setText:dictAppointment[@"fname"]];
    [cell.lblAppointmentTime setText:dictAppointment[@"apntDt"]];
    [cell.lblPhoneNo setText:dictAppointment[@"phone"]];
    
    
    cell.lblDistance.text = [NSString stringWithFormat:@"%@ KMs",dictAppointment[@"distance"]];
    */
    
    
    return cell;
    
}

- (void)popUp:(int)selectedIndex{
    
    UIView *transparentView = [[UIView alloc]initWithFrame:self.view.bounds];
    transparentView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
   // transparentView.alpha = 0.5;
    transparentView.tag = 100;
    [self.view addSubview:transparentView];
    
    UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapgestureAction)];
    [transparentView addGestureRecognizer:tapgesture];
    
    UIView *popUp = [[UIView alloc]initWithFrame:CGRectMake(35, self.view.frame.size.height/2-250/2, 250, 250)];
    popUp.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"pop_out_bg.png"]];
    popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    
    [transparentView addSubview:popUp];
    
     NSDictionary *dictAppointment = self.historys[selectedIndex];
    UILabel *labelRemark = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, 240, 230)];
    labelRemark.numberOfLines = 0;
    if ([dictAppointment[@"remarks"] length] > 0) {
         [Helper setToLabel:labelRemark Text:dictAppointment[@"remarks"] WithFont:ZURICH_CONDENSED FSize:12 Color:UIColorFromRGB(0x333333)];
    }else{
         [Helper setToLabel:labelRemark Text:@"No Remarks" WithFont:ZURICH_CONDENSED FSize:12 Color:UIColorFromRGB(0x333333)];
    }
   
    labelRemark.textAlignment = NSTextAlignmentCenter;
   [popUp addSubview:labelRemark];
    
    [UIView animateWithDuration:0.3/1.5 animations:^{
        popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            popUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                popUp.transform = CGAffineTransformIdentity;
            }];
        }];
    }];
}

- (void)tapgestureAction{
    
    UIView *transparentView = [self.view viewWithTag:100];
    
    [UIView animateWithDuration:1.0
                     animations:^{
                         transparentView.alpha = 1.0;
                     } completion:^(BOOL finished) {
                       [transparentView removeFromSuperview];
                     }];
    
}

@end
