//
//  AccountViewController.m
//  privMD
//
//  Created by Rahul Sharma on 19/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "AccountViewController.h"
#import "XDKAirMenuController.h"
//#import "RoadyoPubNubWrapper.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "ANBlurredImageView.h"
#import "CustomNavigationBar.h"
#import "SplashViewController.h"
#import "profile.h"
#import "HomeViewController.h"
#import "RoundedImageView.h"
#import "SignInViewController.h"
#import "TELogger.h"

@interface AccountViewController () <CustomNavigationBarDelegate,UITableViewDataSource,UITableViewDelegate>
@property (strong) IBOutlet ANBlurredImageView *imageView;
@property(nonatomic,strong)profile *user;
@property(nonatomic,weak) IBOutlet UITableView *tableView;
@property(nonatomic,strong) RoundedImageView *profileImage;
@property(nonatomic,strong) UIImage *pickedImage;
@property(nonatomic,assign)BOOL isEditingModeOn;
@property (nonatomic, strong) NSDictionary *userInfo;
@end




@implementation AccountViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - ViewController LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // self.view.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.7];
    self.navigationController.navigationBarHidden = YES;
    
    _tableView.backgroundColor = UIColorFromRGB(0xe9e9e8);
    self.view.backgroundColor = UIColorFromRGB(0xe9e9e8);
    
    
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
    
    /*
     
     // ProfileImageView
     UIView *view = [[UIView alloc] initWithFrame:CGRectMake(3, 32, 75, 75)];
     
     _profileImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 75, 75)];
     _profileImage.image = [UIImage imageNamed:@"signup_profile_image"];
     [view addSubview:_profileImage];
     
     UIButton *buttonSelectPicture = [UIButton buttonWithType:UIButtonTypeCustom];
     buttonSelectPicture.frame = CGRectMake(0, 0, 75, 75);
     [buttonSelectPicture addTarget:self action:@selector(profilePicButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
     [view addSubview:buttonSelectPicture];
     
     [self.tableView addSubview:view];
     */
    
    
    
    [self addCustomNavigationBar];
    [self sendServiceForGetProfileData];
    
    
    
    
    [[TELogger getInstance] initiateFileLogging];
    //TELogInfo(@"%@ ",[NSString stringWithFormat:@"%@",@"ABHISHEKKKKKKK"]);
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    
    
    
    //    UIStoryboard *storyboard = self.storyboard;
    //    HomeViewController *home = [storyboard instantiateViewControllerWithIdentifier:@"ViewController1"];
    //    UINavigationController *navigationcontroller = (UINavigationController*)self.storyboard.instantiateInitialViewController;
    //    navigationcontroller.viewControllers = @[home];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    // self.navigationController.navigationBarHidden = YES;
    
}


#pragma mark - WebService call

-(void)sendServiceForUpdateProfile
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Saving.."];
    
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    
    NSString *deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    
    NSString *fName = _user.fName;
    NSString *lName = _user.lName;
    NSString *eMail = _user.email;
    NSString *phone = _user.mobile;
    
    
    
    NSString *parameters = [NSString stringWithFormat:@"ent_sess_token=%@&ent_dev_id=%@&ent_first_name=%@&ent_last_name=%@&ent_email=%@&ent_phone=%@&ent_date_time=%@",[[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken],deviceId,fName,lName,eMail,phone,[Helper getCurrentDateTime]];
    
    NSString *removeSpaceFromParameter = [Helper removeWhiteSpaceFromURL:parameters];
    NSLog(@"request doctor around you :%@",parameters);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@updateProfile",BASE_URL]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:[[removeSpaceFromParameter stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]
                             dataUsingEncoding:NSUTF8StringEncoding
                             allowLossyConversion:YES]];
    
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(updateProfileResponse:)];
    
}

-(void)updateProfileResponse:(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    NSLog(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        
        
        NSDictionary *dictResponse=[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:_user.fName forKey:KDAFirstName];
            [[NSUserDefaults standardUserDefaults] setObject:_user.lName forKey:KDALastName];
            [[NSUserDefaults standardUserDefaults] setObject:_user.email forKey:KDAEmail];
            [[NSUserDefaults standardUserDefaults] setObject:_user.mobile forKey:KDAPhoneNo];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
    }
    
}

-(void)sendServiceForGetProfileData
{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:@"Loading..."];
    //ResKitWebService * restkit = [ResKitWebService sharedInstance];
    NSDictionary *queryParams;
    queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                   [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                   [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                   [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    
    TELogInfo(@"param%@",queryParams);
    /*
     [restkit composeRequestForProfile:MethodGetMasterProfile
     paramas:queryParams
     onComplition:^(BOOL success, NSDictionary *response){
     
     if (success) { //handle success response
     
     // TELogInfo(@"info%@",response);
     [self getProfileResponse:(NSArray*)response];
     }
     else{//error
     
     }
     }];
     
     */
    
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient postPath:MethodGetMasterProfile parameters:queryParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getProfileResponse:operation.responseString.JSONValue];
        // NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        //NSLog(@"Request Successful, response '%@'", responseStr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];
    
    
    
}

-(void)getProfileResponse:(NSDictionary *)response
{
 
    NSLog(@"profile :%@",response);
    
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }else if ([[response objectForKey:@"errFlag"] intValue] == 1 && ([[response objectForKey:@"errNum"] intValue] == 6 || [[response objectForKey:@"errNum"] intValue] == 7)) {   // session token expire
        
      
        SignInViewController *signVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        signVC.loginBlock = ^(BOOL success){
            
            [self sendServiceForGetProfileData];
            
        };
        UINavigationController *navigantionVC = [[UINavigationController alloc] initWithRootViewController:signVC];
        [self.navigationController presentViewController:navigantionVC animated:YES completion:^{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Expired" message:@"Your session with privMD is expired. Please Login again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }];

        
    }
    else
    {
        
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            self.userInfo = response;
            [_tableView reloadData];
            
            
            
        }
        else
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
    
}



-(void)dismissKeyboard
{
    //[resignFirstResponder];
    //[passwordloginText resignFirstResponder];
    
    [self.view endEditing:YES];
    
    
}
#pragma mark ButtonAction Methods -

- (void)setProfileImg:(UIImage *)profileImg{
    TELogInfo(@"setImage");
    self.profileImage.image = profileImg;
    
}
- (void)menuButtonPressedAccount
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


- (void)setSelectedButtonByIndex:(NSInteger)index {
    
    
}
#pragma mark Custom Methods -

- (void) addCustomNavigationBar
{
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    
    /*
    float deviceWidth = [UIScreen mainScreen].bounds.size.width;
    UIImageView *logoImage = [[UIImageView alloc]initWithFrame:CGRectMake(deviceWidth/2 - 123/2, 64/2 - 31/2, 123, 31)];
    logoImage.image = [UIImage imageNamed:@"home_navigationbarlogo.png"];
    [customNavigationBarView addSubview:logoImage];
*/
    
    customNavigationBarView.delegate = self;
    
   [customNavigationBarView setTitle:@"Profile"];
   
    
    [self.view addSubview:customNavigationBarView];
    
}

-(void)rightBarButtonClicked:(UIButton *)sender{
    
    if ([sender tag] == 200) {
        _isEditingModeOn = NO;
        sender.tag = 100;
        
        [sender setTitle:@"Edit" forState:UIControlStateNormal];
        [self sendServiceForUpdateProfile];
        
    }
    else{
        _isEditingModeOn = YES;
        sender.tag = 200;
        [sender setTitle:@"Update" forState:UIControlStateNormal];
    }
    
}
-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonPressedAccount];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    SplashViewController *SVC = [segue destinationViewController];
    
    
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)profilePicButtonClicked:(id)sender
{
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Edit Picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose From Library", nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
}

- (IBAction)passwordButtonClicked:(id)sender
{
    [Helper showAlertWithTitle:@"Message" Message:@"Please visit our website privemd.com to change your password."];
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked:nil];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked:nil];
                break;
            }
            default:
                break;
        }
    }
}


-(void)cameraButtonClicked:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate =self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message: @"Camera is not available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}
- (void)libraryButtonClicked:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        //[popover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:@"Uploading Profile Pic..."];
    //  _flagCheckSnap = YES;
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Image Info : %@",info);
    
    _pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
    
    UploadFiles *upload = [[UploadFiles alloc] init];
    upload.delegate = self;
    [upload uploadImageFile:_pickedImage];
    
    
}

#pragma UploadFileDelegate

-(void)uploadFile:(UploadFiles *)uploadfile didUploadSuccessfullyWithUrl:(NSArray *)imageUrls
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    _profileImage.image = _pickedImage;
    
}
-(void)uploadFile:(UploadFiles *)uploadfile didFailedWithError:(NSError *)error{
    NSLog(@"upload file  error %@",[error localizedDescription]);
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
}


#pragma mark - UITextFeildDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (_isEditingModeOn) {
        if (textField.tag >= 199 && textField.tag < 204) {
            return YES;
        }
        return NO;
    }
    return NO;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag == 200) {
        _user.fName = textField.text;
    }
    else if (textField.tag == 201) {
        _user.lName = textField.text;
        
    }
    else if (textField.tag == 202) {
        _user.email = textField.text;
    }
    else if (textField.tag == 203) {
        _user.mobile = textField.text;
    }
    
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}
-(void)moveViewUpToPoint:(int)point
{
    CGRect rect = self.view.frame;
    rect.origin.y = point;
    [UIView animateWithDuration:0.4
                     animations:^(void){
                         self.view.frame = rect;
                     }
                     completion:^(BOOL finished){
                     }];
    
    
    
}

-(void)moveViewDown
{
    CGRect rect = self.view.frame;
    rect.origin.y = 0;
    
    
    [UIView animateWithDuration:0.4
                     animations:^(void){
                         self.view.frame = rect;
                     }
                     completion:^(BOOL finished){
                     }];
    
}

#pragma mark AlertView Delegate -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 400) {
        if (buttonIndex == 1) {
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
            [[NSUserDefaults standardUserDefaults] synchronize];
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
            [[[XDKAirMenuController sharedMenu] view] removeFromSuperview];
            
            if ([XDKAirMenuController relese]) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                            @"Main" bundle:[NSBundle mainBundle]];
                
                SplashViewController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
                
                self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
            }
            
        }
    }
    
}





#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 0) {
        return 4;
    }
    else if(section == 1){
        return 2;
    }else if (section == 2){
        return 3;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = nil;//[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        UIImageView *line =nil;
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                line = [[UIImageView alloc] initWithFrame:CGRectMake(0, 139, 320, 1)];
            }else{
                line = [[UIImageView alloc] initWithFrame:CGRectMake(0, 42, 320, 1)];
            }
            
        }else{
            line = [[UIImageView alloc] initWithFrame:CGRectMake(0, 42, 320, 1)];
        }
        line.image = [UIImage imageNamed:@"gray_line.png"];
        line.tag = 200;
        [cell.contentView addSubview:line];
        
        
    }
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(130, 0, 320, 42)];
    textField.borderStyle = UITextBorderStyleNone;
    textField.font = [UIFont fontWithName:ZURICH_LIGHTCONDENSED size:15];
    //textField.placeholder = @"enter text";
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textField.delegate = self;
    textField.tag = 199;
    textField.textColor = UIColorFromRGB(0x666666);
    //textField.background = [UIImage imageNamed:@"myprofile_text_tab.png"];
    [textField setTintColor:[UIColor blueColor]];
    [cell.contentView addSubview:textField];
    
    
    
    UILabel *labelHelperText = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 150, 42)];
    [Helper setToLabel:labelHelperText Text:@"" WithFont:ZURICH_LIGHTCONDENSED FSize:15 Color:UIColorFromRGB(0x006699)];
    labelHelperText.backgroundColor = [UIColor clearColor];
    [cell.contentView addSubview:labelHelperText];
    
    
    
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            
            
            self.profileImage = [[RoundedImageView alloc]initWithFrame:CGRectMake(320/2- 84/2, 15, 84, 84)];
            self.profileImage.image = [UIImage imageNamed:@"myprofile_image_boader.png"];
            [cell.contentView addSubview:self.profileImage];
            
            NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForThumbnailImage,self.userInfo[@"pPic"]];
            
            /*
             [self.profileImage setImageWithURL:[NSURL URLWithString:strImageUrl]
             placeholderImage:[UIImage imageNamed:@"doctor_image_thumbnail.png"]
             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
             
             }];
            */
            
            [self.profileImage sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                          placeholderImage:[UIImage imageNamed:@"myprofile_image_boader.png"] options:0 progress:nil
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   //  [activityIndicator stopAnimating];
                                     
                                 }];
            
            /*
            UIButton *buttonProfile = [UIButton buttonWithType:UIButtonTypeCustom];
            buttonProfile.frame = CGRectMake(320/2- 84/2, 15, 84, 84);
            [buttonProfile addTarget:self action:@selector(profilePicButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:buttonProfile];
            */
            
            UITextField *textFieldName = [[UITextField alloc] initWithFrame:CGRectMake(0, 98, 320, 42)];
            textFieldName.borderStyle = UITextBorderStyleNone;
            textFieldName.font = [UIFont fontWithName:ZURICH_LIGHTCONDENSED size:15];
            //textField.placeholder = @"enter text";
            textFieldName.autocorrectionType = UITextAutocorrectionTypeNo;
            textFieldName.keyboardType = UIKeyboardTypeDefault;
            textFieldName.returnKeyType = UIReturnKeyDone;
            textFieldName.clearButtonMode = UITextFieldViewModeWhileEditing;
            textFieldName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            textFieldName.textAlignment = NSTextAlignmentCenter;
            textFieldName.delegate = self;
            if (self.userInfo[@"fName"] == nil || [self.userInfo[@"fName"] length] == 0) {
                textFieldName.text = @"";
            }else{
            textFieldName.text = [NSString stringWithFormat:@"%@ %@",self.userInfo[@"fName"],self.userInfo[@"lName"]];
            }
            textFieldName.textColor = UIColorFromRGB(0x333333);
            [textField setTintColor:[UIColor blueColor]];
            [cell.contentView addSubview:textFieldName];
            
            
            
        }
        if (indexPath.row == 1) {
            
            textField.tag = 202;
            labelHelperText.text = @"Profession:";
            /*
            if ([self.userInfo[@"type"] isEqualToString:@"MD"]) {
                textField.text = @"Doctor";
            }else if ([self.userInfo[@"type"] isEqualToString:@"Nurse"]){
              textField.text = @"Nurse";
            }
            else{
                textField.text = @"";
            }
            */
            
            textField.text = self.userInfo[@"type"];
            
        }
        if (indexPath.row == 2) {
            textField.tag = 203;
            labelHelperText.text = @"Mobile:";
            textField.text = self.userInfo[@"mobile"];
        }
        if (indexPath.row == 3) {
            
            labelHelperText.text = @"Email:";
            textField.text = self.userInfo[@"email"];
        }
        
    }
    else if (indexPath.section == 1){
        
        if (indexPath.row == 0) {
            labelHelperText.text = @"Licence Number:";
            textField.text = self.userInfo[@"licNo"];
        }
        if (indexPath.row == 1) {
            labelHelperText.text = @"Expiry Date";
            textField.text = self.userInfo[@"licExp"];
        }
    }
    else if (indexPath.section == 2){
        if (indexPath.row == 0) {
            labelHelperText.text = @"Number Of Reviews:";
            textField.text = self.userInfo[@"totRats"];
        }
        if (indexPath.row == 1) {
            labelHelperText.text = @"Average Rating:";
            textField.text = self.userInfo[@"avgRate"];
        }
        if (indexPath.row == 2) {
            labelHelperText.text = @"Completed Booking:";
            textField.text = self.userInfo[@"cmpltApts"];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}



#pragma mark - UITableViewDataSource Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* tableViewCell = [tableView cellForRowAtIndexPath:indexPath];
    [tableViewCell setSelected:NO animated:YES];
    
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 21)];
    headerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"myprofile_headerbar.png"]];
    //UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(5, 7.5, 15, 15)];
    
    //[headerView addSubview:icon];
    
    UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 21)];
    labelTitle.textAlignment = NSTextAlignmentCenter;
    [Helper setToLabel:labelTitle Text:@"" WithFont:ZURICH_LIGHTCONDENSED FSize:15 Color:WHITE_COLOR];
    [headerView addSubview:labelTitle];
    
    if (section == 1) {
        //icon.image = [UIImage imageNamed:@"signup_driver_icon"];
        labelTitle.text = @"MEDICAL LICENCE DETAILS";
    }
    else if(section == 2){
        // icon.image = [UIImage imageNamed:@"signup_car_icon"];
        labelTitle.text = @"STATISTICS";
    }
    return headerView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0  ;
    }
    return 21;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return 140;
        }
    }
    return 42;
}
@end
