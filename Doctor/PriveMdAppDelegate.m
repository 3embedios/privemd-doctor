//
//  PriveMdAppDelegate.m
//  Doctor
//
//  Created by Rahul Sharma on 17/04/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "PriveMdAppDelegate.h"
#import "HelpViewController.h"
#import "SplashViewController.h"
//#import "MapViewController.h"
#import "Reachability.h"
#import <Crashlytics/Crashlytics.h>
#import "MenuViewController.h"
#import "HomeViewController.h"
#import "PendingNotificationViewController.h"
#import "PendingNotificationDetailController.h"
#import <Pushwoosh/PushNotificationManager.h>
#import <AudioToolbox/AudioToolbox.h>
#import "PMDReachabilityWrapper.h"
#import "TELogger.h"

@interface PriveMdAppDelegate()
{
    NSString *alertMsg;
}
@property (nonatomic,strong) HelpViewController *helpViewController;
@property (nonatomic,strong) SplashViewController *splashViewController;
//@property (nonatomic,strong) MapViewController *mapViewContoller;

@property (nonatomic, strong) Reachability *hostReach;
@property (nonatomic, strong) Reachability *internetReach;
@property (nonatomic, strong) Reachability *wifiReach;

@property (nonatomic, strong) NSDictionary *pushInfo;

@end

@implementation PriveMdAppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize hostReach;
@synthesize internetReach;
@synthesize wifiReach;


-(void)customizeNavigationBar {
    
    
    if (IS_IOS7) {
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"login_screen_navigation_bar@2x.png"] forBarMetrics:UIBarMetricsDefault];
    }
    else{
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"login_screen_navigation_bar_iphonesix@2x.png"] forBarMetrics:UIBarMetricsDefault];
    }
    

    
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           UITextAttributeTextColor: UIColorFromRGB(0x333333),
                                                           UITextAttributeTextShadowColor: [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8],
                                                           UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
                                                           UITextAttributeFont: [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0],
                                                           }];
    
    
    
//    [[UINavigationBar appearance] setTitleTextAttributes:@{
//                                                           NSForegroundColorAttributeName: UIColorFromRGB(0x333333),
//                                                           UITextAttributeTextShadowColor: [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8],
//                                                           UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
//                                                           NSFontAttributeName: [UIFont fontWithName:ZURICH_CONDENSED_BT size:34.0],
//                                                           }];

}

- (void)customizeNavigationLeftButton:(UIViewController *)controller{
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 66.5, 44);
    [backBtn setImage:[UIImage imageNamed:@"notificationlistview_back_btn_off@2x.png"] forState:UIControlStateNormal];
     [backBtn setImage:[UIImage imageNamed:@"notificationlistview_back_btn_on@2x.png"] forState:UIControlStateHighlighted];
    [backBtn addTarget:controller action:@selector(buttonBackTapped) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *backButtonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 66.5, 44)];
    backButtonView.bounds = CGRectOffset(backButtonView.bounds, 20, 0);
    [backButtonView addSubview:backBtn];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithCustomView:backButtonView];
    controller.navigationItem.leftBarButtonItem = leftBarButton;
  
}

- (void)buttonBackTapped{
    
 
}

- (void)insertDefaultValue{
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    if ([ud boolForKey:@"isFirstLoginCompleted"] == NO) {
        
        [ud setBool:YES forKey:@"isFirstLoginCompleted"];
        
        [ud setValue:@"3" forKey:@"DoctorStatus"];
    }
}

void uncaughtExceptionHandler(NSException *exception) {
    //[Flurry startSession:@"VX2Q9RPNNFQGCD5KDP86"];
    //[Flurry logError:@"Uncaught" message:@"Crash!" exception:exception];
    NSLog(@"The app has encountered an unhandled exception: %@", [exception debugDescription]);
    NSLog(@"Stack trace: %@", [exception callStackSymbols]);
    NSLog(@"desc: %@", [exception description]);
    NSLog(@"name: %@", [exception name]);
    NSLog(@"user info: %@", [exception userInfo]);
}
//use NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //-----------PUSHWOOSH PART-----------
    // set custom delegate for push handling, in our case - view controller
    //    PushNotificationManager * pushManager = [PushNotificationManager pushManager];
    //    pushManager.delegate = self;
    
    // handling push on app start
    [[PushNotificationManager pushManager] handlePushReceived:launchOptions];
    
    // make sure we count app open in Pushwoosh stats
    [[PushNotificationManager pushManager] sendAppOpen];
    
    // register for push notifications!
    [[PushNotificationManager pushManager] registerForPushNotifications];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    //LumberJack init
    [[TELogger getInstance] initiateFileLogging];
    
    
    [GMSServices provideAPIKey:kPMDGoogleMapsAPIKey];
     //RKClient* client = [RKClient clientWithBaseURL:@"http://restkit.org"];
    // Override point for customization after application launch.
    
   
    
    
  // [[Crashlytics sharedInstance] setDebugMode:YES];
    [Crashlytics startWithAPIKey:kPMDCrashLyticsAPIKey];
    
    [self customizeNavigationBar];
    [self handlePushNotificationWithLaunchOption:launchOptions];
    [self insertDefaultValue];
    
    
    /*
    NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];
    
    NSArray *fontNames;
    NSInteger indFamily, indFont;
    for (indFamily=0; indFamily<[familyNames count]; ++indFamily)
    {
        NSLog(@"Family name: %@", [familyNames objectAtIndex:indFamily]);
        fontNames = [[NSArray alloc] initWithArray:
                     [UIFont fontNamesForFamilyName:
                      [familyNames objectAtIndex:indFamily]]];
        for (indFont=0; indFont<[fontNames count]; ++indFont)
        {
            NSLog(@"    Font name: %@", [fontNames objectAtIndex:indFont]);
        }
    }
*/
    

    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    PMDReachabilityWrapper *networkHandler = [PMDReachabilityWrapper sharedInstance];
    [networkHandler monitorReachability];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"user Info %@",userInfo);
  //  application.applicationIconBadgeNumber = 0;
    //[[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:@"PUSH"];
    //[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isBooked"];
    
 //   [self handleNotificationForUserInfo:userInfo];
    
    
    alertMsg = userInfo[@"aps"][@"alert"];
    NSString *badge = userInfo[@"aps"][@"badge"];
    TELogInfo(@"My userInfo is: %@", userInfo);
    
    NSString *jsonString = [userInfo objectForKey:@"u"];
    TELogInfo(@"My jsonString is: %@", jsonString);
    
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    TELogInfo(@"My token is: %@", data);
    
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    TELogInfo(@"My data is: %@", json);
    
    NSMutableDictionary *userinfolocal = [[NSMutableDictionary alloc]initWithObjectsAndKeys:json,@"aps", nil];
    [userinfolocal setValue:alertMsg forKey:@"alert"];
    [userinfolocal setValue:badge forKey:@"badge"];
    TELogInfo(@"My userinfolocal is: %@", userinfolocal);
    
    NSDictionary *dictContainingUserInfo  = [[NSDictionary alloc]initWithDictionary:userinfolocal];
    TELogInfo(@"My dict is: %@", dictContainingUserInfo);
    [self handleNotificationForUserInfo:dictContainingUserInfo];
    
    

    
    //self.textView.text = [userInfo description];
    
    // We can determine whether an application is launched as a result of the user tapping the action
    
    // button or whether the notification was delivered to the already-running application by examining
    
    // the application state.
    
//    if (application.applicationState == UIApplicationStateBackground  | application.applicationState ==UIApplicationStateInactive )
//    {
//       NSLog(@"background");
//        
//        UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        MenuViewController *viewcontrolelr = [mainstoryboard instantiateViewControllerWithIdentifier:@"home"];
//        UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
//        
//        
//        //[naviVC pushViewController:viewcontrolelr animated:YES];
//        
//
//    }
//    else if (application.applicationState == UIApplicationStateActive)
//    {
//        
//     
//        
//        UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        //MenuViewController *viewcontrolelr = [mainstoryboard instantiateViewControllerWithIdentifier:@"home"];
//        UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
//        
//        //if ([[naviVC.viewControllers lastObject] isKindOfClass:[HomeViewController class]]) {
//            //already there in same class
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:userInfo];
//       // }
//        
//        //[naviVC pushViewController:viewcontrolelr animated:YES];
//        
//
//    
//        
//        
//    }
//    else{
//        TELogInfo(@"etc");
//    }
    
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
	NSLog(@"My token is: %@", deviceToken);
    NSString *dt = [[deviceToken description] stringByTrimmingCharactersInSet:
                    [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    dt = [dt stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"My token is: %@", dt);
    
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"]) {
        //device is simulator
        [[NSUserDefaults standardUserDefaults]setObject:@"123" forKey:KDAgetPushToken];
    }
    else {
        [[NSUserDefaults standardUserDefaults]setObject:dt forKey:KDAgetPushToken];
    }
    
    
    
    
      [[PushNotificationManager pushManager] handlePushRegistration:deviceToken];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"])
    {
    //device is simulator
    [[NSUserDefaults standardUserDefaults]setObject:@"123" forKey:KDAgetPushToken];
    }
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    
    return YES;
}

#pragma mark -
#pragma mark Core Data stack
// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma Reachability

- (BOOL)isNetworkAvailable {
    
    return self.networkStatus != NotReachable;
}

// Called by Reachability whenever status changes.
- (void)reachabilityChanged:(NSNotification* )note {
    
    Reachability *curReach = (Reachability *)[note object];
    
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    
    _networkStatus = [curReach currentReachabilityStatus];
    
    if (_networkStatus == NotReachable) {
        NSLog(@"Network not reachable.");
    }
    
}
- (void)monitorReachability {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    self.hostReach = [Reachability reachabilityWithHostName:@"http://www.privemd.com"];
    [self.hostReach startNotifier];
    
    self.internetReach = [Reachability reachabilityForInternetConnection];
    [self.internetReach startNotifier];
    
    self.wifiReach = [Reachability reachabilityForLocalWiFi];
    [self.wifiReach startNotifier];
}
- (void)saveContext
{
    
}

#pragma mark - HandlePushNotification
/**
 *  handle push if app is opened by clicking push
 *
 *  @param launchOptions pushpayload dictionary
 */
-(void)handlePushNotificationWithLaunchOption:(NSDictionary*)launchOptions {
    
    NSLog(@"handle push %@",launchOptions);
    NSDictionary *remoteNotificationPayload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotificationPayload) {
        
        //check if user is logged in
        //if([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken])
        //{
           // [self handleNotificationForUserInfo:remoteNotificationPayload];
        //}
        [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(postNotificationForBooking:) userInfo:remoteNotificationPayload repeats:NO];
        
        
    }
}
- (void)handleNotificationForUserInfo:(NSDictionary*)userInfo{
    
    //  NSLog(@"handle push %@",userInfo);
    // NSLog(@"handleNotificationForUserInfo Push Test");
    //play notification sound
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef soundFileURLRef = CFBundleCopyResourceURL(mainBundle, CFSTR("sms-received"), CFSTR("wav"), NULL);
    SystemSoundID soundId;
    AudioServicesCreateSystemSoundID(soundFileURLRef, &soundId);
    AudioServicesPlaySystemSound(soundId);
    CFRelease(soundFileURLRef);
    
    
    [[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:@"PUSH"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isBooked"];
    
    self.pushInfo = userInfo;
    
    int type = [userInfo[@"aps"][@"nt"] intValue];
    
    if (type == 7 || type == 1) {  // new booking from Patient
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:alertMsg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"View", nil];
        
        UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
        
        TELogInfo(@"currentViewControll : %@", [[[[naviVC.viewControllers objectAtIndex:0] childViewControllers] objectAtIndex:1]  childViewControllers]);
        
        NSInteger badge =  [[NSUserDefaults standardUserDefaults] integerForKey:@"Badge"];
        badge = badge + [userInfo[@"badge"] integerValue];
        [[NSUserDefaults standardUserDefaults] setInteger:badge forKey:@"Badge"];
        
        
        // NSLog(@"badge count test :%d",[userInfo[@"badge"] integerValue]);
        // NSLog(@"badge count :%d",badge);
        
        [UIApplication sharedApplication].applicationIconBadgeNumber = badge;
        
        
        if ([[[[[[naviVC.viewControllers objectAtIndex:0] childViewControllers] objectAtIndex:1]  childViewControllers] lastObject] isKindOfClass:[HomeViewController class]]) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:self.pushInfo];
            alert.tag = 200;
            [alert show];
        }else if ([[[[[[naviVC.viewControllers objectAtIndex:0] childViewControllers] objectAtIndex:1]  childViewControllers] lastObject] isKindOfClass:[PendingNotificationDetailController class]]){
            
            alert.tag = 300;
            [alert show];
            
        }
        else if (![[naviVC.viewControllers lastObject] isKindOfClass:[PendingNotificationDetailController class]] ) {
            
            alert.tag = 200;
            [alert show];
            
        }else{
            
            alert.tag = 300;
            [alert show];
            
        }
        
        
        
        
        
    }
    else if (type == 2 || type == 3){ //reject, accept , cancel from admin
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:alertMsg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
        if ([[[[[[naviVC.viewControllers objectAtIndex:0] childViewControllers] objectAtIndex:1]  childViewControllers] lastObject] isKindOfClass:[HomeViewController class]]) {
            alert.tag = 400;
            [alert show];
        }
        
    }
    else if(type == 8){
        
        
        
        UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
        
        if (![[naviVC.viewControllers lastObject] isKindOfClass:[HomeViewController class]]) {
            //[naviVC popToViewController:bookingDirectionVC animated:YES];
            [naviVC popToRootViewControllerAnimated:YES];
            
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:userInfo[@"aps"][@"alert"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else if (type == 6){
        
        UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
        
        TELogInfo(@"currentViewControll : %@", naviVC.viewControllers);
        
        if ([[naviVC.viewControllers lastObject] isKindOfClass:[HomeViewController class]]) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AppointmentCancel" object:nil userInfo:nil];
        }
        
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:alertMsg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }else if (type == 11 || type == 12){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"HandleAcceptAndRejectFromAdmin" object:nil userInfo:userInfo];
    }
}

-(void)postNotificationForBooking:(NSTimer*)timer{
    
    NSLog(@"userinfo %@",timer.userInfo);
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"Match" object:timer.userInfo];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:timer.userInfo];
}

#pragma mark UIAlertView Delegate -
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 200) {
        switch (buttonIndex) {
            case 1:{
              
                 UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
                 // TELogInfo(@"currentViewControll : %@", [[[[[[naviVC.viewControllers objectAtIndex:0] childViewControllers] objectAtIndex:1]  childViewControllers] objectAtIndex:0] childViewControllers]);
//                for (UIViewController *vc in naviVC.viewControllers) {
//                    NSLog(@"VC :%@",vc);
//                }
                 //TELogInfo(@"lastViewControll : %@",[naviVC.viewControllers lastObject]);
                
                
             if   ([[[[[[naviVC.viewControllers objectAtIndex:0] childViewControllers] objectAtIndex:1]  childViewControllers] lastObject] isKindOfClass:[PendingNotificationDetailController class]]){
                 
                   // NSLog(@"inside tag 200");
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:self.pushInfo];
                 
                    
             }else{
                // NSLog(@"outside tag 200");
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                
                PendingNotificationDetailController *notificationDetail = [storyboard instantiateViewControllerWithIdentifier:@"PendingNotificationDetail"];
                notificationDetail.isOpenFromPush = YES;
                notificationDetail.patientEmail = self.pushInfo[@"aps"][@"e"];
                notificationDetail.appointmentTime = self.pushInfo[@"aps"][@"dt"];
                
               // UINavigationController *naviVC =(UINavigationController*) self.window.rootViewController;
               
                [naviVC pushViewController:notificationDetail animated:YES];
             }
                break;
            }
            default:
                break;
        }
    }else if (alertView.tag == 300){
        if (buttonIndex == 1) {
           //  NSLog(@"inside tag 300");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:self.pushInfo];
        }
    }else if (alertView.tag == 400){
        
         [[NSNotificationCenter defaultCenter] postNotificationName:@"NewBooking" object:nil userInfo:self.pushInfo];
    }
}

@end
