    //
//  SignUpViewController.m
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "SignUpViewController.h"
#import "TermsnConditionViewController.h"
#import "SignInViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "SignUp.h"
#import "UploadFiles.h"
#import "HomeViewController.h"
#import  <ActionSheetStringPicker.h>
#import "TELogger.h"

#define REGEX_PASSWORD_ONE_UPPERCASE @"^(?=.*[A-Z]).*$"  //Should contains one or more uppercase letters
#define REGEX_PASSWORD_ONE_LOWERCASE @"^(?=.*[a-z]).*$"  //Should contains one or more lowercase letters
#define REGEX_PASSWORD_ONE_NUMBER @"^(?=.*[0-9]).*$"  //Should contains one or more number
#define REGEX_PASSWORD_ONE_SYMBOL @"^(?=.*[!@#$%&_]).*$"  //Should contains one or more symbol


@interface SignUpViewController ()<CLLocationManagerDelegate>{
    
   IBOutlet UIView *dropDownBox;
    BOOL isOpened;
    
  IBOutlet UIView *transparentView;
}
@property (assign ,nonatomic) BOOL isImageNeedsToUpload;
@property (assign ,nonatomic) BOOL isKeyboardIsShown;


typedef enum {
    PasswordStrengthTypeWeak,
    PasswordStrengthTypeModerate,
    PasswordStrengthTypeStrong
}PasswordStrengthType;

@property(nonatomic,strong)CLLocationManager *locationManager;
@property(nonatomic,assign)float currentLatitude;
@property(nonatomic,assign)float currentLongitude;

//-(PasswordStrengthType)checkPasswordStrength:(NSString *)password;
-(int)checkPasswordStrength:(NSString *)password;

@end

@implementation SignUpViewController

@synthesize mainView;
@synthesize mainScrollView;
@synthesize firstNameTextField;
@synthesize lastNameTextField;
@synthesize selectProfessionType;
@synthesize passwordTextField;
@synthesize conPasswordTextField;
@synthesize phoneNoTextField;
@synthesize zipCodeTextField;
@synthesize helperCountry;
@synthesize helperCity;
@synthesize navCancelButton;
@synthesize navNextButton;
@synthesize saveSignUpDetails;
@synthesize profileButton;
@synthesize profileImageView;
@synthesize tncButton;
@synthesize tncCheckButton;
@synthesize creatingLabel;
@synthesize isKeyboardIsShown;
@synthesize activeTextField;
@synthesize actionShet;
@synthesize pickerview;
@synthesize pickerContent;
@synthesize doctorType;
@synthesize emailTextField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark UIViewLifeCycle -
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
   
    if ([Helper isIphone5]) {
         self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg-568h"]];
    }else{
         self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg.png"]];
    }
   
    
    
    self.mainScrollView.backgroundColor = [UIColor clearColor];
    mainScrollView.showsVerticalScrollIndicator = YES;
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton           = NO;
    
    //self.navigationItem.title = @"Create Account";
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
    [self createNavView];
    [self createNavLeftButton];
    self.title = @"Create Account";
    [self createNavRightButton];
    
    [Helper setToLabel:creatingLabel Text:@"By creating an account you agree to our " WithFont:HELVETICANEUE_LIGHT FSize:14 Color:UIColorFromRGB(0x000000)];
    [Helper setButton:tncButton Text:@"Agree to the Terms & Conditions." WithFont:HELVETICANEUE_LIGHT FSize:14 TitleColor:UIColorFromRGB(0x34718f) ShadowColor:nil];
    
    [firstNameTextField setValue:UIColorFromRGB(0x333333)
                      forKeyPath:@"_placeholderLabel.textColor"];
    [lastNameTextField setValue:UIColorFromRGB(0x333333)
                     forKeyPath:@"_placeholderLabel.textColor"];
    [selectProfessionType setValue:UIColorFromRGB(0x333333)
                  forKeyPath:@"_placeholderLabel.textColor"];
    [passwordTextField setValue:UIColorFromRGB(0x333333)
                     forKeyPath:@"_placeholderLabel.textColor"];
    [conPasswordTextField setValue:UIColorFromRGB(0x333333)
                        forKeyPath:@"_placeholderLabel.textColor"];
    [phoneNoTextField setValue:UIColorFromRGB(0x333333)
                    forKeyPath:@"_placeholderLabel.textColor"];
    [zipCodeTextField setValue:UIColorFromRGB(0x333333)
                    forKeyPath:@"_placeholderLabel.textColor"];
    [emailTextField setValue:UIColorFromRGB(0x333333) forKeyPath:@"_placeholderLabel.textColor"];

    
    firstNameTextField.font                       = [UIFont fontWithName:HELVETICANEUE_LIGHT size:14];
    firstNameTextField.textColor                  = UIColorFromRGB(0x000000);
    lastNameTextField.font                        = [UIFont fontWithName:HELVETICANEUE_LIGHT size:14];
    lastNameTextField.textColor                   = UIColorFromRGB(0x000000);
    selectProfessionType.font                           = [UIFont fontWithName:HELVETICANEUE_LIGHT size:14];
    selectProfessionType.textColor                      = UIColorFromRGB(0x000000);
    passwordTextField.font                        = [UIFont fontWithName:HELVETICANEUE_LIGHT size:14];
    passwordTextField.textColor                   = UIColorFromRGB(0x000000);
    conPasswordTextField.font                        = [UIFont fontWithName:HELVETICANEUE_LIGHT size:14];
    conPasswordTextField.textColor                   = UIColorFromRGB(0x000000);
    phoneNoTextField.font                         = [UIFont fontWithName:HELVETICANEUE_LIGHT size:14];
    phoneNoTextField.textColor                    = UIColorFromRGB(0x000000);
    zipCodeTextField.font                         = [UIFont fontWithName:HELVETICANEUE_LIGHT size:14];
    zipCodeTextField.textColor                    = UIColorFromRGB(0x000000);
    emailTextField.font                         = [UIFont fontWithName:HELVETICANEUE_LIGHT size:14];
    emailTextField.textColor                    = UIColorFromRGB(0x000000);
    
    
    profileImageView.image                        = [UIImage imageNamed:@"createaccount_imagethumbnail.png"];
    
    mainScrollView.scrollEnabled                  = YES;
    
    isTnCButtonSelected = NO;
    
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    if (screenSize.size.height == 480)
    {
        mainScrollView.frame = CGRectMake(0, 0, 320, 480);
        mainScrollView.contentSize = CGSizeMake(320,500);
    }
    else
    {
        mainScrollView.contentSize = CGSizeMake(320,tncButton.frame.size.height+tncButton.frame.origin.y);
    }

    
    pickerContent = @[@"MD",@"Nurse"];
    
    
    transparentView.frame = [UIScreen mainScreen].bounds;
    transparentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.5];
    transparentView.hidden = YES;
    
    UITapGestureRecognizer *tapGestureOnTransparentView = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(changeOpenStatus:)];
    [transparentView addGestureRecognizer:tapGestureOnTransparentView];

    
    [mainScrollView addSubview:transparentView];
    
    dropDownBox.frame = CGRectMake(selectProfessionType.frame.origin.x, selectProfessionType.frame.origin.y + selectProfessionType.frame.size.height, selectProfessionType.frame.size.width, 1);
    dropDownBox.backgroundColor = [UIColor whiteColor];
    [dropDownBox.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [dropDownBox.layer setBorderWidth:2];
    [transparentView addSubview:dropDownBox];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    isKeyboardIsShown = NO;
    

}

- (void)viewDidAppear:(BOOL)animated{
    
    [self getCurrentLocation];
}
- (void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    [self.view endEditing:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    

}
-(void)createNavView
{
    UIView *navView               = [[UIView alloc]initWithFrame:CGRectMake(80, -10, 160, 50)];

    UILabel *navTitle             = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 160, 30)];
    navTitle.text                 = @"Create Account";
    
    navTitle.font                 = [UIFont fontWithName:ZURICH size:20];
    navTitle.textColor            = [UIColor blackColor];
    navTitle.textAlignment        = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:ZURICH_ROMANCONDENSED size:17];
    [navView addSubview:navTitle];

    /*
    UIImageView *navImage         = [[UIImageView alloc]initWithFrame:CGRectMake(160/2-72/2,35,72, 4)];
    navImage.image                = [UIImage imageNamed:@"createaccount_stepline.png"];
    [navView addSubview:navImage];

    UIImageView *navImageDot      = [[UIImageView alloc]initWithFrame:CGRectMake(0,-3,11,11)];
    navImageDot.image             = [UIImage imageNamed:@"createaccount_stepon.png"];
    [navImage addSubview:navImageDot];

    UIImageView *navImageDotOff   = [[UIImageView alloc]initWithFrame:CGRectMake(61,-3,11,11)];
    navImageDotOff.image          = [UIImage imageNamed:@"createaccount_stepoff.png"];
    [navImage addSubview:navImageDotOff];

    self.navigationItem.titleView = navView;
    //[self.navigationItem.titleView addSubview:navView];
    
    //[self.navigationController.navigationBar addSubview:navView];
     */
}
-(void)createNavLeftButton
{
    ///////////
    navCancelButton                         = [UIButton buttonWithType:UIButtonTypeCustom];

    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];

    [navCancelButton setFrame:CGRectMake(0.0f,0.0f, 60,30)];

    //[Helper setButton:navCancelButton Text:@"Cancel" WithFont:@"HelveticaNeue" FSize:17 TitleColor:[UIColor blueColor] ShadowColor:nil];
    navCancelButton.titleLabel.font         = [UIFont fontWithName:ZURICH_LIGHTCONDENSED size:15];
    [navCancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [navCancelButton setTitleColor:[UIColor colorWithWhite:0.384 alpha:1.000] forState:UIControlStateHighlighted];
    [navCancelButton setTitle:@"Cancel" forState:UIControlStateNormal];


    [navCancelButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];

    // Create a container bar button
   // self.navigationItem.leftBarButtonItem   = containingcancelButton;
    
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)createNavRightButton
{
    
    navNextButton = [UIButton buttonWithType:UIButtonTypeCustom];

    [navNextButton addTarget:self action:@selector(NextButtonClicked) forControlEvents:UIControlEventTouchUpInside];

    [navNextButton setFrame:CGRectMake(0.0f,0.0f, 60,30)];

    //[Helper setButton:navNextButton Text:@"Next" WithFont:@"HelveticaNeue" FSize:17 TitleColor:[UIColor blueColor] ShadowColor:nil];
    navNextButton.titleLabel.font = [UIFont fontWithName:ZURICH_LIGHTCONDENSED size:15];
    [navNextButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [navNextButton setTitleColor:[UIColor colorWithWhite:0.384 alpha:1.000] forState:UIControlStateHighlighted];
    [navNextButton setTitle:@"Next" forState:UIControlStateNormal];
   
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];

}
-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    [firstNameTextField resignFirstResponder];
    [lastNameTextField resignFirstResponder];
    [selectProfessionType resignFirstResponder];
    [passwordTextField resignFirstResponder];
    [conPasswordTextField resignFirstResponder];
    [phoneNoTextField resignFirstResponder];
}

-(void)signUp
{
    
    [self.view endEditing:YES];
    
    NSString *signupFirstName       = firstNameTextField.text;
    //NSString *signupLastName       = lastNameTextField.text;
    NSString *signupDoctorType      = selectProfessionType.text;
    NSString *signupPassword        = passwordTextField.text;
    NSString *signupConfirmPassword = conPasswordTextField.text;
    NSString *signupPhoneno         = phoneNoTextField.text;
    NSString *signupEmail           = emailTextField.text;
    NSString *signupZip             = zipCodeTextField.text;
    
    NSString *professionType = [NSString stringWithFormat:@"%d",doctorType];

    if ((unsigned long)signupFirstName.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter First Name"];
        [firstNameTextField becomeFirstResponder];
    }
   

    else if ((unsigned long)signupEmail.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Email ID"];
        [selectProfessionType becomeFirstResponder];
    }
    else if ((unsigned long)signupPassword.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Password"];
        [passwordTextField becomeFirstResponder];
    }
    else if ((unsigned long)signupConfirmPassword.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Password to Confirm"];
        [conPasswordTextField becomeFirstResponder];
    }
    else if ([Helper emailValidationCheck:signupEmail] == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Invalid Email Id"];
         emailTextField.text             = @"";
        [emailTextField becomeFirstResponder];
    }

    else if ([signupPassword isEqualToString:signupConfirmPassword] == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Password Mismatched"];
       // passwordTextField.text = @"";
        conPasswordTextField.text       = @"";
        [conPasswordTextField becomeFirstResponder];
    }
    else if ((unsigned long)signupPhoneno.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter PhoneNo"];
        [phoneNoTextField becomeFirstResponder];
    }
    else if ((unsigned long)signupDoctorType.length == 0){
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Profession type"];
        [self.selectProfessionType becomeFirstResponder];
    }else if ((unsigned long)signupZip.length == 0){
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Zip code"];
        [self.zipCodeTextField becomeFirstResponder];
    }
    else if (![self validateZip:signupZip]){
//        if (![self validateZip:signupZip]) {
            [Helper showAlertWithTitle:@"Message" Message:@"Enter a valid zip code"];
        //}
    }
    else if (!isTnCButtonSelected)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Please select our Terms and Condition"];
    }
    else
    {
        if ((unsigned long)lastNameTextField.text.length == 0)
        {
    lastNameTextField.text          = @"";
        }

        //[self validateEmailAndPostalCode];
        
        
        NSString * pushToken;
        if([[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken])
        {
            pushToken =[[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken];
            
        }
        else
        {
            pushToken =@"dgfhfghr765998ghghj";
            
        }
        
        
        NSString *deviceID ;
        
        if (IS_SIMULATOR) {
            deviceID = kPMDTestDeviceidKey;
        }
        else {
            deviceID = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
        }
        
        
        ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
        [progressIndicator showPIOnView:self.view withMessage:@"Signing in"];
        //  ResKitWebService * restKit = [ResKitWebService sharedInstance];
        
        NSString * lattitude =[NSString stringWithFormat:@"%f",_currentLatitude];
        NSString * logitude =[NSString stringWithFormat:@"%f",_currentLongitude];
        
        
        NSDictionary *queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                     
                                     signupFirstName,kSMPSignUpFirstName,
                                     lastNameTextField.text,kSMPSignUpLastName,
                                     signupEmail,kSMPSignUpEmail,
                                     signupPassword, kSMPSignUpPassword,
                                     signupPhoneno,kSMPSignUpMobile,
                                     signupZip,kSMPSignUpZipCode,
                                     deviceID,kSMPSignUpDeviceId,
                                     pushToken,kSMPgetPushToken,
                                     @"1",kSMPSignUpDeviceType,
                                     lattitude,kSMPSignUpLattitude,
                                     logitude,kSMPSignUpLongitude,
                                     professionType,kSMPSignupDoctorType,
                                     [Helper getCurrentDateTime],kSMPSignUpDateTime, nil];
        
        TELogInfo(@"param%@",queryParams);
        
        /*
         [restKit composeRequestForSignUpWithMethod:MethodPatientSignUp
         paramas:queryParams
         onComplition:^(BOOL success, NSDictionary *response){
         
         if (success) { //handle success response
         [self signupResponse:(NSArray*)response];
         }
         else{//error
         ProgressIndicator *pi = [ProgressIndicator sharedInstance];
         [pi hideProgressIndicator];
         [Helper showAlertWithTitle:@"Error" Message:@"Error occured please try again later"];
         }
         }];
         
         
         
         
         */
        
        NSURL *url = [NSURL URLWithString:BASE_URL];
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
        [httpClient postPath:MethodPatientSignUp parameters:queryParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self signupResponse:operation.responseString.JSONValue];
            // NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            //NSLog(@"Request Successful, response '%@'", responseStr);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        }];
        
        
    }
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)signupResponse:(NSDictionary *)response{
    
   // NSLog(@"response:%@",response);
     NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    if (!response)
    {
        
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }else if (response[@"error"]){
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"error" Message:[response objectForKey:@"error"]];
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }else if ([[dictResponse objectForKey:@"errFlag"] intValue] == 1){
       // NSDictionary *dictResponse  = response;
         [pi hideProgressIndicator];
         [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"errMsg"]];
    }
    else
    {
       
        
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setObject:response[@"token"] forKey:KDAcheckUserSessionToken];
            [ud setObject:response[@"chn"] forKey:kNSURoadyoPubNubChannelkey];
            [ud setObject:response[@"email"] forKey:kNSUDoctorEmailAddressKey];
            [ud setObject:response[@"name"] forKey:kNSUDoctorNameKey];
            [ud setObject:response[@"subsChn"] forKey:kNSUDoctorSubscribeChanelKey];
            [ud setObject:response[@"fName"] forKey:kNSUDoctorNameKey];
            [ud setObject:response[@"serverChn"] forKey:kNSUDoctorServerChanelKey];
            [ud setObject:response[@"phone"] forKey:kNSUDoctorPhonekey];
            [ud setObject:@"Profilepic.png" forKey:kNSUDoctorProfilePicKey];
            [ud setObject:response[@"typeId"] forKey:kNSUDoctorTypekey];
            [ud setValue:@"3" forKey:@"DoctorStatus"];
            
            
            if (_pickedImage != nil) {
                
                [[ProgressIndicator sharedInstance] changePIMessage:@"Uploading Profile Pic..."];
                [self uploadImage];
            }
            else {
                
                [pi hideProgressIndicator];
                [self gotoHomeScreen];
            }
            
        }
        
    }
}

-(void)gotoHomeScreen{
    
    
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
    
        HomeViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    
     
     
    
    /*
    [self cancelButtonClicked];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Thankyou for your request.In next 24 hours one of our staff will contact you for further process." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
     */
}

-(void)uploadImage{
    
    UploadFiles * upload = [[UploadFiles alloc]init];
    upload.delegate = self;
   [upload uploadImageFile:_pickedImage];
    
}



- (void) validateEmailAndPostalCode
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Validating email.."];

    WebServiceHandler *handler      = [[WebServiceHandler alloc] init];

    NSString *parameters            = [NSString stringWithFormat:@"ent_email=%@&zip_code=%@&ent_user_type=%@&ent_date_time=%@",selectProfessionType.text,zipCodeTextField.text,@"2",[Helper getCurrentDateTime]];

    NSString *removeSpaceFromParameter=[Helper removeWhiteSpaceFromURL:parameters];
    NSLog(@"request doctor around you :%@",parameters);

    NSURL *url                      = [NSURL URLWithString:[NSString stringWithFormat:@"%@validateEmailZip",BASE_URL]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];

    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:[[removeSpaceFromParameter stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]
                             dataUsingEncoding:NSUTF8StringEncoding
                             allowLossyConversion:YES]];

    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(validateEmailResponse:)];

}

- (BOOL)validateZip:(NSString *)candidate {
    NSString *emailRegex = @"(^[0-9]{5}(-[0-9]{4})?$)";   //@"(^{5}(-{4})?$)";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}


-(void)validateEmailResponse :(NSDictionary *)response
{
    /*
    ProgressIndicator *pi  = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    NSLog(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        
        
        NSDictionary *dictResponse=[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            // [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            saveSignUpDetails      = [[NSArray alloc]initWithObjects:firstNameTextField.text,lastNameTextField.text,emailTextField.text,phoneNoTextField.text,zipCodeTextField.text,passwordTextField.text,conPasswordTextField.text, nil];
            
            [self performSegueWithIdentifier:@"CardController" sender:self];
            
        }
        else
        {
            [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
     */
}

- (UIImage *)scaleImage:(UIImage *)image toSize:(CGSize)size
{
    
    UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
    
}


#pragma mark UIButton Action -
- (void)showDatePickerWithTitle:(UITextField*)textFeild
{
	
    NSString *pickerTitle = @"Choose Profession Type";
    actionShet=[[UIActionSheet alloc]initWithTitle:pickerTitle
                                        delegate:nil
                               cancelButtonTitle:nil
                          destructiveButtonTitle:nil
                               otherButtonTitles:nil];
    actionShet.backgroundColor = [UIColor whiteColor];
    
    
    //[self moveViewupwordto:300];
    pickerview = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    pickerview.delegate = self;
    pickerview.dataSource = self;
    pickerview.showsSelectionIndicator = YES;
    pickerview.opaque = NO;
    pickerview.tag = 100;
    [self.view addSubview:pickerview];
    [actionShet addSubview:pickerview];
    UIButton *closeButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [closeButton setTitle:@"Done" forState:UIControlStateNormal];
    [closeButton setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
    
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    
    [closeButton addTarget:self action:@selector(actionSheetDoneButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [actionShet addSubview:closeButton];
    
    UIButton *cancelButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(10, 7.0f, 70.0f, 30.0f);
    
    
    cancelButton.tintColor = [UIColor greenColor];
    [cancelButton addTarget:self action:@selector(dismissActionSheet) forControlEvents:UIControlEventTouchUpInside];
    [actionShet addSubview:cancelButton];
    
    
    [actionShet showInView:[[UIApplication sharedApplication] keyWindow]];
    [actionShet setBounds:CGRectMake(0, 0, 320, 480)];
    
}

/* Added Picker with io8 Support */

- (void)showProfessionTypePicker:(UITextField *)sender{
    
[ActionSheetStringPicker showPickerWithTitle:@"Choose Profession Type"
                                        rows:pickerContent
                            initialSelection:0
                                   doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                       NSLog(@"Picker: %@", picker);
                                      // NSLog(@"Selected Index: %@", selectedIndex);
                                      // NSLog(@"Selected Value: %@", selectedValue);
                                       
                                       doctorType = (int)selectedIndex + 1;   // 1 = Doctor , 2 = Nurse
                                       selectProfessionType.text = pickerContent[selectedIndex];
                                       
                                       [self.phoneNoTextField becomeFirstResponder];
                                       
                                   }
                                 cancelBlock:^(ActionSheetStringPicker *picker) {
                                     NSLog(@"Block Picker Canceled");
                                 }
                                      origin:sender];


}


#pragma mark -
#pragma mark UIPickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (pickerView.tag == 100) {
        return [pickerContent count];
    }
    
    return 0;
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
   return [pickerContent objectAtIndex:row];
    
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
{
    
    //
    //    if (pickerView.tag == 100)
    //    {
    //          Cartype *type = [arrCartype objectAtIndex:row];
    //          //[Helper setToLabel:lblCarType Text:type.type_name WithFont:Robot_Light FSize:11 Color:UIColorFromRGB(0x000000)];
    //
    //    }
    
}

- (void)actionSheetDoneButtonClicked//save
{
    [actionShet dismissWithClickedButtonIndex:1 animated:YES];
    NSInteger selectedIndex = [pickerview selectedRowInComponent:0];
    
    doctorType = (int)selectedIndex + 1;   // 1 = Doctor , 2 = Nurse
    selectProfessionType.text = pickerContent[selectedIndex];
    [self.emailTextField becomeFirstResponder];
    
    }

- (void)dismissActionSheet
{
    
    [actionShet dismissWithClickedButtonIndex:1 animated:YES];
    if (pickerview.tag == 100) {
       // [self findNextResponder:self.activeTextField];
    }
    else {
       
    }
   
}


#pragma mark - TextFields
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
    if ([textField isEqual:selectProfessionType]) {
        
        
        [self.view endEditing:YES];
        [self changeOpenStatus:nil];
        return NO;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == conPasswordTextField)
    {
        int strength = [self checkPasswordStrength:passwordTextField.text];
        if(strength == 0)
        {

            [passwordTextField becomeFirstResponder];
        }
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
//    if(textField == passwordTextField)
//    {
//        int strength = [self checkPasswordStrength:passwordTextField.text];
//        
//        if (strength == 0)
//        {
//            NSLog(@"Not entered ");
//        }
//        else
//        {
//            if(strength <= 3)
//            {
//                NSLog(@"Weak");
//            }
//            else if(3 < strength && strength < 6)
//            {
//                NSLog(@"Moderate");
//                
//            }
//            else
//            {
//                NSLog(@"Strong");
//                
//            }
//
//        }
//    }

}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == firstNameTextField)
    {
        // [textField resignFirstResponder];
        [lastNameTextField becomeFirstResponder];
    }
    else if(textField == lastNameTextField)
    {
          [textField resignFirstResponder];
       // [selectProfessionType becomeFirstResponder];
        [self changeOpenStatus:nil];
    }
    
    
    else if (textField == selectProfessionType)
    {
        // [textField resignFirstResponder];
        [phoneNoTextField becomeFirstResponder];
    }
    
    else if (textField == phoneNoTextField)
    {
        // [textField resignFirstResponder];
        [emailTextField becomeFirstResponder];
        // [self moveViewUp];
    }
    else if ([textField isEqual:emailTextField]){
        [zipCodeTextField becomeFirstResponder];
    }
    else if (textField == zipCodeTextField)
    {
        // [textField resignFirstResponder];
        [passwordTextField becomeFirstResponder];
        // [self moveViewUp];
    }
    else if (textField == passwordTextField)
    {
        // [textField resignFirstResponder];
        int strength = [self checkPasswordStrength:passwordTextField.text];
        if(strength == 0)
        {
            [passwordTextField becomeFirstResponder];
        }
        else
        {
            [conPasswordTextField becomeFirstResponder];

        }
    }
    
    else
    {
        [conPasswordTextField resignFirstResponder];
        [self NextButtonClicked];
        
    }
    return YES;
}


- (void)NextButtonClicked
{
    
    [self signUp];
    //[self performSegueWithIdentifier:@"CardController" sender:self];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    /*
    if ([[segue identifier] isEqualToString:@"CardController"])
    {
      //  CardLoginViewController *CLVC = [[CardLoginViewController alloc]init];
        CardLoginViewController *CLVC = (CardLoginViewController*)[segue destinationViewController];

        CLVC.getSignupDetails = saveSignUpDetails;
        CLVC.pickedImage = _pickedImage;
        for(int i=0;i< saveSignUpDetails.count;i++)
            NSLog(@"contents of array %@ ",[saveSignUpDetails objectAtIndex:i]);
        
      //  CLVC =[segue destinationViewController];
        
    }
    else if ([[segue identifier] isEqualToString:@"gotoTerms"])
    {
        TermsnConditionViewController *TNCVC = (TermsnConditionViewController*)[segue destinationViewController];
        NSLog(@"enabledTypes %@",TNCVC);

    }
     */
}

- (void)cancelButtonClicked
{
   
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)callPop
{
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.375];
    [self.navigationController popViewControllerAnimated:NO];
    [UIView commitAnimations];
}

- (IBAction)TermsNconButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"gotoTerms" sender:self];
}

- (IBAction)checkButtonClicked:(id)sender
{
    UIButton *mBut = (UIButton *)sender;
    // [self setSelectedButtonByIndex:((UIButton *)sender).tag] ;
    
    mBut.userInteractionEnabled = YES;
    
    if(mBut.isSelected)
    {
        isTnCButtonSelected = NO;
        mBut.selected=NO;
        //[tncCheckButton setTitle:@"N" forState:UIControlStateNormal];
        [tncCheckButton setImage:[UIImage imageNamed:@"signup_btn_checkbox_off.png"] forState:UIControlStateNormal];
        [tncCheckButton setImage:[UIImage imageNamed:@"createaccount_checkbox_off"] forState:UIControlStateNormal];

    }
    else
    {
        isTnCButtonSelected = YES;
        mBut.selected=YES;
        //[tncCheckButton setTitle:@"Y" forState:UIControlStateSelected];
        [tncCheckButton setImage:[UIImage imageNamed:@"signup_btn_checkbox_on.png"] forState:UIControlStateSelected];
        [tncCheckButton setImage:[UIImage imageNamed:@"createaccount_checkbox_on"] forState:UIControlStateSelected];
        
    }
}

- (IBAction)profileButtonClicked:(id)sender
{
    UIActionSheet *actionSheet = nil;
    if (!_pickedImage) {
      actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose From Library", nil];
        actionSheet.tag = 1;

    }else{
       actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose From Library",@"Remove Photo", nil];
        actionSheet.tag = 1;

    }
      [actionSheet showInView:self.view];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked:nil];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked:nil];
                break;
            }
            case 2:{
                [self removePhoto];
                return;
            }
            default:
                break;
        }
    }
}

- (void)removePhoto{
    _pickedImage = nil;
    profileImageView.image = [UIImage imageNamed:@"createaccount_imagethumbnail.png"];
    
    
}
-(void)cameraButtonClicked:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate =self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message: @"Camera is not available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
}
-(void)libraryButtonClicked:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    // picker.contentSizeForViewInPopover = CGSizeMake(400, 800);
    //    [self presentViewController:picker animated:YES completion:nil];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        // popover = [[UIPopoverController alloc] initWithContentViewController:picker];
        
        
        
        //[popover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //  _flagCheckSnap = YES;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
     profileImageView.image = image;
    
    CGSize size = CGSizeMake(125, 125);
   _pickedImage = [self scaleImage:image toSize:size];
    
    
    /*
    NSData *data=UIImagePNGRepresentation(_pickedImage);
    NSString *FileName=[NSString stringWithFormat:@"patient.png"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *tempPath = [documentsDirectory stringByAppendingPathComponent:FileName];
    [data writeToFile:tempPath atomically:YES];
    */
}

//-(NSURL *) getImagePathWithURL
//{
//    NSString *strPath= [NSString stringWithFormat:@"NexPanama.png"];
//    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
//    NSString *path = [documentsDirectory stringByAppendingPathComponent:strPath];
//    NSURL *targetURL = [NSURL fileURLWithPath:path];
//    //    NSData *returnData=[NSData dataWithContentsOfURL:targetURL];
//    //    UIImage *imagemain=[UIImage returnData];
//    return targetURL;
//}

#pragma mark - Password Checking

-(int)checkPasswordStrength:(NSString *)password
{
    unsigned long int  len = password.length;
    //will contains password strength
    int strength = 0;
    
    if (len == 0) {
        [Helper showAlertWithTitle:@"Message" Message:@"Please enter password first"];

        return 0;//PasswordStrengthTypeWeak;
    } else if (len <= 5) {
        strength++;
    } else if (len <= 10) {
        strength += 2;
    } else{
        strength += 3;
    }
    int kp = strength;
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_UPPERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Please enter atleast one Uppercase alphabet"];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_LOWERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Please enter atleast one Lowercase alphabet"];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_NUMBER caseSensitive:YES];
    if (kp >= strength) {
        [Helper showAlertWithTitle:@"Message" Message:@"Please enter atleast  one Number"];
       // [passwordTextField becomeFirstResponder];
        return 0;
    }
    return 1;
  //  strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_SYMBOL caseSensitive:YES];
   // if (kp >= strength) {
        //[Helper showAlertWithTitle:@"Message" Message:@"Please enter atleast one special symbol"];
        //[passwordTextField becomeFirstResponder];
       // return 0;
   // }
   
    
//    if(strength <= 3){
//        return PasswordStrengthTypeWeak;
//    }else if(3 < strength && strength < 6){
//        return PasswordStrengthTypeModerate;
//    }else{
//        return PasswordStrengthTypeStrong;
//    }
}

// Validate the input string with the given pattern and
// return the result as a boolean
- (int)validateString:(NSString *)string withPattern:(NSString *)pattern caseSensitive:(BOOL)caseSensitive
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:((caseSensitive) ? 0 : NSRegularExpressionCaseInsensitive) error:&error];
    
    NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    //NSLog(@"test range %ld",textRange);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = 0;
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = 1;
   
    return didValidate;
}

-(void) keyboardWillShow:(NSNotification *)note
{
    if(isKeyboardIsShown)
    {
        return;
    }
    // Get the keyboard size
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
    
    CGSize keyboardSize = [[note.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    // Detect orientation
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect frame = self.mainScrollView.frame;
    
    // Start animation
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Reduce size of the scroll view
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        frame.size.height -= keyboardBounds.size.height;
    else
        frame.size.height -= keyboardSize.width; //not used just for testing
    
    // Apply new size of scroll view
    self.mainScrollView.frame = frame;
    
    // Scroll the scroll view to see the TextField just above the keyboard
    if (self.activeTextField)
    {
        CGRect textFieldRect = [self.mainScrollView convertRect:self.activeTextField.bounds fromView:self.activeTextField];
        [self.mainScrollView scrollRectToVisible:textFieldRect animated:NO];
    }
    [UIView commitAnimations];
    isKeyboardIsShown = YES;
}


-(void) keyboardWillHide:(NSNotification *)note
{
    // Get the keyboard size
    CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
    
    // Detect orientation
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect frame = self.mainScrollView.frame;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    
    // Reduce size of the scroll view
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        frame.size.height += keyboardBounds.size.height;
    else
        frame.size.height += keyboardBounds.size.width;
    
    // Apply new size of scroll view
    
    self.mainScrollView.frame = frame; //CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height);
    
    [UIView commitAnimations];
    isKeyboardIsShown = NO;
    
}

-(void)getCurrentLocation{
    
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled]) {
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_locationManager requestAlwaysAuthorization];
            }
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            
        }
        [_locationManager startUpdatingLocation];
        
        
    }
    else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Service" message:@"Unable to find your location,Please enable location services." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        
    }
    
}

#pragma mark DropDownMenu -
- (void)changeOpenStatus:(id)sender {
    
    if (isOpened) {
        
        
        
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect frame = dropDownBox.frame;
            frame.size.height = 1;
            [dropDownBox setFrame:frame];
            self.btnDropBoxMD.alpha = 0.0f;
            self.btnDropBoxNurse.alpha = 0.0f;
            
           
            
        } completion:^(BOOL finished){
            
            isOpened=NO;
            transparentView.hidden = YES;
        }];
    }else{
        
        [mainScrollView bringSubviewToFront:transparentView];
        
        [mainScrollView insertSubview:selectProfessionType aboveSubview:transparentView];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            
            CGRect frame=dropDownBox.frame;
            
            frame.size.height = 104;
            [dropDownBox setFrame:frame];
            
            transparentView.hidden = NO;
            
        } completion:^(BOOL finished){
            
            isOpened=YES;
            
            self.btnDropBoxMD.alpha = 1.0f;
            self.btnDropBoxNurse.alpha = 1.0f;
        }];
        
        
    }
    
    
}

- (IBAction)dropBoxButtonAction:(UIButton *)sender{
    
    
    if ([sender.titleLabel.text isEqualToString:@"PHYSICIAN (M.D./D.O.)"]) {
        
        doctorType =  1;   // 1 = Doctor , 2 = Nurse
        selectProfessionType.text = @"PHYSICIAN (M.D./D.O.)";
        
        
    }else if ([sender.titleLabel.text isEqualToString:@"PHYSICIAN EXTENDER (P.A./N.P.)"]){
        
        doctorType =  2;
        selectProfessionType.text = @"PHYSICIAN EXTENDER (P.A./N.P.)";
    }
    
    if (selectProfessionType.text.length > 0 || selectProfessionType.text != nil) {
        [self.phoneNoTextField becomeFirstResponder];
    }
    
    [self changeOpenStatus:nil];
}


#pragma mark -CLLocation manager deleagte method
- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation __OSX_AVAILABLE_BUT_DEPRECATED(__MAC_10_6, __MAC_NA, __IPHONE_2_0, __IPHONE_6_0){
    
    _currentLatitude= newLocation.coordinate.latitude;
    _currentLongitude= newLocation.coordinate.longitude;
    [_locationManager stopUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    
}


#pragma UploadFileDelegate

-(void)uploadFile:(UploadFiles *)uploadfile didUploadSuccessfullyWithUrl:(NSArray *)imageUrls
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
  
      NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if (imageUrls.count > 0) {
      
        [ud setObject:imageUrls[0] forKey:kNSUDoctorProfilePicKey];
    }else{
         [ud setObject:@"ProfilePic.png" forKey:kNSUDoctorProfilePicKey];
    }
    
    
     [self gotoHomeScreen];
    
    
}
-(void)uploadFile:(UploadFiles *)uploadfile didFailedWithError:(NSError *)error{
    NSLog(@"upload file  error %@",[error localizedDescription]);
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
}


@end
