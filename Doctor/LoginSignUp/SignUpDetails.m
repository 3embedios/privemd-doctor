//
//  SignUpDetails.m
//  Roadyo
//
//  Created by Surender Rathore on 03/07/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "SignUpDetails.h"

@implementation SignUpDetails
@synthesize firstName;
@synthesize lastName;
@synthesize email;
@synthesize password;
@synthesize confirmPassword;
@synthesize mobile;
@synthesize postCode;
@synthesize compneyId;
@synthesize compneyName;
@synthesize otherCompneyName;
@synthesize taxNumber;
@synthesize profileImage;



static SignUpDetails  *signupdetails;

+ (id)sharedInstance {
	if (!signupdetails) {
		signupdetails  = [[self alloc] init];
	}
	
	return signupdetails;
}

-(void)clear {
    self.firstName = nil;
    self.lastName = nil;
    self.email = nil;
    self.password = nil;
    self.mobile = nil;
    self.postCode = nil;
    self.compneyId = nil;
    self.compneyName = nil;
    self.otherCompneyName = nil;
    self.taxNumber = nil;
    self.profileImage = nil;
    self.confirmPassword = nil;
}

@end
