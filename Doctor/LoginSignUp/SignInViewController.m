//
//  SignInViewController.m
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "SignInViewController.h"
#import "UploadFiles.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "Helper.h"
#import "AppConstants.h"
#import "Service.h"
#import "WebServiceConstants.h"
#import "ProgressIndicator.h"
#import "Fonts.h"
#import "CustomNavigationBar.h"
#import "Login.h"
#import "Errorhandler.h"
@interface SignInViewController ()<CustomNavigationBarDelegate,CLLocationManagerDelegate>
@property(nonatomic,assign)double currentLatitude;
@property(nonatomic,assign)double currentLongitude;
@property(nonatomic,strong)CLLocationManager *locationManager;
@end

@implementation SignInViewController

@synthesize emailTextField;
@synthesize passwordTextField;
@synthesize navCancelButton;
@synthesize navDoneButton;
@synthesize emailImageView;
@synthesize signinButton;
@synthesize loginBlock;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)getCurrentLocation{
    
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled]) {
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_locationManager requestAlwaysAuthorization];
            }
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            
        }
        [_locationManager startUpdatingLocation];
        
        
    }
    else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Service" message:@"Unable to find your location,Please enable location services." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        
    }
    
}

#pragma mark UIViewLifeCycle -

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //mainScroll.contentSize = CGSizeMake(300,600);
    
    if ([Helper isIphone5]) {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg-568h"]];
    }else{
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg.png"]];
    }
    
    self.navigationItem.title = @"SIGN IN";
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = NO;
    
    [self createNavLeftButton];
    
    [emailTextField becomeFirstResponder];
    _fNameLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_textfield.png"]];
    _passwordLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_textfield.png"]];
    
    
    [Helper setButton:signinButton Text:@"SIGN IN" WithFont:Robot_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    [signinButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateHighlighted];
    [signinButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    
    [Helper setButton:_forgotPasswordButton Text:@"Forgot password?" WithFont:Robot_Light FSize:12 TitleColor:UIColorFromRGB(0x000000) ShadowColor:nil];
    [_forgotPasswordButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateNormal];
    [_forgotPasswordButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateHighlighted];
    
    
    [emailTextField setValue:UIColorFromRGB(0x333333)
                  forKeyPath:@"_placeholderLabel.textColor"];
    [passwordTextField setValue:UIColorFromRGB(0x333333)
                     forKeyPath:@"_placeholderLabel.textColor"];
    [_caridTextField setValue:UIColorFromRGB(0x000000)
                   forKeyPath:@"_placeholderLabel.textColor"];
    
    emailTextField.font = [UIFont fontWithName:HELVETICANEUE_LIGHT size:14];
    emailTextField.textColor = UIColorFromRGB(0x000000);
    passwordTextField.font = [UIFont fontWithName:HELVETICANEUE_LIGHT size:14];
    passwordTextField.textColor = UIColorFromRGB(0x000000);
    _caridTextField.font = [UIFont fontWithName:HELVETICANEUE_LIGHT size:14];
    _caridTextField.textColor = UIColorFromRGB(0x000000);
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    // [self addCustomNavigationBar];
    
    _currentLongitude = 0.0;
    _currentLatitude = 0.0;
    
    [Helper setToLabel:self.labelRemberMe Text:@"RememberMe" WithFont:Robot_Light FSize:12 Color:UIColorFromRGB(0x000000)];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud boolForKey:@"RememberMeKey"] == YES) {
        
        self.emailTextField.text = [ud valueForKey:@"UserEmailID"];
        self.passwordTextField.text = [ud valueForKey:@"UserPassword"];
        
        self.btnRememberMe.selected = YES;
    }else{
        self.emailTextField.text = @"";
        self.passwordTextField.text = @"";
        self.btnRememberMe.selected = NO;
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [self getCurrentLocation];
}

- (void)viewDidDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
}

- (void)createNavLeftButton
{
    //UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [cancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,60,30)];
    
    [Helper setButton:cancelButton Text:@"Cancel" WithFont:Robot_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateSelected];
    [cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateHighlighted];
    cancelButton.titleLabel.font = [UIFont fontWithName:ZURICH_LIGHTCONDENSED size:15];
    // [cancelButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
    
    //Adding Button onto View
    // [navView addSubview:navCancelButton];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:cancelButton];
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
    
    
}
-(void)createnavRightButton
{
    navDoneButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navDoneButton addTarget:self action:@selector(DoneButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [navDoneButton setFrame:CGRectMake(0.0f,0.0f, 60,30)];
    
    [Helper setButton:navDoneButton Text:@"Done" WithFont:@"HelveticaNeue" FSize:17 TitleColor:[UIColor blueColor] ShadowColor:nil];
    
    
    [navDoneButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    // Create a container bar button
    UIBarButtonItem *containingnextButton = [[UIBarButtonItem alloc] initWithCustomView:navDoneButton];
    
    self.navigationItem.rightBarButtonItem = containingnextButton;
    
    
    
    
    
}

- (void) addCustomNavigationBar{
    
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:@"Sign In"];
    [customNavigationBarView setLeftBarButtonTitle:@"Back"];
    
    [self.view addSubview:customNavigationBarView];
    
    
    
}

-(void)leftBarButtonClicked:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dismissKeyboard
{
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    
}

-(void)cancelButtonClicked
{
    //[self.navigationController popViewControllerAnimated:YES];
    //[self callPop];
    
    
    if (self.loginBlock) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}


-(void)callPop
{
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.375];
    [self.navigationController popViewControllerAnimated:NO];
    [UIView commitAnimations];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)signInUser
{
    
    
    NSString *email = emailTextField.text;
    NSString *password = passwordTextField.text;
    
    if((unsigned long)email.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Email ID"];
        [emailTextField becomeFirstResponder];
    }
    
    else if((unsigned long)password.length == 0)
    {
        [Helper showAlertWithTitle:@"Message" Message:@"Enter Password"];
        [passwordTextField becomeFirstResponder];
    }
    else if([self emailValidationCheck:email] == 0)
    {
        //email is not valid
        [Helper showAlertWithTitle:@"Message" Message:@"Invalid Email ID"];
        emailTextField.text = @"";
        [emailTextField becomeFirstResponder];
        
    }
    else
    {
        [self.view endEditing:YES];
        checkLoginCredentials = YES;
        [self sendServiceForLogin];
        
    }
    
}



-(void)DoneButtonClicked
{
    
    
    
    [self signInUser];
}

- (void)sendServiceForLogin
{
    
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:@"Signing in"];
    //  ResKitWebService * restKit = [ResKitWebService sharedInstance];
    
    NSString * pushToken;
    if([[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken])
    {
        pushToken =[[NSUserDefaults standardUserDefaults]objectForKey:kSMPgetPushToken];
        
    }
    else
    {
        pushToken =@"dgfhfghr765998ghghj";
        
    }
    
    
    
    NSDictionary *queryParams;
    queryParams = [NSDictionary dictionaryWithObjectsAndKeys: emailTextField.text, kSMPLoginEmail,
                   passwordTextField.text, kSMPLoginPassword,
                   [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPLoginDevideId,
                   pushToken,kSMPLoginPushToken,
                   @"1",kSMPLoginDeviceType,
                   [NSNumber numberWithDouble:_currentLatitude],kSMPSignUpLattitude,
                   [NSNumber numberWithDouble:_currentLongitude],kSMPSignUpLongitude,
                   [Helper getCurrentDateTime], kSMPCommonUpDateTime,
                   nil];
    
    
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient postPath:MethodPatientLogin parameters:queryParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self loginResponse:operation.responseString.JSONValue];
        // NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        //NSLog(@"Request Successful, response '%@'", responseStr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];
    
    
    
}

- (void)loginResponse:(NSDictionary *)response{
    
  //  NSLog(@"response:%@",response);
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    if (!response)
    {
        
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }else if (response[@"error"]){
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"error" Message:[response objectForKey:@"error"]];
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setObject:response[@"token"] forKey:KDAcheckUserSessionToken];
            [ud setObject:response[@"chn"] forKey:kNSURoadyoPubNubChannelkey];
            [ud setObject:response[@"subsChn"] forKey:kNSUDoctorSubscribeChanelKey];
            [ud setObject:response[@"email"] forKey:kNSUDoctorEmailAddressKey];
            [ud setObject:response[@"profilePic"] forKey:kNSUDoctorProfilePicKey];
            [ud setObject:response[@"fName"] forKey:kNSUDoctorNameKey];
            [ud setObject:response[@"typeId"] forKey:kNSUDoctorTypekey];
            [ud setObject:response[@"phone"] forKey:kNSUDoctorPhonekey];
            [ud setObject:response[@"serverChn"] forKey:kNSUDoctorServerChanelKey];
            
            [ud setObject:self.emailTextField.text forKey:@"UserEmailID"];
            [ud setObject:self.passwordTextField.text forKey:@"UserPassword"];
            
            if (self.btnRememberMe.selected) {
                [ud setBool:YES forKey:@"RememberMeKey"];
            }else{
                [ud setBool:NO forKey:@"RememberMeKey"];
            }
             [ud setValue:@"3" forKey:@"DoctorStatus"];
            
            [ud synchronize];
            
            
            
            if (self.loginBlock) {
                
                [self dismissViewControllerAnimated:YES completion:^{
                    self.loginBlock(YES);
                }];
            }
            else {
                
                
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                            @"Main" bundle:[NSBundle mainBundle]];
                
                MenuViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
                self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
                
            }
        }
        else {
            
            [pi hideProgressIndicator];
            [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
        }
        
    }
    
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    //     MapViewController *MPC = [[MapViewController alloc]init];
    //      MPC =[segue destinationViewController];
    
    
}

#pragma mark - TextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if(textField == emailTextField)
    {
        [_caridTextField becomeFirstResponder];
    }
    else if(textField == _caridTextField){
        [passwordTextField becomeFirstResponder];
    }
    else  {
        
        [passwordTextField resignFirstResponder];
        [self DoneButtonClicked];
        
    }
    return YES;
    
}


- (IBAction)forgotPasswordButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    
    UIAlertView *forgotPasswordAlert = [[UIAlertView alloc]
                                        initWithTitle:@"Can't sign in? "
                                        message:@"Enter your email address below and we will send you password reset instruction."
                                        delegate:self
                                        cancelButtonTitle:@"Cancel"
                                        otherButtonTitles:@"Submit", nil];
    
    UITextField *emailForgot = [[UITextField alloc]initWithFrame:CGRectMake(30, 100, 225, 30)];
    emailForgot.delegate =self;
    
    emailForgot.placeholder = @"Email";
    
    emailImageView.frame = CGRectMake(220,110,18,18);
    
    [forgotPasswordAlert addSubview:emailForgot];
    [forgotPasswordAlert addSubview:emailImageView];
    
    forgotPasswordAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [forgotPasswordAlert show];
    
    //forgotView.hidden=NO;
    NSLog(@"password recovery to be done here");
    
    
}

- (IBAction)signInButtonClicked:(id)sender {
    [self signInUser];
}

- (IBAction)buttonRememberMeTapped:(UIButton *)sender {
    
    
    
    if (sender.selected) {
        [sender setSelected:NO];
        
        
    }else{
        [sender setSelected:YES];
        
    }
}
- (void) forgotPaswordAlertviewTextField
{
    UIAlertView *forgotPasswordAlert = [[UIAlertView alloc]
                                        initWithTitle:@"Invalid Email ID"
                                        message:@"Reenter your email ID "
                                        delegate:self
                                        cancelButtonTitle:@"Cancel"
                                        otherButtonTitles:@"Submit", nil];
    forgotPasswordAlert.tag = 1;
    UITextField *emailForgot = [[UITextField alloc]initWithFrame:CGRectMake(30, 100, 225, 30)];
    
    
    [forgotPasswordAlert addSubview:emailForgot];
    
    forgotPasswordAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [forgotPasswordAlert show];
    
    
}


#pragma mark UIAlertView Delegate -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex == 1)
    {
        UITextField *forgotEmailtext = [alertView textFieldAtIndex:0];
        NSLog(@"Email Name: %@", forgotEmailtext.text);
        
        if (((unsigned long)forgotEmailtext.text.length ==0) || [Helper emailValidationCheck:forgotEmailtext.text] == 0)
        {
            [self forgotPaswordAlertviewTextField];
        }
        else
        {
            [self retrievePassword:forgotEmailtext.text];
        }
    }
    
    else
    {
        NSLog(@"cancel");
        // emailTextField.text = @"";
        passwordTextField.text = @"";
    }
    
}

- (void)retrievePassword:(NSString *)text
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    
    NSString *parameters = [NSString stringWithFormat:@"ent_email=%@&ent_user_type=%@",text,@"1"];
    
    NSString *removeSpaceFromParameter=[Helper removeWhiteSpaceFromURL:parameters];
    NSLog(@"request doctor around you :%@",parameters);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@forgotPassword",BASE_URL]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:[[removeSpaceFromParameter stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]
                             dataUsingEncoding:NSUTF8StringEncoding
                             allowLossyConversion:YES]];
    
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(retrievePasswordResponse:)];
    
}

-(void)retrievePasswordResponse :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    NSLog(@"response:%@",response);
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        
        
        NSDictionary *dictResponse=[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
        }
        else
        {
            [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
}



- (BOOL) emailValidationCheck: (NSString *) emailToValidate{
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:emailToValidate];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    
    [_locationManager stopUpdatingLocation];
    _currentLatitude = newLocation.coordinate.latitude;
    _currentLongitude = newLocation.coordinate.longitude;
    
    
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    
}


@end
