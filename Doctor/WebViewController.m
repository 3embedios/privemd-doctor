//
//  WebViewController.m
//  Loupon
//
//  Created by Rahul Sharma on 08/01/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

@synthesize webView,weburl;
@synthesize backButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)backButtonclicked
{

    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}


- (void) addCustomNavigationBar
{
    
    UIView *customNavigationBarView = nil;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0.0"))
    {
        customNavigationBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 64)];
    
    }else{
        customNavigationBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    }
    
    // Add navigationbar item
   
    customNavigationBarView.backgroundColor = [UIColor lightGrayColor];
    
    //Add title
    UILabel *labelTitle = nil;
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0.0"))
    {
        labelTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, 320, 44)];
    }
    else
    {
        labelTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    }
    
    [Helper setToLabel:labelTitle Text:@"Terms and Condition" WithFont:@"HelveticaNeue" FSize:22 Color:[UIColor whiteColor]];
    [labelTitle setTextAlignment:NSTextAlignmentCenter];
    [customNavigationBarView addSubview:labelTitle];
    
    //Add right Navigation button
    
    UIButton *leftNavButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0.0"))
    {
        leftNavButton.frame = CGRectMake(10, 20,50, 44);
    }
    else
    {
        leftNavButton.frame = CGRectMake(10, 0, 50, 44);
    }
    
    [Helper setButton:leftNavButton Text:@"Back" WithFont:@"HelveticaNeue" FSize:15 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    [leftNavButton addTarget:self action:@selector(gotoRootViewController) forControlEvents:UIControlEventTouchUpInside];
    [customNavigationBarView addSubview:leftNavButton];
    [self.view addSubview:customNavigationBarView];
    
    
    
}


-(void)gotoRootViewController
{
    //[self.navigationController dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self addCustomNavigationBar];
    
    PriveMdAppDelegate *delegate = (PriveMdAppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate customizeNavigationLeftButton:self];

    
    webView.delegate= self;
    NSURL *url = [NSURL URLWithString:weburl];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:@""];
    
}

- (void)viewDidUnload
{
  // [super viewDidUnload];
  //Release any retained subviews of the main view.
  // e.g. self.myOutlet = nil;
  //  self.webView = nil;
 
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.webView stopLoading];
    self.webView.delegate = nil;
    
   
}

#pragma mark Other Methods -
- (void)buttonBackTapped{
    NSLog(@"button back taped");
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:@"Loading.."];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
}


@end
