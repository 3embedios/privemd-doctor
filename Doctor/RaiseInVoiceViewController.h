//
//  RaiseInVoiceViewController.h
//  Doctor
//
//  Created by Rahul Sharma on 27/03/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RaiseInVoiceViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *txtViewRaiseInvoice;
@property(strong, nonatomic) NSMutableDictionary *dictAppointmentDetails;
@end
