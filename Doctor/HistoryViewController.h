//
//  HistoryViewController.h
//  Roadyo
//
//  Created by Rahul Sharma on 23/07/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryViewController : UIViewController

@property (strong, nonatomic)NSArray *historys;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) NSString *patientEmail;
@end
