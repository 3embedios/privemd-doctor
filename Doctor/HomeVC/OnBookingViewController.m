//
//  OnBookingViewController.m
//  Roadyo
//
//  Created by Rahul Sharma on 01/05/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "OnBookingViewController.h"
#import "CustomNavigationBar.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "BookingHistoryViewController.h"
#import "DirectionService.h"
#import <GoogleMaps/GoogleMaps.h>
#import "TELogger.h"


@interface OnBookingViewController ()<CustomNavigationBarDelegate,GMSMapViewDelegate,CLLocationManagerDelegate>
{
    GMSMapView *mapView_;
    GMSGeocoder *geocoder_;
    NSMutableArray *waypoints_;
    NSMutableArray *waypointStrings_;
}
@property(nonatomic,strong)NSString *startLocation;
@property(nonatomic,strong)NSString *destinationLocation;
@property(nonatomic,assign)CLLocationCoordinate2D destinationCoordinates;
@property(nonatomic,assign)CLLocationCoordinate2D pickupCoordinates;
@property(strong,nonatomic) IBOutlet UIView *customMapView;
@property(nonatomic,strong)GMSMarker *destinationMarker;
@property(nonatomic,assign) float currentLatitude;
@property(nonatomic,assign) float currentLongitude;
@property(nonatomic,assign) BOOL isUpdatedLocation;
@property(nonatomic,assign)CLLocationCoordinate2D previouCoord;
@property(nonatomic,strong)NSMutableDictionary *allMarkers;
@property(nonatomic,strong)CLLocationManager *locationManager;
@property(nonatomic,assign)BOOL isPathPlotted;
@property(nonatomic,assign)BOOL isDriverArrived;

@property(strong,nonatomic) IBOutlet UIView *topView;
@property(strong,nonatomic) IBOutlet UIView *bottomView;


@property(strong,nonatomic) NSString *statusButtonTitle;
@property(assign,nonatomic) int statusButtonTag;
@end

@implementation OnBookingViewController
@synthesize customMapView;
@synthesize passDetail;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)loadView {
    
    [self inilizeMap];
    
}
-(void)inilizeMap{
    waypoints_ = [[NSMutableArray alloc]init];
    waypointStrings_ = [[NSMutableArray alloc]initWithCapacity:2];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:37.778376
                                                            longitude:-122.409853
                                                                 zoom:13];
    //UIView *map = [[UIView alloc] initWithFrame:CGRectMake(0, 200, 320, 300)];
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView_.delegate = self;
    self.view = mapView_;
}

#pragma mark-view Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    
    
    if (IS_IPHONE_5) {
    
    }
    else{
        
        customMapView.frame =CGRectMake(0, 161, 320, 300);
        viewStatus.frame = CGRectMake(0, 141+300, 320, 300);
        
    }
   

    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForXXHDPIImage,passDetail[@"pPic"]];
    
    [imgPass setImageWithURL:[NSURL URLWithString:strImageUrl]
                    placeholderImage:[UIImage imageNamed:@"doctor_image_thumbnail.png"]
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                           }];
    
    
    
    
    [self getCurrentLocation];
    
    [self createTopView];
    
    
    
    //check for booking status
    if (_bookingStatus == kNotificationTypeBookingOnTheWay) {
        
        _statusButtonTitle = @"I HAVE ARRIVED";
        self.title = _statusButtonTitle;
        _statusButtonTag = 500;
        [self changePickUpAddressOnUI];
    }
    else if (_bookingStatus == kNotificationTypeBookingArrived) {
        
        _statusButtonTitle = @"PASSENGER DROPPED";
        self.title = _statusButtonTitle;
        _statusButtonTag = 600;
        [self changeDropAddressOnUI];
        
    }
    else if (_bookingStatus == kNotificationTypeBookingDropped) {
        
    }
	
    [self createBottomView];
    
    
    
    
    
    
    
}
- (void)viewWillDisappear:(BOOL)animated{
    
    
    [_allMarkers removeAllObjects];
    _isUpdatedLocation = NO;
    //[self stopPubNubStream];
    //self.navigationController.navigationBarHidden = YES;
//    [mapView_ removeObserver:self
//                  forKeyPath:@"myLocation"
//                     context:NULL];
    
    
}

- (void) viewDidAppear:(BOOL)animated{
    
    
    // [self addgestureToMap];
    
    if (IS_SIMULATOR) {
        _currentLatitude = 13.028866;
        _currentLongitude = 77.589760;
        
        CLLocationCoordinate2D locationCord = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
        
        mapView_.camera = [GMSCameraPosition cameraWithTarget:locationCord
                                                         zoom:14];
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
        marker.title = @"It's Me";
        marker.snippet = @"Current Location";
        marker.map = mapView_;
        
        
        //subscribe to pubnub to get stream of doctors location
        //[self startPubNubStream];
        
    }
    else {
        
        NSLog(@"latitude %f",[mapView_.myLocation coordinate].latitude);
        [self.view bringSubviewToFront:viewPassDetail];
        //[self.view bringSubviewToFront:_bottomView];
    }
    
    
    /*
     //test
     AppointedDoctor *appointedDoctor = [[AppointedDoctor alloc] init];
     appointedDoctor.doctorName      =   @"Surender";
     appointedDoctor.estimatedTime   =   @"12:00";
     appointedDoctor.appoinmentDate  =   @"11/11/11";
     appointedDoctor.distance        =   @"12 m";
     appointedDoctor.contactNumber   =   @"8050023645";
     appointedDoctor.profilePicURL   =   @"";
     appointedDoctor.email           =   @"surender1101@gmail.com";
     appointedDoctor.status          =   @"I am on the way";
     
     NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:appointedDoctor];
     [[NSUserDefaults standardUserDefaults] setObject:encodedObject forKey:kNSUAppoinmentDoctorDetialKey];
     [[NSUserDefaults standardUserDefaults] synchronize];
     
     DoctorDetailView *docDetailView = [[DoctorDetailView alloc] initWithButtonTitle:@"hello"];
     //[docDetailView initWithButtonTitle:@"hello"];
     [docDetailView show];
     
     [docDetailView updateStatus:@"reached"]; */
    
}

#pragma mark- Custom Methods

- (void) addCustomNavigationBar{
    
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:@"Pick Up"];
    [customNavigationBarView setLeftBarButtonTitle:@"Back"];
    [self.view addSubview:customNavigationBarView];
    
}
/**
 *  create a bottom button
 */
-(void)createBottomView {
    
    _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height-40, 320, 40)];
    [_bottomView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_bottomView];
    
    UIButton *buttonDriverState = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonDriverState.frame = CGRectMake(0, 0, 320, 40);
    buttonDriverState.tag = _statusButtonTag;

    [buttonDriverState setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateHighlighted];
    [buttonDriverState setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateNormal];
    [buttonDriverState setBackgroundImage:[UIImage imageNamed:@"arrive_status_bg"] forState:UIControlStateNormal];
    [buttonDriverState setBackgroundImage:[UIImage imageNamed:@"arrive_status_bg_selected.png"] forState:UIControlStateHighlighted];
    
    [buttonDriverState addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [buttonDriverState setTitle:_statusButtonTitle forState:UIControlStateNormal];
    [_bottomView addSubview:buttonDriverState];
}

/**
 *  create a view with all the booking details
 */
-(void)createTopView {
    
    _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 65, 320, 115)];
    [_topView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_topView];
    
    
    UIImageView *profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 100, 100)];
    profileImageView.image = [UIImage imageNamed:@"Icon-60"];
    [_topView addSubview:profileImageView];
    
    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForXXHDPIImage,passDetail[@"pPic"]];
    
    [profileImageView setImageWithURL:[NSURL URLWithString:strImageUrl]
                placeholderImage:[UIImage imageNamed:@"doctor_image_thumbnail.png"]
                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                       }];
    
    //Name
    UILabel *labelPassengerName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(profileImageView.frame)+5, 5,40,20)];
    [Helper setToLabel:labelPassengerName Text:@"Name :" WithFont:Robot_Bold FSize:10 Color:UIColorFromRGB(0x333333)];
   // [_topView addSubview:labelPassengerName];
    
    
    UILabel *labelPassengerNameValue = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(profileImageView.frame)+5, 5, 200, 20)];
    labelPassengerNameValue.backgroundColor = [UIColor blueColor];
    [Helper setToLabel:labelPassengerNameValue Text:[NSString stringWithFormat:@"%@ %@",passDetail[@"fName"],passDetail[@"lName"]].uppercaseString WithFont:Robot_CondensedRegular FSize:13 Color:UIColorFromRGB(0x333333)];
    [_topView addSubview:labelPassengerNameValue];
    
    
    //Mobile
    UILabel *labelMobile = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(profileImageView.frame)+5, CGRectGetMaxY(labelPassengerName.frame)+ 5,30, 20)];
    [Helper setToLabel:labelMobile Text:@"Mob :" WithFont:Robot_Bold FSize:10 Color:UIColorFromRGB(0x333333)];
   // [_topView addSubview:labelMobile];
    
    
    UIButton *buttonMobileValue = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonMobileValue.frame = CGRectMake(CGRectGetMaxX(profileImageView.frame)+5, CGRectGetMaxY(labelPassengerNameValue.frame) + 5, 80, 20);
    buttonMobileValue.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [Helper setButton:buttonMobileValue Text:passDetail[@"mobile"] WithFont:Robot_CondensedRegular FSize:13 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    [buttonMobileValue setTitleColor:[UIColor colorWithRed:0.162 green:0.554 blue:0.839 alpha:1.000] forState:UIControlStateNormal];
    [buttonMobileValue setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    [buttonMobileValue addTarget:self action:@selector(contactButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:buttonMobileValue];
    
    //Address
    UILabel *labelAddress = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(profileImageView.frame)+5,CGRectGetMaxY(labelMobile.frame)+5,40, 20)];
    [Helper setToLabel:labelAddress  Text:@"PickUp :"WithFont:Robot_Bold FSize:10 Color:UIColorFromRGB(0x333333)];
    labelAddress.tag = 210;
    [_topView addSubview:labelAddress];
    
    
    UILabel *labelAddressValue = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(labelPassengerName.frame)+5, CGRectGetMaxY(labelMobile.frame)+5, 160, 40)];
    labelAddressValue.numberOfLines = 2;
    labelAddressValue.tag = 211;
    [Helper setToLabel:labelAddressValue Text:passDetail[@"addr1"] WithFont:Robot_CondensedRegular FSize:13 Color:UIColorFromRGB(0x333333)];
    [_topView addSubview:labelAddressValue];
    
    
//    //distance strip view
//    UIView *distanceTimeView = [[UIView alloc] initWithFrame:CGRectMake(0, 110, 320, 20)];
//    distanceTimeView.backgroundColor = [UIColor grayColor];
//    [_topView addSubview:distanceTimeView];
//    
//    //distance
//    UILabel *labelDistance = [[UILabel alloc] initWithFrame:CGRectMake(0,0, 80, 20)];
//    [Helper setToLabel:labelDistance Text:@"Distance:" WithFont:Robot_Light FSize:12 Color:UIColorFromRGB(0xffffff)];
//    
//    [distanceTimeView addSubview:labelDistance];
//    
//    
//
//    UILabel *labelDistanceValue = [[UILabel alloc] initWithFrame:CGRectMake(80,0, 80, 20)];
//    [Helper setToLabel:labelDistanceValue Text:[NSString stringWithFormat:@"%@ mile",passDetail[@"dis"]] WithFont:Robot_Regular FSize:12 Color:UIColorFromRGB(0xffffff)];
//    [distanceTimeView addSubview:labelDistanceValue];
//    
//    
//    //time
//    UILabel *labelTime = [[UILabel alloc] initWithFrame:CGRectMake(160, 0,80, 20)];
//    [Helper setToLabel:labelTime Text:@"ETA:" WithFont:Robot_Light FSize:12 Color:UIColorFromRGB(0xffffff)];
//    [distanceTimeView addSubview:labelTime];
//    
//    
//
//    UILabel *labeTimeValue = [[UILabel alloc] initWithFrame:CGRectMake(240,0, 80, 20)];
//    [Helper setToLabel:labeTimeValue Text:[NSString stringWithFormat:@"%@ min",passDetail[@"dur"]] WithFont:Robot_Regular FSize:12 Color:UIColorFromRGB(0xffffff)];
//    [distanceTimeView addSubview:labeTimeValue];


}

-(void)contactButtonClicked:(id)sender{
    TELogInfo(@"DETAILS contactButtonClicked CLICKED");
    
    UIButton *button = (UIButton *)sender;
    NSString *buttonTitle = button.currentTitle;
    
    TELogInfo(@"PhnNo. :%@",buttonTitle);
    NSString *number = [NSString stringWithFormat:@"%@",@"9738929033"];
    TELogInfo(@"PhnNo. :%@",number);
    NSURL* callUrl=[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",number]];
    
    //check  Call Function available only in iphone
    if([[UIApplication sharedApplication] canOpenURL:callUrl])
    {
        [[UIApplication sharedApplication] openURL:callUrl];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"ALERT" message:@"This function is only available on the iPhone"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}
/**
 *  changes address when drivers reached at pickup location and shows drop location
 */
-(void)changeDropAddressOnUI{
    
    UILabel *labelAddress = (UILabel*)[_topView viewWithTag:210];
    UILabel *labelAddressValue = (UILabel*)[_topView viewWithTag:211];
    
    labelAddress.text = @"Drop :";
    labelAddressValue.text = passDetail[@"dropAddr1"];//[NSString stringWithContentsOfFile:passDetail[@"dropAddr1"] encoding:NSUTF32StringEncoding error:nil];
}
-(void)changePickUpAddressOnUI{
    
    UILabel *labelAddress = (UILabel*)[_topView viewWithTag:210];
    UILabel *labelAddressValue = (UILabel*)[_topView viewWithTag:211];
    self.title = _statusButtonTitle;
    labelAddress.text = @"Pickup :";
    labelAddressValue.text = passDetail[@"addr1"];
}

#pragma mark - Map direction

-(void)getCurrentLocation{
    
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled]) {
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            
            [_locationManager requestAlwaysAuthorization];
        }
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [_locationManager startUpdatingLocation];
        
    }
    else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Service" message:@"Unable to find your location,Please enable location services." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
}


/**
 *  adds direction on Map
 *
 *  @param json representaion of pathpoints
 */
- (void)addDirections:(NSDictionary *)json {
    
    if ([json[@"routes"] count]>0) {
        NSDictionary *routes = [json objectForKey:@"routes"][0];
        
        NSDictionary *route = [routes objectForKey:@"overview_polyline"];
        NSString *overview_route = [route objectForKey:@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        polyline.map = mapView_;
    }
    
    
}

/**
 *  Sets the start location of Path
 *
 *  @param latitude  start point latitude
 *  @param longitude start point longitude
 */
-(void)setStartLocationCoordinates:(float)latitude Longitude:(float)longitude{
    //change map camera postion to current location
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:15];
    [mapView_ setCamera:camera];
    
    
    //add marker at current location
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    marker.map = mapView_;
    marker.icon = [UIImage imageNamed:@"map_icn_location.png"];
    [waypoints_ addObject:marker];
    
    
    //save current location to plot direciton on map
    _startLocation = [NSString stringWithFormat:@"%.6f,%.6f",latitude, longitude];
    [waypointStrings_ insertObject:_startLocation atIndex:0];
    
    
    if (_isDriverArrived) {
        
        [self changeDropAddressOnUI];
        [waypoints_ removeLastObject];
        [waypointStrings_ removeLastObject];
        _isPathPlotted = NO;
        
//        CLLocationCoordinate2D destination;
//        destination.latitude = [passDetail[@"dropLat"] doubleValue];
//        destination.longitude = [passDetail[@"dropLong"] doubleValue];
//        CLLocationCoordinate2D destination = [self geoCodeUsingAddress:passDetail[@"dropAddr1"]];
        [self updateDestinationLocationWithLatitude:[passDetail[@"dropLat"] doubleValue]Longitude:[passDetail[@"dropLong"] doubleValue]];
    }
    else {
        
       // CLLocationCoordinate2D destination = [self geoCodeUsingAddress:passDetail[@"addr1"]];
        [self updateDestinationLocationWithLatitude:[passDetail[@"pickLat"] doubleValue]Longitude:[passDetail[@"pickLong"] doubleValue]];
    }
    
   
    
    
}

/**
 * sets destination point of path
 *
 *  @param latitude  destination point latitude
 *  @param longitude destination point longitude
 */
-(void)updateDestinationLocationWithLatitude:(float)latitude Longitude:(float)longitude{
    
    
    if (!_isPathPlotted) {
        
        
        
        NSLog(@"pathplotted");
        _isPathPlotted = YES;
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(
                                                                     latitude,
                                                                     longitude);
        
        _previouCoord = position;
        
        _destinationMarker = [GMSMarker markerWithPosition:position];
        _destinationMarker.map = mapView_;
        _destinationMarker.flat = YES;
        //_destinationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        //_destinationMarker.icon = [UIImage imageNamed:@"car.png"];
        
        [waypoints_ addObject:_destinationMarker];
        NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",
                                    latitude,longitude];
        
        [waypointStrings_ addObject:positionString];
        
        if([waypoints_ count]>1){
            NSString *sensor = @"false";
            NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                                   nil];
            NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
            NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                              forKeys:keys];
            DirectionService *mds=[[DirectionService alloc] init];
            SEL selector = @selector(addDirections:);
            [mds setDirectionsQuery:query
                       withSelector:selector
                       withDelegate:self];
        }
        
    }
    else {
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        CLLocationDirection heading = GMSGeometryHeading(_previouCoord, position);
        _previouCoord = position;
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        _destinationMarker.position = position;
        [CATransaction commit];
        if (_destinationMarker.flat) {
            _destinationMarker.rotation = heading;
        }
        
    }
    
    
    
}


#pragma mark - CLLocationManager Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    
    if (!_isUpdatedLocation) {
        
        [_locationManager stopUpdatingLocation];
        
        //change flag that we have updated location once and we don't need it again
        _isUpdatedLocation = YES;
        
        [self setStartLocationCoordinates:newLocation.coordinate.latitude Longitude:newLocation.coordinate.longitude];
        
        
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"locationManager failed to update location : %@",[error localizedDescription]);
}


/**
 *  To get the coordinated of string address
 *
 *  @param address location address
 *
 *  @return address coordinates
 */
- (CLLocationCoordinate2D)geoCodeUsingAddress:(NSString *)address
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
   // _destinationCoordinates = center;
    
    //[self requestForDoctorAroundYou:_appointmentType];
    //return center;
}




#pragma mark- GMSMapviewDelegate


- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    
}
- (void) mapView:(GMSMapView *) mapView willMove:(BOOL)gesture{
    
    NSLog(@"willMove");
    
}

- (void) mapView:(GMSMapView *) mapView didChangeCameraPosition:(GMSCameraPosition *) position{
    
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
    NSLog(@"idleAtCameraPosition");
    
   
}
- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    
    [self performSegueWithIdentifier:@"doctorDetails" sender:marker];
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    
    NSLog(@"marker userData %@",marker.userData);
    
    return nil;
}
- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker{
    NSLog(@"didBeginDraggingMarker");
}
- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    NSLog(@"didEndDraggingMarker");
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (!_isUpdatedLocation) {
        // If the first location update has not yet been recieved, then jump to that
        // location.
        _isUpdatedLocation = YES;
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                         zoom:14];
    }
}

#pragma mark -leftbar and righbar action

-(void)rightBarButtonClicked:(UIButton *)sender{
    
    //[self gotolistViewController];
}

-(void)leftBarButtonClicked:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)buttonAction:(id)sender
{
    ProgressIndicator * pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:@"Status Upadating.."];
    
    [self sendRequestForUpdateStatus:sender];
    
}

-(void)sendRequestForUpdateStatus:(UIButton*)sender
{
    
    if (![sender isKindOfClass:[UIButton class]])
    return;
    ProgressIndicator * pi = [ProgressIndicator sharedInstance];
    NSString *title = [(UIButton *)sender currentTitle];
    NSLog(@"ttle%@",title);
    ResKitWebService * restkit = [ResKitWebService sharedInstance];
    NSDictionary * dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"PUSH"];
    NSDictionary * dictP = [dict objectForKey:@"aps"];
       NSDictionary *queryParams;
    if(sender.tag == 500)
    {
   
         queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                   [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                   [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                   dictP[@"e"],kSMPRespondPassengerEmail,
                   dictP[@"dt"], kSMPRespondBookingDateTime,
                   constkNotificationTypeBookingArrived,kSMPRespondResponse,
                   @"testing",kSMPRespondDocNotes,
                   [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    
    }
    else{
     
        queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                       [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                       [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                       dictP[@"e"],kSMPRespondPassengerEmail,
                       dictP[@"dt"], kSMPRespondBookingDateTime,
                       constkNotificationTypeBookingDropped,kSMPRespondResponse,
                       @"testing",kSMPRespondDocNotes,
                       [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    }
    
    [restkit composeRequestForUpdateStatus:MethodupdateApptStatus
                             paramas:queryParams
                        onComplition:^(BOOL success, NSDictionary *response){
                            
                            if (success) { //handle success response
                                //[self updateStstusResponse:(NSArray*)response];
                                
                                Errorhandler * handler = [(NSArray*)response objectAtIndex:0];
                                ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
                                [progressIndicator hideProgressIndicator];
                                
                                if ([[handler errFlag] intValue] ==0)
                                {
                                    if(sender.tag == 500)
                                    {
                              
                                        [sender setTitle:@"PASSENGER DROPPED" forState:UIControlStateNormal];
                                     // [Helper setButton:sender Text:@"PASSENGER DROPPED" WithFont:Robot_Bold FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
                                        [pi hideProgressIndicator];
                                        
                                        sender.tag = 600;
                                        _isPathPlotted = NO;
                                        _isUpdatedLocation = NO;
                                        _isDriverArrived = YES;
                                        [mapView_ clear];
                                        [self getCurrentLocation];
                                        
                                       
                                        
                                    }
                                    else if (sender.tag == 600){
                                        
                                         [pi hideProgressIndicator];
                                         [self performSegueWithIdentifier:@"GoToBookingHistory" sender:self];
                                    }
                                   
                                    
                                  
                                    
                                    
                                }
                                else{
                                    if ([handler.errNum intValue] == 41) {
                                        [self.navigationController popToRootViewControllerAnimated:YES];
                                    }
                                    
                                    [Helper showAlertWithTitle:@"Message" Message:handler.errMsg];
                                  
                                }

                                
                                
                            }
                            else{//error
                                
                            }
                        }];
    
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"GoToBookingHistory"])
    {
        BookingHistoryViewController *booking = (BookingHistoryViewController*)[segue destinationViewController];
        booking.passDetail=passDetail;
        
        
    }
}

@end
