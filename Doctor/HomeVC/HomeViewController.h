//
//  HomeViewController.h
//  Doctor
//
//  Created by Rahul Sharma on 17/04/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DAPageIndicatorView.h"
#import <GoogleMaps/GoogleMaps.h>
#import "ViewPagerController.h"
#import "PICircularProgressView.h"
#import "MCPagerView.h"
#import "CustomNavigationBar.h"
#import "HomeScreenCell.h"

@interface HomeViewController : UIViewController<UIScrollViewDelegate,UIScrollViewAccessibilityDelegate,UIGestureRecognizerDelegate,GMSMapViewDelegate,MCPagerViewDelegate,UITableViewDelegate,UITableViewDataSource,HomeScreenCellDelegate>
{
    IBOutlet UIButton *btnAccept;
    IBOutlet UIButton *btnReject;
    IBOutlet UILabel *labelPickupTitle;
    IBOutlet UILabel *labelDropOffTitle;
    IBOutlet UILabel *labelPickeup;
    IBOutlet UILabel *labelDropOff;
    IBOutlet UILabel *labelPickUpdistance;
    IBOutlet UILabel *labelDropOffdistance;
    IBOutlet UILabel *labelDropOffMile;
    IBOutlet UILabel *labelPickUpMile;
    IBOutlet UILabel *labelStatus;
    NSMutableDictionary * dictpassDetail;




    IBOutlet UIView *viewPushPopUp;
    ViewPagerController *viewPage;
    BOOL isPassangeBookDriver;
    __weak IBOutlet MCPagerView *pagerView;

}
@property(strong,nonatomic)NSDictionary * dictPush;
@property(strong,nonatomic)NSTimer * timer;
@property(assign,nonatomic)int counter;
@property (strong, nonatomic) IBOutlet PICircularProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *lblAvailabilty;
@property (strong, nonatomic) IBOutlet UIScrollView *scrolView;
@property (strong, nonatomic) IBOutlet UILabel *labelDateHeader;
@property (strong, nonatomic) NSDate *date;
@property (assign, nonatomic) int currentPage;
@property (assign, nonatomic) int previousPage;
@property (strong, nonatomic) NSMutableArray *appointmentsArr;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) CustomNavigationBar *customNavigationBarView;
@property (strong, nonatomic) NSDictionary *dictAppointments;
@property (weak, nonatomic) IBOutlet UISwitch *switchDoctorStatus;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewNoAppointments;


-(IBAction)doctorAvalabilityClicked:(id)sender;
- (IBAction)updateDoctorStatus:(UISwitch *)sender;

- (IBAction)btnPreviousTapped;
- (IBAction)btnNextTapped;

@end
