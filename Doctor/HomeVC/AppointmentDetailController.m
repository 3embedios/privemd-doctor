//
//  AppointmentDetailController.m
//  Roadyo
//
//  Created by Rahul Sharma on 21/07/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "AppointmentDetailController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "HistoryViewController.h"
#import "KxMenu.h"
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
#import "UpdateBookingStatus.h"
#import "DirectionService.h"
#import "MTGoogleMapCustomURLInteraction.h"
#import "LocationTracker.h"
#import "XDKAirMenuController.h"
#import "SplashViewController.h"
#import <MessageUI/MessageUI.h>
#import "RaiseInVoiceViewController.h"


#define mapZoomLevel 20
#define BTNONTHEWAY 300
//#define BTNARRIVED 400

@interface AppointmentDetailController ()<CLLocationManagerDelegate,MFMessageComposeViewControllerDelegate,UIActionSheetDelegate>
{
    GMSMapView *mapView_;
    NSMutableArray *waypoints_;
    NSMutableArray *waypointStrings_;
    GMSGeocoder *geocoder_;
    UIView *customMapView;
    
    BOOL isKeyboardIsShown;
}
enum doctorStatus{
    ON_THE_WAY = 5,
    AM_ARRIVED = 6,
    INVOICE = 7
    
    
};

@property(nonatomic,assign) float currentLatitude;
@property(nonatomic,assign) float currentLongitude;
@property(nonatomic,assign)BOOL isUpdatedLocation;
@property(nonatomic,strong)NSString *startLocation;
@property(nonatomic,strong)NSString *destinationLocation;
@property(nonatomic,assign)CLLocationCoordinate2D destinationCoordinates;
@property(nonatomic,assign)CLLocationCoordinate2D pickupCoordinates;
@property(strong,nonatomic) IBOutlet UIView *customMapView;
@property(nonatomic,strong)GMSMarker *destinationMarker;
//@property(nonatomic,assign) float currentLatitude;
//@property(nonatomic,assign) float currentLongitude;
//@property(nonatomic,assign) BOOL isUpdatedLocation;
@property(nonatomic,assign)CLLocationCoordinate2D previouCoord;
@property(nonatomic,strong)NSMutableDictionary *allMarkers;
@property(nonatomic,strong)CLLocationManager *locationManager;
@property(nonatomic,assign)BOOL isPathPlotted;
@property(nonatomic,assign)BOOL isDriverArrived;

@property(nonatomic,strong) GMSMarker *currentLocationMarker;
@end

@implementation AppointmentDetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)updateDoctorStatus:(int)state{
    
    
    UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
    updateBookingStatus.driverState = state;
    updateBookingStatus.dictPatientDetails = self.dictAppointmentDetails;
    [updateBookingStatus updateToPassengerForDriverState];
    [updateBookingStatus startUpdatingStatus];
    
    
}

-(void)onTHEWAYButtonTapped{
    
    
    
    int doctorCurrentStatus = 0;
    
    //    if ([self.dictAppointmentDetails[@"status"] intValue] == 6) {
    //        [self pushToRaiseVoiceVC];
    //        return;
    //    }
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
    switch ([self.dictAppointmentDetails[@"status"] intValue]) {
            
        case 2:{
            doctorCurrentStatus = ON_THE_WAY;
            [self updateDoctorStatus:5];
            break;
        }
        case 5:{
            doctorCurrentStatus = AM_ARRIVED;
            [self updateDoctorStatus:6];
            break;
        }
        case 6:{
            doctorCurrentStatus = INVOICE;
            [self updateDoctorStatus:7];
            break;
            
        }
        default:
            break;
    }
    
    
    
    NSDictionary *parameters = @{kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId:[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 @"ent_appnt_dt":self.dictAppointmentDetails[@"apntDt"],
                                 KSMPPatientEmail:self.dictAppointmentDetails[@"email"],
                                 kSMPRespondResponse:[NSString stringWithFormat:@"%d",doctorCurrentStatus],
                                 kSMPRespondDocNotes:self.textViewRemark.text,
                                 @"ent_date_time":[Helper getCurrentDateTime]};
    
    
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    
    
    [httpClient postPath:MethodupdateApptStatus parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getONTHEWAYResponse:operation.responseString.JSONValue];
        // NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        //NSLog(@"Request Successful, response '%@'", responseStr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [Helper showAlertWithTitle:@"Error!" Message:error.localizedDescription];
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
    }];
    
    
}

-(void)getONTHEWAYResponse:(NSDictionary *)response
{
    
    // NSLog(@"response:%@",response);
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }else if ([[response objectForKey:@"errFlag"] intValue] == 1 && ([[response objectForKey:@"errNum"] intValue] == 6 || [[response objectForKey:@"errNum"] intValue] == 7)) {   // session token expire
        
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        
        /*
         SignInViewController *signVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
         signVC.loginBlock = ^(BOOL success){
         
         [self sendServicegetPatientAppointment];
         
         };
         UINavigationController *navigantionVC = [[UINavigationController alloc] initWithRootViewController:signVC];
         [self.navigationController presentViewController:navigantionVC animated:YES completion:^{
         
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Expired" message:@"Your session with privMD is expired. Please Login again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
         [alert show];
         }];
         */
        
        [self userSessionTokenExpire];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Expired" message:@"Your session with privMD is expired. Please Login again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
        
    }
    else
    {
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
            UIButton *button = (UIButton *)[self.view viewWithTag:BTNONTHEWAY];
            
            
            
            if ([self.dictAppointmentDetails[@"status"] intValue] == 6) {
                [self buttonBackTapped];
            }else if([self.dictAppointmentDetails[@"status"] intValue] == 2) { //on the way
                [self.dictAppointmentDetails setValue:@"5" forKey:@"status"];
                
                /* change navigation rightbar button title */
                
                self.navigationItem.rightBarButtonItem = nil;
                
                
                [self addNavRightButtonFor:@"Direction"];
                
                
                [self.btnOntheWay setTitle:@"I HAVE ARRIVED" forState:UIControlStateNormal];
                
                [self changeBottomViewFrame];
                
                //  _isPathPlotted = NO;
                _isUpdatedLocation = NO;
                _isDriverArrived = YES;
                
                
                /*
                 check if source and destination Distance , if distance between them is equalTo or less than 100
                 then call I Have Arrived service Automaticially
                 
                 */
                
                
                LocationTracker *locaitontraker = [LocationTracker sharedInstance];
                
                double latitude = locaitontraker.lastLocaiton.coordinate.latitude;
                
                double longitude = locaitontraker.lastLocaiton.coordinate.longitude;
                
                CLLocation *locA = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
                
                double latitudeDestination = [self.dictAppointmentDetails[@"apptLat"] doubleValue];
                double longitudeDestination = [self.dictAppointmentDetails[@"apptLong"] doubleValue];
                
                // NSLog(@"patient latidute and longitude:%@,%@",self.dictAppointmentDetails[@"apptLat"],self.dictAppointmentDetails[@"apptLong"]);
                
                CLLocation *locB = [[CLLocation alloc] initWithLatitude:latitudeDestination longitude:longitudeDestination];
                
                CLLocationDistance distance = [locA distanceFromLocation:locB];
                
                // NSLog(@"distance :%f",distance);
                if (distance <= 100) {
                    
                    [self onTHEWAYButtonTapped];
                }
                
                
                
            }else if([self.dictAppointmentDetails[@"status"] intValue] == 5) { //Arrived
                [self.dictAppointmentDetails setValue:@"6" forKey:@"status"];
                
                // [self.navigationItem.rightBarButtonItem setTitle:@"History"];
                
                self.navigationItem.rightBarButtonItem = nil;
                [self addNavRightButtonFor:@"History"];
                
                
                [button setTitle:@"RAISE INVOICE" forState:UIControlStateNormal];
                
                [self raiseINvoiceViewSetUp];
                
                
            }
            
        }
        else
        {
            
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
    
}

- (void)cancelAppointment:(NSString *)reason{
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
    NSDictionary *parameters = @{kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId:[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 @"ent_appnt_dt":self.dictAppointmentDetails[@"apntDt"],
                                 @"ent_email":self.dictAppointmentDetails[@"email"],
                                 @"ent_reason":reason,
                                 @"ent_date_time":[Helper getCurrentDateTime]};
    
    
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    

    
    [httpClient postPath:MethodabortAppointment parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        
        //NSLog(@"Request Successful, response '%@'", responseStr);
        
        /*Send message using PubNub */
        
        NSString *cancleReason ;
        if ([reason intValue] == 1) {
            cancleReason = @"Personal Emergency";
        }else{
            cancleReason = @"Medical Emergency";
        }
        
        NSDictionary *dict = @{@"a":@"10",
                               @"e":self.dictAppointmentDetails[@"email"],
                               @"d":self.dictAppointmentDetails[@"apntDt"],
                               @"r":cancleReason
                               };
        [self sendPubNubMessage:dict];
        
        //----------****end****--------****end****--------****end****---------------
        
        [self buttonBackTapped];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];
    
    
}

#pragma mark PUBNUB -

- (void)sendPubNubMessage:(NSDictionary *)message{
    
    NSString *patientChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUPatientPubNubChannelkey];
    PatientPubNubWrapper *pubNub = [PatientPubNubWrapper sharedInstance];
    [pubNub sendMessageAsDictionary:message toChannel:patientChannel];
    
}

#pragma mark UIView LifeCycle -

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    self.title = @"Appointment";
    
    CGRect rect = self.bottomCustomView.frame;
    rect.origin.y = [[UIScreen mainScreen] bounds].size.height - 112;
    self.bottomCustomView.frame = rect;
    self.bottomCustomView.backgroundColor = UIColorFromRGB(0x333333);
    
    
    [Helper setToLabel:self.lblPatientMessageText Text:@"PATIENT MESSAGE" WithFont:ZURICH_LIGHTCONDENSED FSize:15 Color:UIColorFromRGB(0xffffff)];
    [Helper setToLabel:self.lblAddressText Text:@"ADDRESS" WithFont:ZURICH_LIGHTCONDENSED FSize:15 Color:UIColorFromRGB(0xffffff)];
    
    
    self.lblAddressText.backgroundColor = UIColorFromRGB(0x61A0C0);
    self.lblPatientMessageText.backgroundColor = UIColorFromRGB(0x61A0C0);
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:37.778376 longitude:-122.409853 zoom:mapZoomLevel];
    
    customMapView.backgroundColor = [UIColor clearColor];
    CGRect rectMap ;
    if ([self.dictAppointmentDetails[@"status"] intValue] == 2) {
        rectMap = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, 320, self.view.bounds.size.height);
        self.textViewRemark.hidden = YES;
        
    }else if ([self.dictAppointmentDetails[@"status"] intValue] == ON_THE_WAY) {
        
        rectMap = CGRectMake(0, self.view.bounds.size.height, 320, self.view.bounds.size.height - self.bottomCustomView.frame.size.height);
        
        self.textViewRemark.hidden = YES;
        CGRect bottomViewFrame = self.bottomCustomView.frame;
        bottomViewFrame.origin.y = bottomViewFrame.origin.y + bottomViewFrame.size.height/2;
        bottomViewFrame.size.height = bottomViewFrame.size.height/2;
        self.bottomCustomView.frame = bottomViewFrame;
        
        CGRect btnOnTheWayFrame = self.btnOntheWay.frame;
        btnOnTheWayFrame.origin.y = self.bottomCustomView.frame.size.height/2 - self.btnOntheWay.frame.size.height/2;
        self.btnOntheWay.frame = btnOnTheWayFrame;
        
        self.btnCancelAppnt.hidden = YES;
        
        // rectMap = CGRectMake(0, self.bottomCustomView.frame.origin.y, 320, self.view.bounds.size.height - self.bottomCustomView.frame.size.height);
        
        
    }else if ([self.dictAppointmentDetails[@"status"] intValue] == AM_ARRIVED) {
        rectMap = CGRectMake(0, [UIScreen mainScreen].bounds.size.height, 320, self.view.bounds.size.height);
        
        
        self.btnCancelAppnt.hidden = YES;
        
        CGRect bottomViewFrame = self.bottomCustomView.frame;
        bottomViewFrame.origin.y = bottomViewFrame.origin.y + bottomViewFrame.size.height/2;
        bottomViewFrame.size.height = bottomViewFrame.size.height/2;
        self.bottomCustomView.frame = bottomViewFrame;
        
        CGRect btnOnTheWayFrame = self.btnOntheWay.frame;
        btnOnTheWayFrame.origin.y = self.bottomCustomView.frame.size.height/2 - self.btnOntheWay.frame.size.height/2;
        self.btnOntheWay.frame = btnOnTheWayFrame;
        
        
        [self raiseINvoiceViewSetUp];
        
        
        
        
    }
    
    /* getPatientChannelKey */
    [[NSUserDefaults standardUserDefaults]setValue:self.dictAppointmentDetails[@"patChn"] forKey:kNSUPatientPubNubChannelkey];
    
    
    mapView_ = [GMSMapView mapWithFrame:rectMap camera:camera];
    mapView_.delegate = self;
    mapView_.myLocationEnabled = YES;
    mapView_.settings.myLocationButton = YES;
    mapView_.settings.compassButton = YES;
    customMapView = mapView_;
    [self.view addSubview:customMapView];
    
    waypoints_ = [[NSMutableArray alloc]init];
    waypointStrings_ = [[NSMutableArray alloc]initWithCapacity:2];
    
    [self addArrowButtonToMapview];
    // [self addNavBackButton];
    //[self addNavRightButton];
    
    PriveMdAppDelegate *delegate = (PriveMdAppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate customizeNavigationLeftButton:self];
    
    //add tapgesture to phone label
    UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(phoneNumberTapped)];
    self.lblPhone.userInteractionEnabled = YES;
    [self.lblPhone addGestureRecognizer:tapgesture];
    
    
    NSString *address = [NSString stringWithFormat:@"%@ %@",self.dictAppointmentDetails[@"addrLine1"],self.dictAppointmentDetails[@"addrLine2"]];
    
    
    
    
    [Helper setToLabel:self.lblAppointDate Text:self.dictAppointmentDetails[@"apntTime"] WithFont:ZURICH_LIGHTCONDENSED FSize:21 Color:UIColorFromRGB(0x006699)];
    [Helper setToLabel:self.lblPhone Text:self.dictAppointmentDetails[@"phone"] WithFont:ZURICH_LIGHTCONDENSED FSize:17 Color:UIColorFromRGB(0x006699)];
    
    //[Helper setToLabel:self.lblAdress Text:address WithFont:ZURICH_LIGHTCONDENSED FSize:14 Color:UIColorFromRGB(0x333333)];
    //[Helper setToLabel:self.labelAppointmentDate Text:self.dictAppointmentDetails[@"apntDate"] WithFont:ZURICH_LIGHTCONDENSED FSize:17 Color:UIColorFromRGB(0x333333)];
    
    [Helper setToLabel:self.lblAdress Text:[NSString stringWithFormat:@"%@ %@ Miles",self.dictAppointmentDetails[@"addrLine2"],self.dictAppointmentDetails[@"distance"]] WithFont:ZURICH_LIGHTCONDENSED FSize:15 Color:UIColorFromRGB(0x000000)];
    
    [Helper setToLabel:self.lblName Text:self.dictAppointmentDetails[@"fname"] WithFont:ZURICH_LIGHTCONDENSED FSize:18 Color:UIColorFromRGB(0x000000)];
    
    self.textViewRemark.font = [UIFont fontWithName:ZURICH_LIGHTCONDENSED size:14];
    self.textViewRemark.text = self.dictAppointmentDetails[@"notes"];
    
    [Helper setToLabel:self.lblPatientMessage Text:self.dictAppointmentDetails[@"notes"] WithFont:ZURICH_LIGHTCONDENSED FSize:14 Color:UIColorFromRGB(0x262626)];
    
    [Helper setToLabel:self.lblFullAddress Text:address WithFont:ZURICH_LIGHTCONDENSED FSize:18 Color:BLACK_COLOR];
    
    
    [Helper setButton:self.btnCancelAppnt Text:@"CANCEL APPOINTMENT" WithFont:LATO_BOLD FSize:18 TitleColor:WHITE_COLOR ShadowColor:nil];
    [Helper setButton:self.btnOntheWay Text:@"AM ON THE WAY" WithFont:LATO_BOLD FSize:18 TitleColor:WHITE_COLOR ShadowColor:nil];
    
    
    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForThumbnailImage,self.dictAppointmentDetails[@"pPic"]];
    NSLog(@"strUrl :%@",strImageUrl);
    
    
    //self.profileImg = [[RoundedImageView alloc]initWithFrame:CGRectMake(13, 88, 80, 80)];
    // [self.view addSubview:self.profileImg];
    
    typeof(self) __weak weakSelf = self;
    [self.profileImg setImageWithURL:[NSURL URLWithString:strImageUrl]
                    placeholderImage:[UIImage imageNamed:@"notificationlistview_image_boader@2x.png"]
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                               
                               [weakSelf setProfileImage:image];
                           }];
    
    
    
    [self addNavRightButtonFor:@"History"];
    
    
    
    // UIButton *btnOntheWay = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if ([self.dictAppointmentDetails[@"status"] intValue] == 2) {
        // self.btnOntheWay.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 43, 320, 43);
        [self.btnOntheWay setTitle:@"AM ON THE WAY" forState:UIControlStateNormal];
    }else if ([self.dictAppointmentDetails[@"status"] intValue] == 5 ){
        // self.btnOntheWay.frame = CGRectMake(0, self.view.bounds.size.height - 43, 320, 43);
        [self.btnOntheWay setTitle:@"I HAVE ARRIVED" forState:UIControlStateNormal];
        
        self.navigationItem.rightBarButtonItem = nil;
        [self addNavRightButtonFor:@"Direction"];
        
        
        
        /*change Map Frame */
        // mapView_ = [GMSMapView mapWithFrame:rectMap camera:camera];
        
    }else if ([self.dictAppointmentDetails[@"status"] intValue] == 6 ){
        // self.btnOntheWay.frame = CGRectMake(0, self.view.bounds.size.height - 43, 320, 43);
        [self.btnOntheWay setTitle:@"RAISE INVOICE" forState:UIControlStateNormal];
        /*change map Frame */
        // mapView_ = [GMSMapView mapWithFrame:rectMap camera:camera];
        
        self.textViewRemark.userInteractionEnabled = YES;
        self.textViewRemark.text = nil;
        
        
    }
    
    self.btnOntheWay.tag = BTNONTHEWAY;
    
    
    
    
    [self getCurrentLocation];
    
    [self adjustTopViewFrame:self.lblFullAddress];
    
    [self.view bringSubviewToFront:self.bottomCustomView];
    
}

- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
    
    [mapView_ addObserver:self
               forKeyPath:@"myLocation"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    
    mapView_.myLocationEnabled = YES;
    
    
    
    
}
- (void) viewDidAppear:(BOOL)animated{
    
    // [self addgestureToMap];
    
    if (IS_SIMULATOR)
    {
        _currentLatitude = 13.028866;
        _currentLongitude = 77.589760;
        
        
        CLLocationCoordinate2D locationCord = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
        mapView_.camera = [GMSCameraPosition cameraWithTarget:locationCord
                                                         zoom:14];
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
        marker.title = @"It's Me";
        marker.snippet = @"Current Location";
        marker.map = mapView_;
        
        //subscribe to pubnub to get stream of doctors location
        //[self startPubNubStream];
        
    }
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showKeyboard:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(hideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    
    
    if ([self.dictAppointmentDetails[@"status"] intValue] == AM_ARRIVED) {
        
        customMapView.hidden = YES;
        self.textViewRemark.userInteractionEnabled = YES;
        [self.textViewRemark becomeFirstResponder];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [mapView_ removeObserver:self forKeyPath:@"myLocation"];
    
    UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
    
    [updateBookingStatus stopUpdatingStatus];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark - CLLocationManager Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    
    if (!_isUpdatedLocation) {
        
        [_locationManager stopUpdatingLocation];
        
        //change flag that we have updated location once and we don't need it again
        _isUpdatedLocation = YES;
        
        [self setStartLocationCoordinates:[self.dictAppointmentDetails[@"apptLat"]doubleValue] Longitude:[self.dictAppointmentDetails[@"apptLong"] doubleValue]];
        
        _isPathPlotted = NO;
        [self updateDestinationLocationWithLatitude:newLocation.coordinate.latitude Longitude:newLocation.coordinate.longitude];
        
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"locationManager failed to update location : %@",[error localizedDescription]);
}



#pragma mark - Map direction

-(void)getCurrentLocation{
    
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled]) {
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            
            [_locationManager requestAlwaysAuthorization];
        }
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest; //kCLLocationAccuracyThreeKilometers
        [_locationManager startUpdatingLocation];
        
    }
    else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Service" message:@"Unable to find your location,Please enable location services." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
}


/**
 *  adds direction on Map
 *
 *  @param json representaion of pathpoints
 */
- (void)addDirections:(NSDictionary *)json {
    
    if ([json[@"routes"] count]>0) {
        NSDictionary *routes = [json objectForKey:@"routes"][0];
        
        NSDictionary *route = [routes objectForKey:@"overview_polyline"];
        NSString *overview_route = [route objectForKey:@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        polyline.map = mapView_;
    }
    
    
}

/**
 *  Sets the start location of Path
 *
 *  @param latitude  start point latitude
 *  @param longitude start point longitude
 */
-(void)setStartLocationCoordinates:(float)latitude Longitude:(float)longitude{
    //change map camera postion to current location
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:14];
    [mapView_ setCamera:camera];
    
    
    //add marker at current location
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    marker.map = mapView_;
    marker.icon = [UIImage imageNamed:@"map_icn_location.png"];
    [waypoints_ addObject:marker];
    
    
    //save current location to plot direciton on map
    _startLocation = [NSString stringWithFormat:@"%.6f,%.6f",latitude, longitude];
    [waypointStrings_ insertObject:_startLocation atIndex:0];
    
    
    
}

/**
 * sets destination point of path
 *
 *  @param latitude  destination point latitude
 *  @param longitude destination point longitude
 */
-(void)updateDestinationLocationWithLatitude:(float)latitude Longitude:(float)longitude{
    
    
    if (!_isPathPlotted) {
        
        NSLog(@"pathplotted");
        _isPathPlotted = YES;
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        
        _previouCoord = position;
        
        _destinationMarker = [GMSMarker markerWithPosition:position];
        _destinationMarker.map = mapView_;
        _destinationMarker.flat = YES;
        _destinationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        _destinationMarker.icon = [UIImage imageNamed:@"car.png"];
        _destinationMarker.snippet = @"My Current Location";
        
        [waypoints_ addObject:_destinationMarker];
        NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",
                                    latitude,longitude];
        
        [waypointStrings_ addObject:positionString];
        
        if([waypoints_ count]>1){
            NSString *sensor = @"false";
            NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                                   nil];
            NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
            NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                              forKeys:keys];
            DirectionService *mds=[[DirectionService alloc] init];
            SEL selector = @selector(addDirections:);
            [mds setDirectionsQuery:query
                       withSelector:selector
                       withDelegate:self];
        }
        
    }
    else {
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        CLLocationDirection heading = GMSGeometryHeading(_previouCoord, position);
        _previouCoord = position;
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        _destinationMarker.position = position;
        [CATransaction commit];
        if (_destinationMarker.flat) {
            _destinationMarker.rotation = heading;
        }
        
    }
    
    
    
}



#pragma mark- GMSMapviewDelegate

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    CGRect frame = customMapView.frame;
    if (frame.origin.y == 500 || frame.origin.y == 412) {
        frame.origin.y = 221;
        frame.origin.x = 0;
        
        UIButton *btn = (UIButton *)[customMapView viewWithTag:100];
        btn.tag = 200;
        
        UIButton *btnOnTheWay = (UIButton *)[self.view viewWithTag:BTNONTHEWAY];
        CGRect rectButtonONTheWay = btnOnTheWay.frame;
        rectButtonONTheWay.origin.y = self.view.frame.size.height - 43;
        
        
        [UIView animateWithDuration:0.4
                              delay:0.1
                            options: UIViewAnimationOptionCurveEaseIn
                         animations:^
         {
             
             
             customMapView.frame = frame;
             btnOnTheWay.frame = rectButtonONTheWay;
         }
                         completion:^(BOOL finished)
         {
             NSLog(@"Completed");
             //[acceptRejectBtnView setHidden:YES];
             UIButton *btn = (UIButton *)[customMapView viewWithTag:200];
             [btn setHidden:NO];
             
             
             
             
             
         }];
        
    }
    
}
- (void) mapView:(GMSMapView *) mapView willMove:(BOOL)gesture{
    
    NSLog(@"willMove");
    
}

- (void) mapView:(GMSMapView *) mapView didChangeCameraPosition:(GMSCameraPosition *) position{
    
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
    NSLog(@"idleAtCameraPosition");
    
}
- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    
    [self performSegueWithIdentifier:@"doctorDetails" sender:marker];
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    
    NSLog(@"marker userData %@",marker.userData);
    
    
    return nil;
}
- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker{
    NSLog(@"didBeginDraggingMarker");
}
- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    NSLog(@"didEndDraggingMarker");
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
    
    GMSCameraUpdate *vancouverCam = [GMSCameraUpdate setTarget:location.coordinate];
    [mapView_ animateWithCameraUpdate:vancouverCam];
    
    
    //    if (!_isUpdatedLocation) {
    //        // If the first location update has not yet been recieved, then jump to that
    //        // location.
    //       // _isUpdatedLocation = YES;
    //       // mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
    //                       //    zoom:14];
    //
    //
    //    }
    //    else {
    //        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
    //        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
    //        _currentLocationMarker.position = position;
    //
    //    }
    
    
    _isPathPlotted = YES;
    
    [self updateDestinationLocationWithLatitude:location.coordinate.latitude Longitude:location.coordinate.longitude];
    
    
}



#pragma mark Other Method -

- (void)changeBottomViewFrame{
    
    CGRect frame = customMapView.frame;
    frame.origin.y = 190;
    frame.size.height = frame.size.height - 190;
    
    CGRect bottomViewFrame = self.bottomCustomView.frame;
    bottomViewFrame.origin.y = bottomViewFrame.origin.y + bottomViewFrame.size.height/2;
    bottomViewFrame.size.height = bottomViewFrame.size.height/2;
    
    
    CGRect btnOnTheWayFrame = self.btnOntheWay.frame;
    btnOnTheWayFrame.origin.y = bottomViewFrame.size.height/2 - self.btnOntheWay.frame.size.height/2;
    //self.btnOntheWay.frame = btnOnTheWayFrame;
    
    
    
    self.btnCancelAppnt.alpha = 0.0;
    
    [UIView animateWithDuration:0.4
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^
     {
         
         
         self.btnCancelAppnt.hidden = YES;
         
         self.bottomCustomView.frame = bottomViewFrame;
         //  customMapView.frame = frame;
         self.btnOntheWay.frame = btnOnTheWayFrame;
         
         [self.view bringSubviewToFront:self.bottomCustomView];
         
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         //[acceptRejectBtnView setHidden:YES];
         
         
         
     }];
    
}


- (void)raiseINvoiceViewSetUp{
    
    customMapView.hidden = YES;
    
    self.textViewRemark.userInteractionEnabled = YES;
    self.textViewRemark.text = nil;
    [self.textViewRemark becomeFirstResponder];
    
    self.lblAddressText.text = @"Write a note for future appointments";
    
    self.lblFullAddress.hidden = YES;
    self.lblPatientMessageText.hidden = YES;
    self.lblPatientMessage.hidden = YES;
    
    self.textViewRemark.alpha = 0;
    self.textViewRemark.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        self.textViewRemark.alpha = 1;
    }];
    
    
}

-(void)userSessionTokenExpire{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    [[[XDKAirMenuController sharedMenu] view] removeFromSuperview];
    
    if ([XDKAirMenuController relese]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        SplashViewController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
    }
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"HistoryVC"])
    {
        HistoryViewController *history = (HistoryViewController*)[segue destinationViewController];
        history.patientEmail = self.dictAppointmentDetails[@"email"];
        
    }else if ([[segue identifier] isEqualToString:@"RaiseINVoiceVC"]){
        
        RaiseInVoiceViewController *raiseVC = (RaiseInVoiceViewController *)[segue destinationViewController];
        raiseVC.dictAppointmentDetails = self.dictAppointmentDetails;
    }
}

- (void)phoneNumberTapped{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Call" message:@"Do you want to make a call on this number?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
    alert.tag = 200;
    [alert show];
    
    
}


- (void)adjustTopViewFrame:(UILabel *)addressLabel{
    
    addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize maximumLabelSize = CGSizeMake(252, 9999);
    CGSize expectSize = [addressLabel sizeThatFits:maximumLabelSize];
    expectSize.height = expectSize.height + 15;
    if (expectSize.height <=  60) {
        return;
    }
    
    CGRect rect = self.lblFullAddress.frame;
    rect.size.height = expectSize.height;
    self.lblFullAddress.frame = rect;
    
    
    /*
     rect = self.lblFullAddress.frame;
     rect.size.height = self.lblFullAddress.frame.size.height + self.lblFullAddress.frame.origin.y;
     self.lblFullAddress.frame = rect;
     */
    
    
    rect = self.lblPatientMessageText.frame;
    rect.origin.y = self.lblFullAddress.frame.origin.y + self.lblFullAddress.frame.size.height;
    self.lblPatientMessageText.frame = rect;
    
    
    rect = self.lblPatientMessage.frame;
    rect.origin.y = self.lblPatientMessageText.frame.origin.y + self.lblPatientMessageText.frame.size.height;
    self.lblPatientMessage.frame = rect;
    
    
}

- (void)pushToRaiseVoiceVC{
    
    [self performSegueWithIdentifier:@"RaiseINVoiceVC" sender:self];
}


#pragma mark UIButton Action
- (void)addArrowButtonToMapview{
    
    UIButton *buttonArrow = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonArrow.frame = CGRectMake(320/2-15,0, 30, 30);
    buttonArrow.tag =  100;
    [buttonArrow setImage:[UIImage imageNamed:@"homescreenmapview_down_arrow_off.png"] forState:UIControlStateNormal];
    [buttonArrow setImage:[UIImage imageNamed:@"homescreenmapview_down_arrow_on.png"] forState:UIControlStateHighlighted];
    [buttonArrow addTarget:self action:@selector(moveMapViewUPAndDOWN:) forControlEvents:UIControlEventTouchUpInside];
    [buttonArrow setHidden:YES];
    [customMapView addSubview:buttonArrow];
    
    
    
    
}




- (void)moveMapViewUPAndDOWN:(UIButton *)sender{
    
    
    CGRect frame = customMapView.frame;
    if (sender.tag == 100) {
        sender.tag = 200;
        frame.origin.y = 64;
        frame.origin.x = 0;
        frame.size.height = self.view.bounds.size.height - 100;
    }else{
        sender.tag = 100;
        frame.origin.y = self.view.bounds.size.height - 54;
    }
    
    
    
    
    [UIView animateWithDuration:0.4 delay:0.1 options: UIViewAnimationOptionCurveEaseIn animations:^
     {
         
         
         customMapView.frame = frame;
         
         
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
         
         
         if (sender.tag == 200) {
             // sender.hidden = NO;
             [self.view bringSubviewToFront:self.bottomCustomView];
             
             [sender setTitle:@"Close" forState:UIControlStateNormal];
             
         }else{
             // sender.hidden = YES;
             [sender setTitle:@"Map" forState:UIControlStateNormal];
             
             
         }
         
     }];
}
/*
 - (void)addNavBackButton{
 UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
 rightButton.frame = CGRectMake(0, 0, 66.5, 44);
 [rightButton setImage:[UIImage imageNamed:@"notificationlistview_back_btn_off.png"] forState:UIControlStateNormal];
 [rightButton setImage:[UIImage imageNamed:@"notificationlistview_back_btn_on.png"] forState:UIControlStateHighlighted];
 [rightButton addTarget:self action:@selector(buttonBackTapped:) forControlEvents:UIControlEventTouchUpInside];
 
 UIBarButtonItem *barButton = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
 self.navigationItem.leftBarButtonItem = barButton;
 
 
 }
 */

- (void)addNavRightButtonFor:(NSString *)type{
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.tag = 100;
    if ([type isEqualToString:@"History"]) {
        rightButton.frame = CGRectMake(0, 0, 66.5, 60);
        [rightButton setTitle:@"History" forState:UIControlStateNormal];
    }else{
        rightButton.frame = CGRectMake(0, 0, 66.5, 100);
        [rightButton setTitle:@"Map" forState:UIControlStateNormal];
    }
    [rightButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
    
    [rightButton setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
    
    [rightButton addTarget:self action:@selector(openPopUP:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = barButton;
    
}

- (void)buttonBackTapped{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)buttonHistoryTapped{
    
    [self performSegueWithIdentifier:@"HistoryVC" sender:self];
}

- (void)setProfileImage:(UIImage *)img{
    self.profileImg.image = img;
}

- (void)openPopUP:(UIButton *)sender {
    
    
    
    if ([sender.titleLabel.text isEqualToString:@"History"]) {
        [self buttonHistoryTapped];
    }else if ([sender.titleLabel.text isEqualToString:@"Map"] || [sender.titleLabel.text isEqualToString:@"Close"] ){
        /*
         
         if ([MTGoogleMapCustomURLInteraction isGoogleMapsAppPresent]) {
         LocationTracker *locaitontraker = [LocationTracker sharedInstance];
         
         float latitude = locaitontraker.lastLocaiton.coordinate.latitude;
         float longitude = locaitontraker.lastLocaiton.coordinate.longitude;
         
         CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
         [self getAddress:position];
         
         }else{
         
         [Helper showAlertWithTitle:@"Oops!" Message:@"You don't have google maps installed on this device!Please download google maps to use this service."];
         }
         */
        
        [self moveMapViewUPAndDOWN:sender];
        
        
        
    }
    
}
- (void) pushMenuItem:(id)sender
{
    KxMenuItem *menu = (KxMenuItem *)sender;
    NSLog(@"%@", sender);
    if ([menu.title isEqualToString:@"History"]) {
        [self buttonHistoryTapped];
    }else if ([menu.title isEqualToString:@"Direction"]){
        
        // UIButton *btn = (UIButton *)[customMapView viewWithTag:100];
        //[self moveMapViewUPAndDOWN:btn];
        /*
         
         
         NSURL *testURL = [NSURL URLWithString:@"comgooglemaps-x-callback://"];
         if ([[UIApplication sharedApplication] canOpenURL:testURL]) {
         
         NSString *directionsRequest = [NSString stringWithFormat:@"%@?daddr=John+F.+Kennedy+International+Airport,+Van+Wyck+Expressway,+Jamaica,+New+York&x-success=sourceapp://?resume=true&x-source=AirApp",testURL];
         
         //NSString *directionsRequest = @"comgooglemaps-x-callback://" +@"?daddr=John+F.+Kennedy+International+Airport,+Van+Wyck+Expressway,+Jamaica,+New+York" +
         // @"&x-success=sourceapp://?resume=true&x-source=AirApp";
         
         NSURL *directionsURL = [NSURL URLWithString:directionsRequest];
         [[UIApplication sharedApplication] openURL:directionsURL];
         } else {
         NSLog(@"Can't use comgooglemaps-x-callback:// on this device.");
         }
         
         */
        
        LocationTracker *locaitontraker = [LocationTracker sharedInstance];
        
        float latitude = locaitontraker.lastLocaiton.coordinate.latitude;
        float longitude = locaitontraker.lastLocaiton.coordinate.longitude;
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
        [self getAddress:position];
        
    }
}


#pragma mark MFMessageview Controller delegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark UIAddress Formatter

- (void)getAddress:(CLLocationCoordinate2D)coordinate {
    
    if (!geocoder_) {
        geocoder_ = [[GMSGeocoder alloc] init];
    }
    
    
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response,
                                          NSError *error) {
        if (response && response.firstResult) {
            
            GMSAddress *address = response.firstResult;
            NSString *start = [address.lines componentsJoinedByString:@","];
            
            
            [self performSelectorOnMainThread:@selector(openDirection:) withObject:start waitUntilDone:YES];
            
            
            
        }else {
            // NSLog(@"Could not reverse geocode point (%f,%f): %@",
            //coordinate.latitude, coordinate.longitude, error);
        }
    };
    
    [geocoder_ reverseGeocodeCoordinate:coordinate
                      completionHandler:handler];
    
}


- (void)openDirection:(NSString *)startLocation{
    
    NSString *destination = self.dictAppointmentDetails[@"addrLine1"];
    [MTGoogleMapCustomURLInteraction showDirections:@{DirectionsStartAddress: startLocation,
                                                      DirectionsEndAddress: destination,
                                                      DirectionsDirectionMode: @"Driving",
                                                      ShowMapKeyZoom: @"7",
                                                      ShowMapKeyViews: @"Satellite",
                                                      ShowMapKeyMapMode: @"standard"}
                                      allowCallback:YES];
}

#pragma mark UITextView Delegate -
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark UIAlertView Delegate -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 200){
        
        if (buttonIndex == 1) {
            
            NSString *phoneNumber = self.dictAppointmentDetails[@"phone"]; // dynamically assigned
            /*
             NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", phoneNumber];
             NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
             */
            NSString *cleanedString = [[phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
            NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
            [[UIApplication sharedApplication] openURL:telURL];
            
        }
    }
    
    
    
}

#pragma mark UIKeyBoard Observer -
- (void)showKeyboard:(NSNotification *)notification{
    
    /*
     if(isKeyboardIsShown)
     {
     return;
     }
     // Get the keyboard size
     CGRect keyboardBounds;
     [[notification.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
     
     CGRect frame = self.view.frame;
     
     // Start animation
     [UIView beginAnimations:nil context:NULL];
     [UIView setAnimationBeginsFromCurrentState:YES];
     [UIView setAnimationDuration:0.3f];
     
     // Reduce size of the scroll view
     frame.origin.y -= keyboardBounds.size.height - 140;
     // Apply new size of scroll view
     self.view.frame = frame;
     [UIView commitAnimations];
     isKeyboardIsShown = YES;
     */
}

- (void)hideKeyboard:(NSNotification *)notification{
    /*
     CGRect keyboardBounds;
     [[notification.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
     
     // Detect orientation
     
     CGRect frame = self.view.frame;
     [UIView beginAnimations:nil context:NULL];
     [UIView setAnimationBeginsFromCurrentState:YES];
     [UIView setAnimationDuration:0.3f];
     
     frame.origin.y += keyboardBounds.size.height-140;
     // Apply new size of scroll view
     self.view.frame = frame; //CGRectMake(0, 0, 320, [UIScreen mainScreen].bounds.size.height);
     
     [UIView commitAnimations];
     isKeyboardIsShown = NO;
     */
}

- (IBAction)buttonPhoneTapped:(UIButton *)sender {
    [self phoneNumberTapped];
}

- (IBAction)buttonMessageTapped:(UIButton *)sender {
    
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    // NSString *test = @"Test";
    
    NSArray *recipents = @[self.dictAppointmentDetails[@"phone"]];
    NSString *message = @""; //[NSString stringWithFormat:@"Just sent the %@ file to your email. Please check!", test];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
    
}

-(IBAction)buttonCancelAppointmentTapped:(UIButton *)sender {
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Cancel Appointment" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Personal Emergency",@"Medical Emergency", nil];
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    
    
}

- (IBAction)buttonOnTheWay:(UIButton *)sender {
    
    [self onTHEWAYButtonTapped];
}

- (IBAction)buttonDirectionTapped:(UIButton *)sender {
    
    if ([MTGoogleMapCustomURLInteraction isGoogleMapsAppPresent]) {
        LocationTracker *locaitontraker = [LocationTracker sharedInstance];
        
        float latitude = locaitontraker.lastLocaiton.coordinate.latitude;
        float longitude = locaitontraker.lastLocaiton.coordinate.longitude;
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
        [self getAddress:position];
        
    }else{
        
        [Helper showAlertWithTitle:@"Oops!" Message:@"You don't have google maps installed on this device!Please download google maps to use this service."];
    }
}



#pragma mark - IBActionSheet
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex != 2) {
        
        switch (buttonIndex) {
            case 0:
                [self cancelAppointment:@"1"];
                break;
            case 1:{
                // [self abortBooking:Wrong_Address_Was_Given];
                [self cancelAppointment:@"2"];
                break;
            }
            case 2:{
                //[self abortBooking:I_Am_Not_Well_Prepared_For_This_Session];
                break;
            }
            case 3:{
                //  [self abortBooking:I_Forgot_About_My_Other_Engagements];
                
                break;
            }
            case 4:{
                // [self abortBooking:Other_Reason];
                break;
            }
                
            default:
                break;
        }
        
    }
    
}


@end
