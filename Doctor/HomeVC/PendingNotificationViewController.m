//
//  PendingNotificationViewController.m
//  Roadyo
//
//  Created by Rahul Sharma on 19/07/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "PendingNotificationViewController.h"
#import "AppointmentsCell.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "PendingNotificationDetailController.h"
#import "PendingNotificationCell.h"
#import "SignInViewController.h"


#define NO_APPOINTMENT_IMAGE 100
@interface PendingNotificationViewController ()
@property (strong, nonatomic) NSString *selectedPhoneNumber;
@property (strong, nonatomic) NSDictionary *selectedAppointmentDict;
@end

@implementation PendingNotificationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma WebService Request-
-(void)requestPendingAppointments
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
    NSDictionary *parameters = @{kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId:[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 @"ent_date_time":[Helper getCurrentDateTime]};
    
    
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient postPath:MethodGetPendingAppointments parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getPendingAppointmentResponse:operation.responseString.JSONValue];
        
        //NSLog(@"Request Successful, response '%@'", responseStr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];
    
}

- (void)getPendingAppointmentResponse:(NSDictionary *)response
{
    
    NSLog(@"response:%@",response);
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }else if ([[response objectForKey:@"errFlag"] intValue] == 1 && ([[response objectForKey:@"errNum"] intValue] == 6 || [[response objectForKey:@"errNum"] intValue] == 7)) {   // session token expire
        
        
        SignInViewController *signVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        signVC.loginBlock = ^(BOOL success){
            
            [self requestPendingAppointments];
            
        };
        UINavigationController *navigantionVC = [[UINavigationController alloc] initWithRootViewController:signVC];
        [self.navigationController presentViewController:navigantionVC animated:YES completion:^{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Expired" message:@"Your session with privMD is expired. Please Login again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }];
        
        
    }

    else
    {
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            
             self.tblView.hidden = NO;
            
            self.appointments = [[dictResponse objectForKey:@"appointments"] mutableCopy];
            [self.tblView reloadData];
            //date done
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
        }
        else
        {
           // UILabel *labelNoAppointment = [[UILabel alloc]initWithFrame:CGRectMake(150 - 100, self.view.frame.size.height/2-25, 200, 50)];
            //[Helper setToLabel:labelNoAppointment Text:@"No appointments on this date." WithFont:HELVETICANEUE_LIGHT FSize:15 Color:[UIColor blackColor]];
            self.tblView.hidden = YES;
            
            UIImageView *imgVW = [[UIImageView alloc]initWithFrame:CGRectMake(320/2-100, self.view.frame.size.height/2-100, 200, 200)];
            imgVW.image = [UIImage imageNamed:@"no_appointment_logo.png"];
            imgVW.tag = NO_APPOINTMENT_IMAGE;
            [self.view addSubview:imgVW];
            
          //  [self.view addSubview:labelNoAppointment];
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            //[Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
    
}

- (void)requestAcceptRejectBooking:(BOOL)isAccept details:(NSDictionary *)dict{
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
    NSString *appointmentAction;
    if (isAccept) {
        appointmentAction = @"2";
    }else{
        appointmentAction = @"3";
    }
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                                [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                                dict[@"email"],kSMPRespondPassengerEmail,
                                dict[@"apntDt"], kSMPRespondBookingDateTime,
                                appointmentAction,kSMPRespondResponse,
                                dict[@"bookType"],kSMPRespondBookingType,
                                [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    
    
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient postPath:MethodRespondToAppointMent parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self acceptRejectResponse:operation.responseString.JSONValue];
        
        //NSLog(@"Request Successful, response '%@'", responseStr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];
    
    
}

#pragma AcceptReject Response -
- (void)acceptRejectResponse:(NSDictionary *)response
{
    
    NSLog(@"response:%@",response);
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [self.navigationController popViewControllerAnimated:YES];
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
        }
        else
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
    
}

#pragma mark UIView Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Pending Appointments";
    
    PriveMdAppDelegate *delegate = (PriveMdAppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate customizeNavigationLeftButton:self];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [self requestPendingAppointments];
     self.navigationController.navigationBarHidden = NO;
    
  
}

- (void)viewWillDisappear:(BOOL)animated{
   
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark UITableView Delegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 145.0f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   // UITableViewCell *cell=(UITableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
   // [cell setBackgroundColor:[UIColor orangeColor]];
    
   self.dictDetails = self.appointments[indexPath.row];
    [self performSegueWithIdentifier:@"PendingNotificationDetail" sender:self];
    
}
/*
- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
 
   // PendingNotificationCell *cell = (PendingNotificationCell *)[self.tblView cellForRowAtIndexPath:indexPath];
  //  cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"selected_appoinment.png"]];
}
 */
/*
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PendingNotificationCell *cell = (PendingNotificationCell *)[self.tblView cellForRowAtIndexPath:indexPath];
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"selected_appoinment.png"]];
    return indexPath;
}
*/

#pragma mark UITableview DataSource -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.appointments.count >
        0) {
        UIImageView *img= (UIImageView *)[self.view viewWithTag:NO_APPOINTMENT_IMAGE];
        img.hidden = YES;
    }
    return self.appointments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    PendingNotificationCell *cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==nil)
    {
        cell =[[PendingNotificationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        
        cell.selectionStyle=UITableViewCellSelectionStyleDefault;
        
       // cell.backgroundColor=[UIColor clearColor];
        
      
    }
    
    NSDictionary *dictAppointment = self.appointments[indexPath.row];
    
    [cell.activityIndicator startAnimating];
    
    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForThumbnailImage,dictAppointment[@"pPic"]];
    NSLog(@"strUrl :%@",strImageUrl);
    
    [cell.profileImage setImageWithURL:[NSURL URLWithString:strImageUrl]
                      placeholderImage:[UIImage imageNamed:@"notificationlistview_image_boader@2x.png"]
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                                 
                             }];
    
    [cell.activityIndicator stopAnimating];
    NSLog(@"stopAnimating");
    
    
    [cell.buttonAccept addTarget:self action:@selector(acceptBooking:) forControlEvents:UIControlEventTouchUpInside];
    cell.buttonAccept.tag = indexPath.row;
    [cell.buttonReject addTarget:self action:@selector(rejectBooking:) forControlEvents:UIControlEventTouchUpInside];
    cell.buttonReject.tag = indexPath.row;
    
    [cell.lblPatientName setText:dictAppointment[@"fname"]];
    [cell.lblAppointmentTime setText:dictAppointment[@"apntTime"]];
    [cell.labelappointmentDate setText:dictAppointment[@"apntDate"]];
    //[cell.lblPhoneNo setText:dictAppointment[@"phone"]];
    [cell.lblPhoneNo setTitle:dictAppointment[@"phone"] forState:UIControlStateNormal];
    [cell.lblPhoneNo addTarget:self action:@selector(phoneNumberTapped:) forControlEvents:UIControlEventTouchUpInside];
    [cell.lblAddress setText:dictAppointment[@"addrLine2"]];
    if ([dictAppointment[@"bookType"] intValue] == 1) {
        [cell.notificationType setText:@"Now"];
        cell.notificationType.backgroundColor = [UIColor yellowColor];
    }else{
        [cell.notificationType setText:@"Later"];
        cell.notificationType.backgroundColor = [UIColor greenColor];
        
         [cell.lblAppointmentTime setText:[NSString stringWithFormat:@"%@ %@",dictAppointment[@"apntTime"],dictAppointment[@"apntDate"]]];
    }
    
    
    cell.lblDistance.text = [NSString stringWithFormat:@"%@ Miles",dictAppointment[@"distance"]];

    cell.sepratorImage.image = [UIImage imageNamed:@"notificationlistview_blue_line@2x.png"];
    cell.distanceBaloon.image = [UIImage imageNamed:@"homescreenmapview_current_appointment_indicator@2x.png"];
    
    
    return cell;
    
}

#pragma mark Other Method -
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"PendingNotificationDetail"])
    {
        PendingNotificationDetailController *detail = (PendingNotificationDetailController*)[segue destinationViewController];
        detail.dictAppointmentDetails = self.dictDetails;
        
        
        
    }
}


#pragma mark UIButton Action -
-(void)acceptBooking:(UIButton *)sender{
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Appointment!" message:@"Are you sure you want to accept this appointment?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alertView.tag = 600;
    [alertView show];
    
    self.selectedAppointmentDict = self.appointments[sender.tag];
   // [self requestAcceptRejectBooking:YES details:dict];
}
-(void)rejectBooking:(UIButton *)sender{
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Appointment!" message:@"Are you sure you want to reject this appointment?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alertView.tag = 700;
    [alertView show];
    
    self.selectedAppointmentDict = self.appointments[sender.tag];
   // [self requestAcceptRejectBooking:NO details:dict];
}

- (void)buttonBackTapped{
    NSLog(@"button back taped");
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)phoneNumberTapped:(UIButton *)sender{
    
  
    NSDictionary *dictAppointment = self.appointments[sender.tag];
   self.selectedPhoneNumber = dictAppointment[@"phone"];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Call" message:@"Do you want to make a call on this number?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
    alert.tag = 300;
    [alert show];
}

#pragma mark UIAlertView Delegate -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 300){
        
        if (buttonIndex == 1) {
            
            NSString *phoneNumber = self.selectedPhoneNumber; // dynamically assigned
            /*
            NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", phoneNumber];
            NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
            [[UIApplication sharedApplication] openURL:phoneURL];
             */
            
            NSString *cleanedString = [[phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
            NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
            [[UIApplication sharedApplication] openURL:telURL];
        }
    }else if (alertView.tag == 600){ //accept
        if (buttonIndex == 1) {
            
            [self requestAcceptRejectBooking:YES details:self.selectedAppointmentDict];
        }
    }else if (alertView.tag == 700){ //reject
     
        if (buttonIndex == 1) {
             [self requestAcceptRejectBooking:NO details:self.selectedAppointmentDict];
        }
    }
    
    
    
}

@end
