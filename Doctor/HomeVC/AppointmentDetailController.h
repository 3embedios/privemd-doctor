//
//  AppointmentDetailController.h
//  Roadyo
//
//  Created by Rahul Sharma on 21/07/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "RoundedImageView.h"

@interface AppointmentDetailController : UIViewController<GMSMapViewDelegate>
@property(strong, nonatomic) NSMutableDictionary *dictAppointmentDetails;
@property (strong, nonatomic)IBOutlet  RoundedImageView *profileImg;

@property (weak, nonatomic) IBOutlet UILabel *lblAppointDate;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblAdress;
@property (weak, nonatomic) IBOutlet UILabel *lblPatientMessage;
@property (weak, nonatomic) IBOutlet UILabel *labelDistance;
@property (weak, nonatomic) IBOutlet UITextView *textViewRemark;
@property (strong, nonatomic) IBOutlet UILabel *lblFullAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblAddressText;
@property (strong, nonatomic) IBOutlet UILabel *lblPatientMessageText;
@property (strong, nonatomic) IBOutlet UIButton *btnOntheWay;
@property (strong, nonatomic) IBOutlet UIButton *btnCancelAppnt;
@property (strong, nonatomic) IBOutlet UIView *bottomCustomView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrolView;



- (IBAction)buttonPhoneTapped:(UIButton *)sender;
- (IBAction)buttonMessageTapped:(UIButton *)sender;

- (IBAction)buttonCancelAppointmentTapped:(UIButton *)sender;
- (IBAction)buttonOnTheWay:(UIButton *)sender;
- (IBAction)buttonDirectionTapped:(UIButton *)sender;
@end
