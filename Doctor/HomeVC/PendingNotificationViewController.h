//
//  PendingNotificationViewController.h
//  Roadyo
//
//  Created by Rahul Sharma on 19/07/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PendingNotificationViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) NSArray *appointments;
@property (strong, nonatomic) NSDictionary *dictDetails;
@end
