
//  HomeViewController.m
//  Doctor
//
//  Created by Rahul Sharma on 17/04/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "HomeViewController.h"
#import "DAPagesContainer.h"
#import "CustomNavigationBar.h"
#import "CustomMarkerView.h"
#import "XDKAirMenuController.h"
#import "PMDReachabilityWrapper.h"
#import "SliderViewController.h"
#import "WildcardGestureRecognizer.h"
#import "ContentViewController.h"
#import "ProgressIndicator.h"
#import "Helper.h"
#import "WebServiceConstants.h"
#import "AppConstants.h"
#import "passDetail.h"
#import "OnBookingViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "DirectionService.h"
#import <CoreLocation/CoreLocation.h>
#import "DoctorDetailView.h"
#import "AppointedDoctor.h"
#import "HelpViewController.h"
#import "PatientPubNubWrapper.h"
#import "LocationTracker.h"
#import "UploadFiles.h"
#import "BookingHistoryViewController.h"
#import "CheckBookingStatus.h"
#import "NSCalendar+DateManipulation.h"
#import "NSDate+Description.h"
#import "MCPagerView.h"
#import "AppointmentsCell.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "AppointmentDetailController.h"
#import "SplashViewController.h"
#import "SignInViewController.h"
#import "HomeScreenCell.h"
#import "PendingNotificationDetailController.h"
#import "TELogger.h"
#import <QuartzCore/QuartzCore.h>


#define mapZoomLevel 17
#define BottomShadowImageTag 18

@interface HomeViewController ()<CustomNavigationBarDelegate,XDKAirMenuDelegate,CLLocationManagerDelegate,ViewPagerDelegate,PatientPubNubWrapperDelegate,UploadFileDelegate>
{
    
    GMSMapView *mapView_;
    NSMutableArray *waypoints_;
    NSMutableArray *waypointStrings_;
    GMSGeocoder *geocoder_;
    UIView *customMapView;
    // RoadyoPubNubWrapper *pubNub;
    NSString *selectedPhoneNumber;
    
    BOOL isAlertViewPresentedForSessionTokenExpire;
    
    Boolean isMapViewOpened;
    
    NSArray *pendingAppointments;
    NSArray *newAppointments;
}

@property(nonatomic,strong)NSString *startLocation;
@property(nonatomic,strong)NSString *destinationLocation;
@property(nonatomic,strong)CLLocationManager *locationManager;
@property(nonatomic,strong)GMSMutablePath *mutablePath;
@property(nonatomic,strong)GMSMarker *destinationMarker;
@property(nonatomic,strong)NSString *pubnubChannelName;
@property(nonatomic,assign)BOOL isPathPlotted;
@property(nonatomic,assign)BOOL isUpdatedLocation;
@property(nonatomic,assign)BOOL isNewBookingInProcess;
@property(nonatomic,assign)CLLocationCoordinate2D previouCoord;
@property (nonatomic, strong) NSMutableArray *arrBooking;
@property(nonatomic,strong)NSMutableDictionary *allMarkers;
@property(nonatomic,strong)WildcardGestureRecognizer * tapInterceptor;
@property(nonatomic,strong)NSString *patientPubNubChannel;
@property(nonatomic,assign) float currentLatitude;
@property(nonatomic,assign) float currentLongitude;
@property(nonatomic,strong) GMSMarker *currentLocationMarker;

@property (strong,nonatomic)IBOutlet  UIView* acceptRejectBtnView;
@property (strong,nonatomic)IBOutlet  UIView* PickUpAndDropView;
@property (nonatomic) NSUInteger numberOfTabs;
@property(nonatomic,assign) BookingNotificationType bookingStatus;


@property LocationTracker * locationTracker;
@property (nonatomic) NSTimer* locationUpdateTimer;
@property (nonatomic, strong) UIRefreshControl *refreshControl;



-(IBAction)btnAcceptRejectClicked:(id)sender;

@end
static float latitudeChange = 0.01234;
static float longitdeChange = 0.00234;


@implementation HomeViewController
@synthesize arrBooking;
@synthesize acceptRejectBtnView;
@synthesize PickUpAndDropView;
@synthesize dictPush;
@synthesize timer;
@synthesize counter;
@synthesize customNavigationBarView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(NSString *)getMonths
{
    NSDate *date = [NSDate date];
    NSCalendar *calendarLoc = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendarLoc components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    NSInteger Day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    //
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    NSString * monthString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:month]];
    NSString * dayString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:Day]];
    
    NSString *retMonth = [NSString stringWithFormat:@"%ld-%@-%@",(long)year,monthString,dayString];
    return retMonth;
}
#pragma mark Webservice Request -
-(void)getPatientAppointment
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
    NSString *month = [self getMonths];
    NSDictionary *parameters = @{kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId:[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 @"ent_appnt_dt":month,
                                 @"ent_date_time":[Helper getCurrentDateTime]};
    
    
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient postPath:MethodAppointments parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
         [self.tblView setValue:@NO forKeyPath:@"hidden"];
        
        [self getPatientAppointmentResponse:operation.responseString.JSONValue];
     
      
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [Helper showAlertWithTitle:@"Error!" Message:error.localizedDescription];
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
    }];
    
}
- (void)requestUpdateDoctorStatus:(NSString *)statusType{  //statusType 3-->Active ,4-->InActive
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
    
    NSDictionary *parameters = @{kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],kSMPCommonDevideId:[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 KSMPMasterStatus:statusType,@"ent_date_time":[Helper getCurrentDateTime]};
    
    
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    
    // [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [httpClient postPath:MethodUpdateMasterStaus parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self getDoctorStatusResponse:operation.responseString.JSONValue];
      NSLog(@"Request Successful, response");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        
        [Helper showAlertWithTitle:@"Error" Message:error.localizedDescription];
        
        
    }];
    
}


- (NSDateFormatter *)formatter {
    
    //EEE - day(eg: Thu)
    //MMM - month (eg: Nov)
    // dd - date (eg 01)
    // z - timeZone
    
    //eg : @"EEE MMM dd HH:mm:ss z yyyy"
    
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd";
    });
    return formatter;
}
-(void)getPatientAppointmentResponse:(NSDictionary *)response
{
     [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"Badge"];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    [self.refreshControl endRefreshing];
    
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }else if ([[response objectForKey:@"errFlag"] intValue] == 1 && ([[response objectForKey:@"errNum"] intValue] == 6|| [response[@"errNum"] intValue] == 7)) {   // session token expire
        
        
        [self performSelector:@selector(userSessionTokenExpire) withObject:nil afterDelay:5];
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Expired" message:@"Your session with privMD is expired. Please Login again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
        
        
        
        
    }else if ([[response objectForKey:@"errFlag"] intValue] == 1 && ([[response objectForKey:@"errNum"] intValue] == 6|| [response[@"errNum"] intValue] == 11 || [response[@"errNum"] intValue] == 76 )){ //Profile under verification
        
        [self addTransparentViewWhenProfileIsUnderVerification:response[@"errMsg"]];
    }
    else
    {
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            /*
            
            self.appointmentsArr = [[dictResponse objectForKey:@"appointments"] mutableCopy];
            
            
            [customNavigationBarView setRightBarButtonTitle:response[@"penCount"]];
            
            self.tblView = (UITableView *)[self.scrolView viewWithTag:100];
            [self.tblView reloadData];
            
             */
              self.tblView.hidden = NO;
            
            newAppointments = dictResponse[@"newReqs"];
            pendingAppointments = dictResponse[@"appointments"];
            
           // NSLog(@"newAppontmets %lu , pendingAppointment %lu",(unsigned long)newAppointments.count,(unsigned long)pendingAppointments.count);
             
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
            [self.tblView reloadData];
            
             self.imageViewNoAppointments.hidden = YES;
            
          //  self.lblAvailabilty.hidden = NO;
          //  self.switchDoctorStatus.hidden = NO;
            
            
        }
        else
        {
            pendingAppointments = [[dictResponse objectForKey:@"appointments"] mutableCopy];
            newAppointments = dictResponse[@"newReqs"];
           
             [self.tblView reloadData];
            
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
            self.tblView.hidden = YES;
            self.imageViewNoAppointments.hidden = NO;
            
         //   self.lblAvailabilty.hidden = YES;
          //  self.switchDoctorStatus.hidden = YES;
            
            
        }
    }
    
}

- (void)getDoctorStatusResponse:(NSDictionary *)response{
    // NSLog(@"response:%@",response);
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }else if ([[response objectForKey:@"errFlag"] intValue] == 1 && [[response objectForKey:@"errNum"] intValue] == 6) {   // session token expire
        
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        return;
    }
    else
    {
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            
            
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
        }
        else
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
    
    
}

#pragma mark-view life cycle

- (void)viewDidLoad
{
    counter = 0;
    
    
    
    
    waypoints_ = [[NSMutableArray alloc]init];
    waypointStrings_ = [[NSMutableArray alloc]initWithCapacity:2];
    
    
    //[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isBooked"];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:37.778376 longitude:-122.409853 zoom:mapZoomLevel];
    
    customMapView.backgroundColor = [UIColor clearColor];
    //  CGRect rectMap = CGRectMake(0, self.view.bounds.size.height - 68, 320, self.view.bounds.size.height);
    CGRect rectMap = CGRectMake(0, self.view.bounds.size.height , 320, 1);
    mapView_ = [GMSMapView mapWithFrame:rectMap camera:camera];
    mapView_.delegate = self;
    mapView_.myLocationEnabled = YES;
    mapView_.settings.myLocationButton = YES;
    mapView_.settings.compassButton = YES;
    customMapView = mapView_;
    [self.view addSubview:customMapView];
    
    [self addArrowButtonToMapview];
    
    
    // [self.view bringSubviewToFront:PickUpAndDropView];
    // [self.view bringSubviewToFront:acceptRejectBtnView];
    
    [Helper setToLabel:labelPickupTitle Text:@"Pickup Location" WithFont:Robot_CondensedRegular FSize:11 Color:UIColorFromRGB(0x6e6e6e)];
    [Helper setToLabel:labelDropOffTitle Text:@"DropOff Loaction" WithFont:Robot_CondensedRegular FSize:11 Color:UIColorFromRGB(0x6e6e6e)];
    [Helper setToLabel:labelPickeup  Text:nil WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x000000)];
    [Helper setToLabel:labelDropOff Text:nil WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x000000)];
    [Helper setToLabel:labelPickUpdistance Text:@"" WithFont:Robot_CondensedRegular FSize:16 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:labelDropOffdistance Text:@"" WithFont:Robot_CondensedRegular FSize:16 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:labelPickUpMile Text:@"Mile" WithFont:Robot_Regular FSize:11 Color:UIColorFromRGB(0x000000)];
    [Helper setToLabel:labelDropOffMile Text:@"Mile" WithFont:Robot_Regular FSize:11 Color:UIColorFromRGB(0x000000)];
    [Helper setButton:btnAccept Text:@"ACCEPT" WithFont:Robot_CondensedLight FSize:15 TitleColor:UIColorFromRGB(0x24ff00) ShadowColor:nil];
    [Helper setButton:btnReject Text:@"REJECT" WithFont:Robot_CondensedLight FSize:15 TitleColor:UIColorFromRGB(0xff0000) ShadowColor:nil];
    [Helper setToLabel:self.labelDateHeader Text:@"" WithFont:ZURICH_CONDENSED FSize:34 Color:UIColorFromRGB(0xffffff)];
    [Helper setToLabel:self.lblAvailabilty Text:@"Available" WithFont:ZURICH_LIGHTCONDENSED FSize:17 Color:UIColorFromRGB(0x333333)];
    
    [self addCustomNavigationBar];
    
    
    
    
    
    _currentLatitude = 13.028866;
    _currentLongitude = 77.589760;
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    [self.tblView addSubview:self.refreshControl];
    
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [mapView_ addObserver:self forKeyPath:@"myLocation" options:NSKeyValueObservingOptionNew context:NULL];
    
    [self.tblView setValue:@YES forKeyPath:@"hidden"];
    
    
    // Ask for My Location data after the map has already been added to the UI.
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView_.myLocationEnabled = YES;
    });
    
    NSString *bookingDate = [Helper getCurrentDateTime:[NSDate date] dateFormat:@"MM/dd/YY"];
    self.labelDateHeader.text = [NSString stringWithFormat:@"%@",bookingDate];
    
    self.navigationController.navigationBarHidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getPatientAppointment) name:@"AppointmentCancel" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handelAcceptRejectFromAdmin:) name:@"HandleAcceptAndRejectFromAdmin" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getPatientAppointment) name:@"NewBooking" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    
    [self getDoctorCurrentStatus];
    
    
}




- (void) viewDidAppear:(BOOL)animated{
    
    if ([Helper isConnected]) {
        [self getPatientAppointment];
        
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        NSString *status = [ud valueForKey:@"DoctorStatus"];
        
        if ([status intValue] == 3) {
            self.switchDoctorStatus.on  = YES;
        }else{
            self.switchDoctorStatus.on = NO;
        }
        
        [self requestUpdateDoctorStatus:status];
        
    }else{
        [Helper showAlertViewForInternetCheck];
    }
    
    
    
    if (IS_SIMULATOR)
    {
        _currentLatitude = 13.028866;
        _currentLongitude = 77.589760;
        
        
        CLLocationCoordinate2D locationCord = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
        mapView_.camera = [GMSCameraPosition cameraWithTarget:locationCord
                                                         zoom:14];
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
        marker.title = @"It's Me";
        marker.snippet = @"Current Location";
        marker.map = mapView_;
        
        //subscribe to pubnub to get stream of doctors location
        //[self startPubNubStream];
        
    }
    
    
    
    
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self startUpdatingLocationToServer];
    });
    
    
    
}



- (void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AppointmentCancel" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HandleAcceptAndRejectFromAdmin" object:nil];
     [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NewBooking" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    
    [_allMarkers removeAllObjects];
    _isUpdatedLocation = NO;
   
    
    
    [mapView_ removeObserver:self forKeyPath:@"myLocation" context:NULL];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






-(void)uploadFile:(UploadFiles *)uploadfile didUploadSuccessfullyWithUrl:(NSArray *)imageUrls{
    //  NSLog(@"image url %@",imageUrls);
}

-(void)startUpdatingLocationToServer{
    
    self.locationTracker = [LocationTracker  sharedInstance];
    [self.locationTracker startLocationTracking];
    
    //Send the best location to server every 60 seconds
    //You may adjust the time interval depends on the need of your app.
    NSTimeInterval time = 2.0;
    self.locationUpdateTimer =
    [NSTimer scheduledTimerWithTimeInterval:time
                                     target:self
                                   selector:@selector(updateLocation)
                                   userInfo:nil
                                    repeats:YES];
}
-(void)updateLocation {
    // NSLog(@"updateLocation");
    
    [self.locationTracker updateLocationToServer];
}

#pragma mark Other Methods -
-(void)userSessionTokenExpire{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    [[[XDKAirMenuController sharedMenu] view] removeFromSuperview];
    
    [XDKAirMenuController relese];
   // if ([XDKAirMenuController relese]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        SplashViewController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
   // }
    
}


- (void)refreshTable{
    if ([Helper isConnected]) {
        [self getPatientAppointment];
        
       
        
    }else{
        [Helper showAlertViewForInternetCheck];
    }

    
    
}


#pragma mark BookingInfoView Animation

-(void)hideBookingInfoViews{
    
    _isNewBookingInProcess = NO;
    
    [self startAnimationDownForBottom];
    [self startAnimationDownForTop];
    
    [_progressView removeFromSuperview];
    if (timer) {
        [timer invalidate];
    }
}
-(void)showBookingInfoView{
    
    
    _isNewBookingInProcess = YES;
    [self startCoundownTimer];
    [self startAnimationUpForBottom];
    [self startAnimationUpForTop];
}
-(void)startAnimationDownForBottom
{
    CGRect frame = acceptRejectBtnView.frame;
    frame.origin.y = [UIScreen mainScreen].bounds.size.height;
    frame.origin.x = 0;
    
    [UIView animateWithDuration:0.2
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^
     {
         
         
         acceptRejectBtnView.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         [acceptRejectBtnView setHidden:YES];
         
     }];
}

-(void)startAnimationDownForTop
{
    CGRect frame = PickUpAndDropView.frame;
    frame.origin.y = -99;
    frame.origin.x = 0;
    [UIView animateWithDuration:0.2
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^(){
                         
                         PickUpAndDropView.frame = frame;
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Completed");
                         [PickUpAndDropView setHidden:YES];
                         
                     }];
}


-(void)startAnimationUpForBottom
{
    
    
    [acceptRejectBtnView setHidden:NO];
    
    
    CGRect frame = acceptRejectBtnView.frame;
    frame.origin.y = [UIScreen mainScreen].bounds.size.height - 88;
    frame.origin.x = 0;
    
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^(){
                         
                         
                         acceptRejectBtnView.frame = frame;
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Completed");
                         
                     }];
    
    
}

-(void)startAnimationUpForTop
{
    [PickUpAndDropView setHidden:NO];
    
    //PickUpAndDropView.frame = CGRectMake(0, -64-91, 320, 91);
    //   PickUpAndDropView.frame = CGRectMake(0, 0, 320, 89);
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^
     {
         CGRect frame = PickUpAndDropView.frame;
         frame.origin.y = 64;
         frame.origin.x = 0;
         PickUpAndDropView.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         
     }];
    
    
    
    
}






-(void)addgestureToMap
{
    _tapInterceptor = [[WildcardGestureRecognizer alloc] init];
    //  CLLocation *location = mapView_.myLocation;
    
    __weak typeof(self) weakSelf = self;
    _tapInterceptor.touchesBeganCallback = ^(NSSet * touches, UIEvent * event , int touchtype)
    {
        if (touchtype == 1)
        {
            //[weakSelf startAnimation];
        }
        else {
            // [weakSelf endAnimation];
        }
        
    };
    [mapView_  addGestureRecognizer:_tapInterceptor];
    
    
}


#pragma Circula Progress View

-(void)startCoundownTimer{
    
    _progressView = [[PICircularProgressView alloc]initWithFrame:CGRectMake(320/2-50, 155, 100, 100)];
    [self.view addSubview:_progressView];
    self.progressView.thicknessRatio=0.5;
    self.progressView.progress = 1;
    self.progressView.showText =YES;
    self.progressView.roundedHead = YES;
    
    
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
}


- (void)timerTick
{
    float Newvalue = _progressView.progress - 0.06666667;
    _progressView.progress = Newvalue;
    if (Newvalue <= 0.0) {
        [timer invalidate];
        [_progressView removeFromSuperview];
        [self hideBookingInfoViews];
    }
    
}
-(void)getpassengerDeatil
{
    ResKitWebService * rest = [ResKitWebService sharedInstance];
    NSDictionary *queryParams= nil;
    NSDictionary * dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"PUSH"];
    NSDictionary * dictP = [dict objectForKey:@"aps"];
    queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                   [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                   [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                   [dictP objectForKey:@"e"],kSMPSignUpEmail,
                   [dictP objectForKey:@"dt"], kSMPRespondBookingDateTime,
                   constkNotificationTypeBookingType,kSMPPassengerUserType,
                   [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    
    TELogInfo(@"param%@",queryParams);
    [rest composeRequestForPassengerDetail:MethodAppointmentDetail
                                   paramas:queryParams
                              onComplition:^(BOOL success, NSDictionary *response){
                                  if (success) { //handle success response
                                      [self passengerDetailResponse:(NSArray*)response];
                                  }
                                  else{//error
                                      
                                  }
                              }];
    
    
    
}

-(void)passengerDetailResponse:(NSArray*)response
{
    Errorhandler * handler = [response objectAtIndex:0];
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    
    if ([[handler errFlag] intValue] ==0)
    {
        passDetail * profile = handler.objects;
        [Helper setToLabel:labelPickeup  Text:profile.addr1 WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x000000)];
        [Helper setToLabel:labelDropOff Text:profile.dropAddr1 WithFont:Robot_Regular FSize:13 Color:UIColorFromRGB(0x000000)];
        [Helper setToLabel:labelPickUpdistance Text:profile.apptDis WithFont:Robot_CondensedRegular FSize:16 Color:UIColorFromRGB(0x333333)];
        [Helper setToLabel:labelDropOffdistance Text:profile.dis WithFont:Robot_CondensedRegular FSize:16 Color:UIColorFromRGB(0x333333)];
        
        dictpassDetail = [[NSMutableDictionary alloc]init];
        [dictpassDetail setObject:profile.fName forKey:@"fName"];
        [dictpassDetail setObject:profile.lName forKey:@"lName"];
        [dictpassDetail setObject:profile.mobile forKey:@"mobile"];
        [dictpassDetail setObject:profile.addr1 forKey:@"addr1"];
        [dictpassDetail setObject:profile.addr2 forKey:@"addr2"];
        [dictpassDetail setObject:profile.amount forKey:@"amount"];
        [dictpassDetail setObject:profile.fare forKey:@"fare"];
        [dictpassDetail setObject:profile.dis forKey:@"dis"];
        [dictpassDetail setObject:profile.dur forKey:@"dur"];
        [dictpassDetail setObject:profile.pPic  forKey:@"pPic"];
        [dictpassDetail setObject:profile.dropAddr1 forKey:@"dropAddr1"];
        [dictpassDetail setObject:profile.dropAddr2 forKey:@"dropAddr2"];
        [dictpassDetail setObject:profile.pickLat forKey:@"pickLat"];
        [dictpassDetail setObject:profile.pickLong forKey:@"pickLong"];
        [dictpassDetail setObject:profile.dropLat forKey:@"dropLat"];
        [dictpassDetail setObject:profile.dropLong forKey:@"dropLong"];
        [dictpassDetail setObject:profile.apptDis forKey:@"apptDis"];
        
        
        //[self startCoundownTimer];
        
        //[self updateDestinationOnMapWithLocation];
        
    }
    else
    {
        [self hideBookingInfoViews];
        
        if ([[handler errNum] integerValue] == 7 || [[handler errNum] integerValue] == 6) {
            
            [Helper showAlertWithTitle:@"Message" Message:handler.errMsg];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                        @"Main" bundle:[NSBundle mainBundle]];
            
            HelpViewController *help = [storyboard instantiateViewControllerWithIdentifier:@"helpVC"];
            
            self.navigationController.viewControllers = [NSArray arrayWithObjects:help, nil];
        }
        else
        {
            [Helper showAlertWithTitle:@"Message" Message:handler.errMsg];
            
            
        }
        
        
    }
    
}

-(void)updateDestinationOnMapWithLocation {
    
    [_mutablePath removeAllCoordinates];
    
    //  for (int i = 0; i<6; i++) {
    //float latitude = 12.9128118;//[dictpassDetail[@"pickLat"] floatValue];
    // float longitude = 77.6092188;//[dictpassDetail[@"pickLong"] floatValue];
    
    float latitude = [dictpassDetail[@"dropLat"] floatValue];
    float longitude = [dictpassDetail[@"dropLong"] floatValue];
    
    latitudeChange =  latitudeChange +  0.12334;
    longitdeChange = longitdeChange +  0.12334;
    
    latitude = latitude + latitudeChange;
    longitude = longitude + longitdeChange;
    
    [self updateDestinationLocationWithLatitude:latitude Longitude:longitude];
    //  }
    //[waypointStrings_ insertObject:destinationLocation atIndex:1];
    
}


#pragma mark Get Doctor current status

- (void)getDoctorCurrentStatus{
    
    
    PatientPubNubWrapper *pubNub = [PatientPubNubWrapper sharedInstance];
    
    NSString *doctorPubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDoctorSubscribeChanelKey];
    
    [pubNub subscribeOnChannels:@[doctorPubNubChannel]];
    
    [pubNub setDelegate:self];
    
    NSString *driverEmail = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUDoctorEmailAddressKey];
    
    NSDictionary *message =  @{@"a":@"2",
                               @"email": driverEmail,
                               @"chn":doctorPubNubChannel
                               
                               };
    
    
    [pubNub sendMessageAsDictionary:message toChannel:[[NSUserDefaults standardUserDefaults] objectForKey:kNSUDoctorServerChanelKey]];
}


#pragma mark PubNub Delegates -
- (void)recievedMessage:(NSDictionary*)messageDict onChannel:(NSString*)channelName{
    
    
    
    if ([messageDict[@"a"] intValue] == 2) {
        NSLog(@"recievedMessage home:%@",messageDict);
        
        if ([messageDict[@"s"] intValue] == 0) { //  Active, will get bookings
            
            UIView *transparentView = [self.view viewWithTag:500];
            if (transparentView) {
                
                [transparentView removeFromSuperview];
            }
            
        }else if ([messageDict[@"s"] intValue] == 1){ //Under verification, will not get any bookings
            
            
            [self addTransparentViewWhenProfileIsUnderVerification:messageDict[@"msg"]];
            
            
        }else if ([messageDict[@"s"] intValue] == 2){ //Rejected, get doctor out by showing a error message
            [self addTransparentViewWhenProfileIsUnderVerification:messageDict[@"msg"]];
            
        }
        
    }
}
-(void)didFailedToConnectPubNub:(NSError*)error{
    
}

-(void)getCurrentLocation{
    
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled]) {
        
        _isUpdatedLocation = NO;
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_locationManager requestAlwaysAuthorization];
            }
            
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            
        }
        [_locationManager startUpdatingLocation];
        
        
    }
    else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Service" message:@"Unable to find your location,Please enable location services." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        
    }
    
}



#pragma mark- GMSMapviewDelegate

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    CGRect frame = customMapView.frame;
    if (frame.origin.y == 500 || frame.origin.y == 412) {
        isMapViewOpened = YES;
        frame.origin.y = 140;
        frame.origin.x = 0;
        frame.size.height = self.view.bounds.size.height;
        UIButton *btn = (UIButton *)[customMapView viewWithTag:100];
        btn.tag = 200;
        
        
        [UIView animateWithDuration:0.4
                              delay:0.1
                            options: UIViewAnimationOptionCurveEaseIn
                         animations:^
         {
             
             
             customMapView.frame = frame;
         }
                         completion:^(BOOL finished)
         {
             NSLog(@"Completed");
             
             /*clear mapview with old marker */
             [mapView clear];
             /* add marker on map */
             [self customMarkers];
             
             //[acceptRejectBtnView setHidden:YES];
             UIButton *btn = (UIButton *)[customMapView viewWithTag:200];
             [btn setHidden:NO];
             /*hide bottom image shadow */
             UIImageView *img = (UIImageView *)[self.view viewWithTag:BottomShadowImageTag];
             img.hidden = YES;
         }];
        
    }
    
}
- (void) mapView:(GMSMapView *) mapView willMove:(BOOL)gesture{
    
    NSLog(@"willMove");
    
}

- (void) mapView:(GMSMapView *) mapView didChangeCameraPosition:(GMSCameraPosition *) position{
    
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
    NSLog(@"idleAtCameraPosition");
    
    /**
     *  don't update location and pubnub stream
     */
    //    if (_isMarkerClicked) {
    //        _isMarkerClicked = NO;
    //        return;
    //
    //    }
    //
    //    if (_isUpdatedLocation) {
    //
    //        CGPoint point1 = mapView_.center;
    //        CLLocationCoordinate2D coor = [mapView_.projection coordinateForPoint:point1];
    //        NSLog(@"center Coordinate idleAtCameraPosition :%f %f",coor.latitude,coor.longitude);
    //
    //        _currentLatitude = coor.latitude;
    //        _currentLongitude = coor.longitude;
    //
    //        if (!_isCurrentLocationLocked) {
    //            [self changePubNubStream];
    //
    //            if (_isLocaitonChangedManually) {
    //                _isLocaitonChangedManually = NO;
    //            }
    //            else {
    //                [self getAddress:coor];
    //            }
    //
    //        }
    //
    //
    //    }
    //
}
- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    
    [self performSegueWithIdentifier:@"appointmentDetail" sender:marker];
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    
    NSLog(@"marker userData %@",marker.userData);
    
    
    //[self stopPubNubStream];
    //    _isMarkerClicked = YES;
    //
    //    NSDictionary *dictionary = _medicalSpecialistDetail[marker.userData];
    //
    //    CustomMarkerView *customMarkerView = [CustomMarkerView sharedInstance];
    //    [customMarkerView updateTitle:dictionary[@"name"]];
    //
    //    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForThumbnailImage,dictionary[@"image"]];
    //    [customMarkerView.profileImage setImageWithURL:[NSURL URLWithString:strImageUrl]
    //                                  placeholderImage:[UIImage imageNamed:@"signup_image_user.png"]
    //                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
    //
    //                                         }];
    //
    //    [customMarkerView updateRating:[dictionary[@"rating"] floatValue]];
    //
    //
    //    return customMarkerView;
    
    /*
     NSArray *arrayAppointments = self.appointmentsArr[pagerView.page][@"appt"];
     
     NSDictionary *dict ;
     for (int i = 0; i < arrayAppointments.count ; i++) {
     
     dict = arrayAppointments[i];
     
     if (marker.position.latitude == [dict[@"lat"] floatValue] && marker.position.longitude == [dict[@"lon"] floatValue]) {
     
     
     break;
     }
     }
     
     UIView *customMarkerview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 120, 50)];
     customMarkerview.backgroundColor = [UIColor blackColor];
     
     
     UILabel *labelTime = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 120, 20)];
     [Helper setToLabel:labelTime Text: WithFont:<#(NSString *)#> FSize:<#(float)#> Color:<#(UIColor *)#>];
     
     */
    
    
    return nil;
}
- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker{
    NSLog(@"didBeginDraggingMarker");
}
- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    NSLog(@"didEndDraggingMarker");
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    
    
    
    if (!_isUpdatedLocation) {
        // If the first location update has not yet been recieved, then jump to that
        // location.
        _isUpdatedLocation = YES;
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                         zoom:14];
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        _currentLocationMarker = [GMSMarker markerWithPosition:position];
        _currentLocationMarker.flat = YES;
        _currentLocationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        _currentLocationMarker.icon = [UIImage imageNamed:@"map_icn_location.png"];
    }
    else {
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        _currentLocationMarker.position = position;
    }
    
}


#pragma mark - CLLocation manager deleagte method

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
    
    TELogInfo(@"didupdate");
    
    if (!_isUpdatedLocation) {
        
        [_locationManager stopUpdatingLocation];
        
        //change flag that we have updated location once and we don't need it again
        _isUpdatedLocation = YES;
        
        
        
        
        //change map camera postion to current location
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                                longitude:newLocation.coordinate.longitude
                                                                     zoom:mapZoomLevel];
        [mapView_ setCamera:camera];
        
        
        //add marker at current location
        //        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
        //        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        //        marker.flat = YES;
        //        marker.groundAnchor = CGPointMake(0.5f, 0.5f);
        //        marker.icon = [UIImage imageNamed:@" arrive_mapthumbnail.png"];
        //
        //        marker.map = mapView_;
        //        [waypoints_ addObject:marker];
        //
        //        float latitude = [dictpassDetail[@"pickLat"] floatValue];
        //        float longitude = [dictpassDetail[@"pickLong"] floatValue];
        //
        //
        //        //save current location to plot direciton on map
        //        _startLocation = [NSString stringWithFormat:@"%.6f,%.6f",latitude, longitude];
        //         // _startLocation = [NSString stringWithFormat:@"%.6f,%.6f",newLocation.coordinate.latitude, newLocation.coordinate.longitude];
        //
        //        [waypointStrings_ insertObject:_startLocation atIndex:0];
        //
        //
        //        _currentLatitude = newLocation.coordinate.latitude;
        //        _currentLongitude = newLocation.coordinate.longitude;
        //        //[self startPubNubSream];
        //        //[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(startPubNubSream) userInfo:nil repeats:YES];
        //
        //        //subscribe to pubnub to get doctors location stream
        //       // [self subsCribeToPubNubOnLocation:newLocation.coordinate.latitude Longitude:newLocation.coordinate.longitude];
        //
        //        NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
        //        NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    }
    else{
        //         _currentLatitude= newLocation.coordinate.latitude;
        //        //change map camera postion to current location
        //        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
        //                                                                longitude:newLocation.coordinate.longitude
        //                                                                     zoom:10];
        //        [mapView_ setCamera:camera];
        //
        //        //add marker at current location
        //
        //        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
        //        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        //        marker.flat = YES;
        //        marker.groundAnchor = CGPointMake(0.5f, 0.5f);
        //        marker.icon = [UIImage imageNamed:@" arrive_mapthumbnail.png"];
        //        marker.map = mapView_;
        
        
        
    }
    
    
    
}


- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
    NSLog(@"locationManager failed to update location : %@",[error localizedDescription]);
}

- (void)addDirections:(NSDictionary *)json {
    
    if ([json[@"routes"] count]>0) {
        NSDictionary *routes = [json objectForKey:@"routes"][0];
        
        NSDictionary *route = [routes objectForKey:@"overview_polyline"];
        NSString *overview_route = [route objectForKey:@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        polyline.map = mapView_;
    }
    
    
}

-(void)updateDestinationLocationWithLatitude:(float)latitude Longitude:(float)longitude{
    
    if (!_isPathPlotted) {
        
        NSLog(@"pathplotted");
        _isPathPlotted = YES;
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(
                                                                     latitude,
                                                                     longitude);
        
        _previouCoord = position;
        
        _destinationMarker = [GMSMarker markerWithPosition:position];
        _destinationMarker.map = mapView_;
        _destinationMarker.flat = YES;
        _destinationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        //        _destinationMarker.icon = [UIImage imageNamed:@"arrive_caricon.png"];
        _destinationMarker.icon = [UIImage imageNamed:@"map_icn_location.png"];
        [waypoints_ addObject:_destinationMarker];
        NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",
                                    latitude,longitude];
        
        [waypointStrings_ addObject:positionString];
        
        if([waypoints_ count]>1){
            NSString *sensor = @"false";
            NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                                   nil];
            NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
            NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                              forKeys:keys];
            DirectionService *mds=[[DirectionService alloc] init];
            SEL selector = @selector(addDirections:);
            [mds setDirectionsQuery:query
                       withSelector:selector
                       withDelegate:self];
        }
        
    }
    else {
        
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        CLLocationDirection heading = GMSGeometryHeading(_previouCoord, position);
        _previouCoord = position;
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        _destinationMarker.position = position;
        [CATransaction commit];
        if (_destinationMarker.flat) {
            _destinationMarker.rotation = heading;
        }
        
    }
    
    
    
}

//- (void)animateToNextCoord:(GMSMarker *)marker {
//
//    CoordsList *coords = marker.userData;
//    CLLocationCoordinate2D coord = [coords next];
//    CLLocationCoordinate2D previous = marker.position;
//
//    CLLocationDirection heading = GMSGeometryHeading(previous, coord);
//    CLLocationDistance distance = GMSGeometryDistance(previous, coord);
//
//    // Use CATransaction to set a custom duration for this animation. By default, changes to the
//    // position are already animated, but with a very short default duration. When the animation is
//    // complete, trigger another animation step.
//
//    [CATransaction begin];
//    [CATransaction setAnimationDuration:(distance / (1 * 1000))];  // custom duration, 50km/sec
//
//    __weak HomeViewController *weakSelf = self;
//    [CATransaction setCompletionBlock:^{
//        [weakSelf animateToNextCoord:marker];
//    }];
//
//    marker.position = coord;
//
//    [CATransaction commit];
//
//    // If this marker is flat, implicitly trigger a change in rotation, which will finish quickly.
//    if (marker.flat) {
//        marker.rotation = heading;
//    }
//}

#pragma mark -button accept and reject action;

-(IBAction)btnAcceptRejectClicked:(id)sender
{
    
    
    NSDictionary * dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"PUSH"];
    NSDictionary * dictP = [dict objectForKey:@"aps"];
    UIButton * button = (UIButton*)sender;
    
    ResKitWebService * restKit = [ResKitWebService sharedInstance];
    switch (button.tag) {
        case 10://accept
        {
            
            [self sendRequestForUpdateStatus];
            
            
            
            break;
        }
        case 11://reject
        {
            
            
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Meassege" message:@"Are you sure you want to reject this booking?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
            [alert show];
            alert.tag =200;
            
            
            
            
            break;
        }
            
            
        default:
            break;
    }
    
}

#pragma mark UIAlertView Delegate -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    
    if (alertView.tag==200)
    {
        
        if (buttonIndex==0) {
            
            ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
            [progressIndicator showPIOnView:self.view withMessage:@"Loading.."];
            
            ResKitWebService * restKit = [ResKitWebService sharedInstance];
            
            NSDictionary * dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"PUSH"];
            NSDictionary * dictP = [dict objectForKey:@"aps"];
            NSDictionary *queryParams;
            queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                           [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                           [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                           [dictP objectForKey:@"e"],kSMPRespondPassengerEmail,
                           [dictP objectForKey:@"dt"], kSMPRespondBookingDateTime,
                           constkNotificationTypeBookingReject,kSMPRespondResponse,
                           constkNotificationTypeBookingType,kSMPRespondBookingType,
                           [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
            
            TELogInfo(@"param%@",queryParams);
            [restKit composeRequestForAccept:MethodRespondToAppointMent
                                     paramas:queryParams
                                onComplition:^(BOOL success, NSDictionary *response){
                                    
                                    if (success) { //handle success response
                                        [self RejectResponse:(NSArray*)response];
                                    }
                                    else{//error
                                        
                                    }
                                }];
        }
        
        
        
    }else if (alertView.tag == 300){
        
        if (buttonIndex == 1) {
            
            NSString *phoneNumber = selectedPhoneNumber; // dynamically assigned
            // NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", phoneNumber];
            // NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
            
            
            NSString *cleanedString = [[phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
            NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
            [[UIApplication sharedApplication] openURL:telURL];
        }
    }else if (alertView.tag  == 400){ // session token expire
        
        if (buttonIndex == 1) {
            
            
            
            
            isAlertViewPresentedForSessionTokenExpire = NO;
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
            [[NSUserDefaults standardUserDefaults] synchronize];
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
            [[[XDKAirMenuController sharedMenu] view] removeFromSuperview];
            
            if ([XDKAirMenuController relese]) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                            @"Main" bundle:[NSBundle mainBundle]];
                
                SplashViewController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
                
                self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
            }
            
            
        }
        
    }
    
    
    
}

#pragma mark -AcceptReject Response
-(void)RejectResponse :(NSArray*)array
{
    TELogInfo(@"_response%@",array);
    Errorhandler * handler = [array objectAtIndex:0];
    
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    
    [self hideBookingInfoViews];
    
    if ([[handler errFlag] intValue] ==0)
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
        isPassangeBookDriver = NO;
        mapView_ .frame = CGRectMake(0, 0, 320, [[UIScreen mainScreen] bounds].size.height);
        
    }
    else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
        mapView_ .frame = CGRectMake(0, 0, 320, [[UIScreen mainScreen] bounds].size.height);
        [Helper showAlertWithTitle:@"Message" Message:handler.errMsg];
    }
    
    
}
-(void)acceptResponse :(NSArray*)array
{
    //[self performSegueWithIdentifier:@"GotoDriverDetailController" sender:self];
    
    Errorhandler * handler = [array objectAtIndex:0];
    [handler description];
    
    if ([[handler errFlag] intValue] == 0) {
        
        [self sendRequestForUpdateStatus];
        
        
    }
    else
    {
        ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
        [progressIndicator hideProgressIndicator];
        
        [Helper showAlertWithTitle:@"Message" Message:handler.errMsg];
        [self hideBookingInfoViews];
        
        
    }
    
}


-(void)sendRequestForUpdateStatus
{
    
    NSDictionary * dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"PUSH"];
    NSDictionary * dictP = [dict objectForKey:@"aps"];
    
    ResKitWebService * restkit = [ResKitWebService sharedInstance];
    NSDictionary *queryParams;
    queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                   [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                   [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                   dictP[@"e"],kSMPRespondPassengerEmail,
                   dictP[@"dt"], kSMPRespondBookingDateTime,
                   constkNotificationTypeBookingOnTheWay,kSMPRespondResponse,
                   @"testing",kSMPRespondDocNotes,
                   [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    
    TELogInfo(@"param%@",queryParams);
    
    
    [restkit composeRequestForUpdateStatus:MethodupdateApptStatus
                                   paramas:queryParams
                              onComplition:^(BOOL success, NSDictionary *response){
                                  
                                  if (success) { //handle success response
                                      [self updateStstusResponse:(NSArray*)response];
                                  }
                                  else{//error
                                      [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                      
                                    
                                  }
                              }];
    
    
    
}

-(void)updateStstusResponse :(NSArray*)response
{
    
    Errorhandler * handler = [response objectAtIndex:0];
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    
    [self hideBookingInfoViews];
    
    
    if ([[handler errFlag] intValue] == kResponseFlagSuccess)
    {
        //[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isBooked"];
        _bookingStatus = kNotificationTypeBookingOnTheWay;
        [self performSegueWithIdentifier:@"GotoDriverDetailController" sender:self];
        
        
    }
    else{
        // [self performSegueWithIdentifier:@"GotoDriverDetailController" sender:self];
        [Helper showAlertWithTitle:@"Message" Message:handler.errMsg];
        //[self performSegueWithIdentifier:@"GotoDriverDetailController" sender:self];
    }
    
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"GotoDriverDetailController"])
    {
        OnBookingViewController *booking = (OnBookingViewController*)[segue destinationViewController];
        booking.bookingStatus = _bookingStatus;
        booking.passDetail = dictpassDetail;
        
        
    }else if ([[segue identifier] isEqualToString:@"NewNotificationDetail"] || [[segue identifier] isEqualToString:@"appointmentDetail"] ){
        GMSMarker *marker = (GMSMarker *)sender;
        
        PendingNotificationDetailController *detail = (PendingNotificationDetailController*)[segue destinationViewController];
        
        if (isMapViewOpened) {
            NSArray *arrayAppointments = pendingAppointments;
            
            for (int i = 0; i < arrayAppointments.count; i++) {
                self.dictAppointments = arrayAppointments[i];
                if ([self.dictAppointments[@"apntTime"] isEqualToString:marker.title]) {
                    break;
                }
                
            }
            
        }
        
        
        detail.dictAppointmentDetails = [self.dictAppointments mutableCopy];
        
        
    }
}
#pragma mark UIButton Action -
- (void)customMarkers{
    NSArray *arrayAppointments = pendingAppointments;
    
    int markerCount = 0;
    for (int i = 0; i < arrayAppointments.count; i++) {
        
        markerCount++;
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,62,77)];
        UIImageView *pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"circular_map_icon.png"]];
        
        
        
        /*
        UILabel *label;
        
        if (markerCount > 9) {
            label = [[UILabel alloc] initWithFrame:CGRectMake(5, 1, 60, 18)];
            label.font = [UIFont fontWithName:HELVETICANEUE_LIGHT size:12];
        }else{
            label = [[UILabel alloc] initWithFrame:CGRectMake(8, 1, 60, 18)];
            label.font = [UIFont fontWithName:HELVETICANEUE_LIGHT size:14];
        }
        //label.textAlignment = NSTextAlignmentCenter;
        label.text = [NSString stringWithFormat:@"%d",markerCount];
        */
        
        
        //[label sizeToFit];
        
      //  [view addSubview:pinImageView];
        //[view addSubview:label];
        
        NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForThumbnailImage,arrayAppointments[i][@"pPic"]];
        
      __block RoundedImageView *patientImg = [[RoundedImageView alloc]initWithFrame:CGRectMake(4, 3.5, 55, 55)];
        
        [patientImg sd_setImageWithURL:[NSURL URLWithString:strImageUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            if (image == nil) {
                patientImg.image = [UIImage imageNamed:@"doctor_image_thumbnail.png"];
                
            }else{
                patientImg.image = image;
            }
        }];
        
        
          [view addSubview:pinImageView];
          [view addSubview:patientImg];
        //i.e. customize view to get what you need
        
        UIImage *markerIcon = [self imageFromView:view];
        
        
        
        NSDictionary *dict = arrayAppointments[i];
        float latitude = [dict[@"apptLat"] floatValue];
        float longitude = [dict[@"apptLong"] floatValue];
        
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(latitude, longitude);
        marker.tappable = YES;
        marker.map = mapView_;
        marker.title = dict[@"apntTime"];
        marker.snippet = dict[@"addrLine2"];
        marker.icon = markerIcon;
        
        // mapView_.selectedMarker = marker;
        
        
        
    }
    // add auto ZoomIn if there is more than 2 markers
    
    if (arrayAppointments.count > 2) {
        GMSCameraUpdate *zoomCamera = [GMSCameraUpdate zoomTo:mapZoomLevel+1];
        [mapView_ animateWithCameraUpdate:zoomCamera];
    }
    
    
    
}

- (UIImage *)imageFromView:(UIView *) view
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (IBAction)btnPreviousTapped{
    
    /*
     if (self.scrolView.contentOffset.x > 1) {
     [self.scrolView setContentOffset:CGPointMake(self.scrolView.contentOffset.x - 320, 0) animated:YES];
     
     NSCalendar *calendar = [NSCalendar currentCalendar];
     if (!self.date) {
     self.date = [NSDate date];
     }
     
     self.date =[calendar dateBySubtractingDays:1 fromDate:self.date];
     NSLog(@"%@",self.date);
     NSString *bookingDate = [Helper getCurrentDateTime:self.date dateFormat:@"MM/dd/YY"];
     self.labelDateHeader.text = [NSString stringWithFormat:@"%@",bookingDate];
     
     NSLog(@"page no :%f ",(self.scrolView.contentOffset.x / self.scrolView.frame.size.width));
     
     
     }
     */
    if (pagerView.page > 0) {
        //  pagerView.page = pagerView.page - 1;
        [pagerView setPage:pagerView.page - 1];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        if (!self.date) {
            self.date = [NSDate date];
        }
        
        self.date =[calendar dateBySubtractingDays:1 fromDate:self.date];
        // NSLog(@"%@",self.date);
        // NSString *bookingDate = [Helper getCurrentDateTime:self.date dateFormat:@"MM/dd/YY"];
        // self.labelDateHeader.text = [NSString stringWithFormat:@"%@",bookingDate];
        
        self.labelDateHeader.text =  self.appointmentsArr[pagerView.page][@"mmddyy"] ;
        
        // NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        // [dateFormatter setDateFormat:@"yyyy-dd-mm"];
        // NSDate *now = [dateFormatter dateFromString:self.appointmentsArr[pagerView.page][@"date"]];
        //  self.labelDateHeader.text = [Helper getCurrentDateTime:now dateFormat:@"MM/dd/YY"];
        // NSLog(@"page no :%d ",pagerView.page);
        
    }
    
    
}
- (IBAction)btnNextTapped{
    /*
     if (self.scrolView.contentOffset.x < 320*6) {
     [self.scrolView setContentOffset:CGPointMake(self.scrolView.contentOffset.x + 320, 0) animated:YES];
     
     }
     */
    if (pagerView.page < 6) {
        //pagerView.page = pagerView.page + 1;
        [pagerView setPage:pagerView.page + 1];
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        if (!self.date) {
            self.date = [NSDate date];
        }
        
        self.date =[calendar dateByAddingDays:1 toDate:self.date];
        NSLog(@"%@",self.date);
        
        // NSString *bookingDate = [Helper getCurrentDateTime:self.date dateFormat:@"MM/dd/YY"];
        //  self.labelDateHeader.text = [NSString stringWithFormat:@"%@",bookingDate];
        
        
        self.labelDateHeader.text =  self.appointmentsArr[pagerView.page][@"mmddyy"] ;
        NSLog(@"page no :%d ",pagerView.page);
        
    }
    
}

- (IBAction)updateDoctorStatus:(UISwitch *)sender{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    if (sender.on) {
        self.lblAvailabilty.text = @"Available";
        [self requestUpdateDoctorStatus:@"3"];
        [ud setValue:@"3" forKey:@"DoctorStatus"];
        
    }else{
        self.lblAvailabilty.text = @"Not Available";
        [self requestUpdateDoctorStatus:@"4"];
        [ud setValue:@"4" forKey:@"DoctorStatus"];
    }
    
    
}
/*
- (void)phoneNumberTapped:(UIButton *)sender{
    
    NSArray *arrayAppointments = self.appointmentsArr[pagerView.page][@"appt"];
    NSDictionary *dictAppointDetail = arrayAppointments[sender.tag];
    selectedPhoneNumber = dictAppointDetail[@"phone"];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Call" message:@"Do you want to make a call on this number?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
    alert.tag = 300;
    [alert show];
}
 */
#pragma mark UITableView Delegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
       UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 22)];
    labelHeader.textAlignment = NSTextAlignmentCenter;
    if (section == 0) {
              [Helper setToLabel:labelHeader Text:@"NEW APPOINTMENT REQUEST" WithFont:ZURICH_LIGHTCONDENSED FSize:13 Color:UIColorFromRGB(0xffffff)];
        labelHeader.backgroundColor = UIColorFromRGB(0xE99580);
    }else if (section == 1){
        [Helper setToLabel:labelHeader Text:@"TODAY'S SCHEDULE" WithFont:ZURICH_LIGHTCONDENSED FSize:13 Color:UIColorFromRGB(0xffffff)];
        labelHeader.backgroundColor = UIColorFromRGB(0x78D2A2);
    }
 
  
    return labelHeader;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.tblView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
       HomeScreenCell *cell = (HomeScreenCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        if (newAppointments.count > 0 && !cell.isAppointmentExpired) {
            self.dictAppointments = newAppointments[indexPath.row];
            [self performSegueWithIdentifier:@"NewNotificationDetail" sender:indexPath];
        }else if (!cell.isAppointmentExpired){
            
            [Helper showAlertWithTitle:@"OOPS!" Message:@"This appointment expired."];
        }
       
    }else{
        if (pendingAppointments.count > 0) {
            
            self.dictAppointments = pendingAppointments[indexPath.row];
            [self performSegueWithIdentifier:@"appointmentDetail" sender:self];
        }
       
    }
  
    
   // [self performSegueWithIdentifier:@"appointmentDetail" sender:self];
    
}


#pragma mark UITableview DataSource -


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (newAppointments.count == 0 && pendingAppointments.count == 0) {
        return 0;
    }
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // NSLog(@"total count :%d",[self.appointmentsArr[0][@"appt"] count]);
    
    if (section == 0) {
        if (newAppointments.count == 0) {
            return 1;
        }
        return newAppointments.count;
    }else if (section == 1)
        if (pendingAppointments.count == 0) {
            return 1;
        }
        return pendingAppointments.count;
    
    return 0;
  
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"HomeViewCell";
    HomeScreenCell *cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    
    [self hideCellSubViews:cell hide:NO];
    
    cell.delegate = self;
    [Helper setToLabel:cell.customerAddress Text:@"" WithFont:ZURICH_LIGHTCONDENSED FSize:13 Color:UIColorFromRGB(0x000000)];
    
    
    NSDictionary *dictAppointDetail;
    
  
    
    if (indexPath.section == 0) {
        
         cell.backgroundColor = UIColorFromRGB(0xF1F0F0);
        if (newAppointments.count == 0) {
            
            [self hideCellSubViews:cell hide:YES];
            [Helper setToLabel:cell.lblNoAppointments Text:@"You have no new Appoitments." WithFont:ZURICH_LIGHTCONDENSED FSize:20 Color:BLACK_COLOR];
            
            return cell;
        }

        
        cell.labelMin.hidden = NO;
        cell.timerLabel.hidden = NO;
        cell.customView.hidden = NO;
       
        dictAppointDetail = newAppointments[indexPath.row];
        
        int expireTime = [dictAppointDetail[@"expireTime"] intValue];
        cell.timeMin = expireTime/60;
        cell.timeSec = expireTime%60;
        [cell startTimer];

         cell.customerAddress.text = [NSString stringWithFormat:@"%@ %@ miles",dictAppointDetail[@"addrLine2"],dictAppointDetail[@"distance"]];
    }else{
        
        if (pendingAppointments.count == 0) {
            [self hideCellSubViews:cell hide:YES];
            [Helper setToLabel:cell.lblNoAppointments Text:@"You have no appointments scheduled for today." WithFont:ZURICH_LIGHTCONDENSED FSize:20 Color:BLACK_COLOR];
            
            return cell;

        }
        
        dictAppointDetail = pendingAppointments[indexPath.row];
        cell.backgroundColor = UIColorFromRGB(0xFFFFFF);
        cell.labelMin.hidden = YES;
        cell.timerLabel.hidden = YES;
        cell.customView.hidden = YES;
        
        if ([dictAppointDetail[@"status"] intValue] == 5) {
            cell.customerAddress.text = @"Am on the way";
            
        }else if ([dictAppointDetail[@"status"] intValue] == 6){
             cell.customerAddress.text = @"I have arrived";
        }else if ([dictAppointDetail[@"status"] intValue] == 7){
            cell.customerAddress.text = @"Raise invoice";
        }else{
            cell.customerAddress.text = [NSString stringWithFormat:@"%@ %@ miles",dictAppointDetail[@"addrLine2"],dictAppointDetail[@"distance"]];
        }
        
    }

       
   
    NSString *strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForThumbnailImage,dictAppointDetail[@"pPic"]];
    
   //__weak HomeScreenCell *weakCell = (HomeScreenCell *)cell;
    [cell.customerImage sd_setImageWithURL:[NSURL URLWithString:strImageUrl] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    [Helper setToLabel:cell.appointmentTime Text:@"" WithFont:ZURICH_LIGHTCONDENSED FSize:20 Color:UIColorFromRGB(0x006699)];
    [Helper setToLabel:cell.customerName Text:@"" WithFont:ZURICH_LIGHTCONDENSED FSize:18 Color:UIColorFromRGB(0x000000)];
    [Helper setToLabel:cell.timerLabel Text:@"" WithFont:ZURICH_LIGHTCONDENSED FSize:15 Color:UIColorFromRGB(0xffffff)];
    [Helper setToLabel:cell.labelMin Text:@"MIN" WithFont:ZURICH_LIGHTCONDENSED FSize:17 Color:UIColorFromRGB(0xffffff)];
    
    
    cell.appointmentTime.text = dictAppointDetail[@"apntTime"];
    cell.customerName.text = dictAppointDetail[@"fname"];
    
   
    if ([dictAppointDetail[@"bookType"] intValue] == 1) {
        cell.circularSmallView.image = [UIImage imageNamed:@"home_green_dot_online_icon.png"];
    }else{
          cell.circularSmallView.image = [UIImage imageNamed:@"home_green_dot_offline_icon.png"];
    }
    
    
    
        
        
        return cell;
    
}

#pragma mark HomeScreenCell Delegate -
- (void)phoneNumberTapped:(HomeScreenCell *)cell{
    
    NSIndexPath *indexPath = [self.tblView indexPathForCell:cell];
    
    int section = indexPath.section ;
    int row = indexPath.row ;
    if (section == 0) {
        selectedPhoneNumber = newAppointments[row][@"phone"];
    }else{
        selectedPhoneNumber = pendingAppointments[row][@"phone"];
    }
    
 
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Call" message:@"Do you want to make a call on this number?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
    alert.tag = 300;
    [alert show];

    
}

- (void)refreshAppointmentList:(HomeScreenCell *)cell {
    
    
    cell.isAppointmentExpired = YES;
    
}

#pragma mark- Custom Methods

- (void)addGradientToView:(UIImageView *)imageView
{
    /*
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = view.bounds;
   // gradient.colors = @[(id)[[UIColor lightGrayColor] CGColor],
                       // (id)[[UIColor whiteColor] CGColor]];
    gradient.colors = @[(id)[[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0] CGColor],
                        (id)[[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0] CGColor],
                        (id)[[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0] CGColor]];
    [view.layer insertSublayer:gradient atIndex:0];
     */
    
    UIView *bottomShadow = [[UIView alloc] initWithFrame:CGRectMake(60/2 -34/2, 27, imageView.frame.size.width - 15, 34)];
    
    CAGradientLayer *bottomGradient = [CAGradientLayer layer];
    bottomGradient.frame = bottomShadow.bounds;
    bottomGradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:0 green:0 blue:0 alpha:0.01] CGColor], (id)[[UIColor colorWithRed:0 green:0 blue:0 alpha:.1] CGColor],(id)[[UIColor colorWithRed:0 green:0 blue:0 alpha:.2] CGColor],[[UIColor colorWithRed:0 green:0 blue:0 alpha:.4] CGColor], nil];
    [bottomShadow.layer insertSublayer:bottomGradient atIndex:0];
    [imageView addSubview:bottomShadow];
}


- (void)hideCellSubViews:(HomeScreenCell *)cell hide:(BOOL)isHide{
    if (isHide) {
       
        cell.customerName.hidden = YES;
        cell.customerImage.hidden = YES;
        cell.customerAddress.hidden = YES;
        cell.btnPhoneTapped.hidden = YES;
        cell.imgNext.hidden = YES;
        cell.appointmentTime.hidden = YES;
        cell.labelMin.hidden = YES;
        cell.timerLabel.hidden = YES;
        cell.circularSmallView.hidden = YES;
        
        cell.lblNoAppointments.hidden = NO;
        cell.customView.hidden  = YES;

    }else{
        
        cell.lblNoAppointments.hidden = YES;
        
        cell.appointmentTime.hidden = NO;
        cell.customerName.hidden = NO;
        cell.customerImage.hidden = NO;
        cell.customerAddress.hidden = NO;
        cell.btnPhoneTapped.hidden = NO;
        cell.imgNext.hidden = NO;
        cell.labelMin.hidden = NO;
        cell.timerLabel.hidden = NO;
        cell.circularSmallView.hidden = NO;
        cell.customView.hidden = NO;

    }
    
}
- (void)handelAcceptRejectFromAdmin:(NSNotification *)notification{
    
    NSDictionary *dict = notification.userInfo;
    
    
    
    int type = [dict[@"aps"][@"nt"] intValue];
    if (type == 11) { // 11 == accept , 12 == reject
        
        [Helper showAlertWithTitle:@"Accepted!" Message:dict[@"alert"]];
        UIView *transparentView = [self.view viewWithTag:500];
        if (transparentView) {
            
            [transparentView removeFromSuperview];
        }
        
        
    }else{
        [Helper showAlertWithTitle:@"Rejected!" Message:dict[@"alert"]];
        [self addTransparentViewWhenProfileIsUnderVerification:dict[@"alert"]];
    }
}
- (void) addCustomNavigationBar{
    
    customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 64)];
    
    float deviceWidth = [UIScreen mainScreen].bounds.size.width;
    
    UIImageView *logoImage = [[UIImageView alloc]initWithFrame:CGRectMake(deviceWidth/2 - 123/2, 64/2 - 31/2, 123, 31)];
    logoImage.image = [UIImage imageNamed:@"home_navigationbarlogo.png"];
    [customNavigationBarView addSubview:logoImage];
    
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
   [customNavigationBarView addRightBarButton:YES];
    [self.view addSubview:customNavigationBarView];
    
    
    //--------------****-----------***********-------------*******--------------*****--------------------//
    
    
}

- (void)customRightBarButton:(CustomNavigationBar *)navBar{
    UIView *viewNavBar = [[UIView alloc] initWithFrame:CGRectMake(223, 20, 133/2, 44)];
    viewNavBar.backgroundColor = [UIColor clearColor];
    
    UIButton *buttonNewNotification = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonNewNotification.frame = CGRectMake(0, 0, 133/2, 44);
    [buttonNewNotification setBackgroundImage:[UIImage imageNamed:@"homescreeenlistview_notification_btn_off.png"] forState:UIControlStateNormal];
    [buttonNewNotification setBackgroundImage:[UIImage imageNamed:@"homescreeenlistview_notification_btn_on.png"] forState:UIControlStateHighlighted];
    
    [Helper setButton:buttonNewNotification Text:@"12" WithFont:HELVETICANEUE_LIGHT FSize:10 TitleColor:[UIColor blackColor] ShadowColor:nil];
    
    [buttonNewNotification setTitleEdgeInsets:UIEdgeInsetsMake(-15, 0, 0, -13)];
    // [buttonNewNotification addTarget:self action:@selector(getNewNotifications) forControlEvents:UIControlEventTouchUpInside];
    
    [viewNavBar addSubview:buttonNewNotification];
    
    [navBar addSubview:viewNavBar];
}


- (void)addArrowButtonToMapview{
    
    UIButton *buttonArrow = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonArrow.frame = CGRectMake(320/2-15,0, 30, 30);
    buttonArrow.tag =  100;
    [buttonArrow setImage:[UIImage imageNamed:@"homescreenmapview_down_arrow_off.png"] forState:UIControlStateNormal];
    [buttonArrow setImage:[UIImage imageNamed:@"homescreenmapview_down_arrow_on.png"] forState:UIControlStateHighlighted];
    [buttonArrow addTarget:self action:@selector(moveMapViewUPAndDOWN:) forControlEvents:UIControlEventTouchUpInside];
    [buttonArrow setHidden:YES];
    [customMapView addSubview:buttonArrow];
}

- (void)moveMapViewUPAndDOWN:(UIButton *)sender{
    
    sender.enabled = NO;
    CGRect frame = customMapView.frame;
    if (sender.tag == 100) {
        isMapViewOpened = YES;
        sender.tag = 200;
        frame.origin.y = 65;
        frame.origin.x = 0;
        frame.size.height = self.view.bounds.size.height - 65;
        [self customMarkers];
        
    }else{
        isMapViewOpened = NO;
        sender.tag = 100;
        frame.origin.y = self.view.bounds.size.height ;
        frame.size.height = 68;
    }
    
    
    [UIView animateWithDuration:0.4
                          delay:0.1
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^
     {
         
         
         customMapView.frame = frame;
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Completed");
         sender.enabled = YES;
         //[acceptRejectBtnView setHidden:YES];
         UIImageView *img = (UIImageView *)[self.view viewWithTag:BottomShadowImageTag];
         
         if (sender.tag == 200) {
             //sender.hidden = NO;
             img.hidden = YES;
             [sender setImage:[UIImage imageNamed:@"cross_icon_off.png"] forState:UIControlStateNormal];
             [sender setImage:[UIImage imageNamed:@"cross_icon_on.png"] forState:UIControlStateHighlighted];

         }else{
             //sender.hidden = YES;
             img.hidden = NO;
             
             [sender setImage:[UIImage imageNamed:@"doctor_location_pin_icon_off.png"] forState:UIControlStateNormal];
             [sender setImage:[UIImage imageNamed:@"doctor_location_pin_icon_on.png"] forState:UIControlStateHighlighted];
         }
         
        
     }];
}

- (void)addTransparentViewWhenProfileIsUnderVerification:(NSString *)message{
    UIView *transparentView = [self.view viewWithTag:500];
    if (!transparentView) {
        UIView *transparentView = [[UIView alloc]initWithFrame:CGRectMake(10, [UIScreen mainScreen].bounds.size.height - 60, 300, 55)];
        [transparentView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]];
        transparentView.tag = 500;
        
        // border radius
        [transparentView.layer setCornerRadius:15.0f];
        
        // border
        [transparentView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [transparentView.layer setBorderWidth:1.5f];
        
        [self.view addSubview:transparentView];
        
        
        
        UILabel *labelMessage = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 300, 44)];
        labelMessage.textAlignment = NSTextAlignmentCenter;
        labelMessage.numberOfLines = 2;
        labelMessage.tag = 100;
        [Helper setToLabel:labelMessage Text:message WithFont:HELVETICANEUE_LIGHT FSize:13 Color:WHITE_COLOR];
        [transparentView addSubview:labelMessage];
    }else{
        
        UILabel  *label = (UILabel *)[transparentView viewWithTag:100];
        label.text = message;
    }
    
}

#pragma mark -leftbar and righbar action

-(void)rightBarButtonClicked:(UIButton *)sender{
    
    
    
   // [pagerView setPage:0];
   // [self performSegueWithIdentifier:@"PendingNotification" sender:sender];
    
    
    
    [self moveMapViewUPAndDOWN:sender];
    
    
}

-(void)leftBarButtonClicked:(UIButton *)sender{
    
    if (_isNewBookingInProcess) {
        
    }
    else {
        [self menuButtonPressed:sender];
    }
    
}

#pragma mark MCPagerView Delegate -
- (void)updatePager
{
    //  NSLog(@"updatePager");
    pagerView.page = floorf(self.scrolView.contentOffset.x / self.scrolView.frame.size.width);
    
    // NSLog(@"%d",pagerView.page);
    if (pagerView.page >= 0) {
        // self.labelDateHeader.text = [Helper getCurrentDateTime:self.appointmentsArr[pagerView.page][@"date"] dateFormat:@"MM/dd/YY"];
        self.labelDateHeader.text =  self.appointmentsArr[pagerView.page][@"mmddyy"] ;
        
        self.tblView = (UITableView *)[self.scrolView viewWithTag:100+pagerView.page];
        [self.tblView reloadData];
        
        
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self updatePager];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
        NSLog(@"!decelerate");
        [self updatePager];
    }
}

- (void)pageView:(MCPagerView *)pageView didUpdateToPage:(NSInteger)newPage
{
    CGPoint offset = CGPointMake(self.scrolView.frame.size.width * pagerView.page, 0);
    [self.scrolView setContentOffset:offset animated:YES];
}


#pragma mark - Calendar Methods
//- (NSDate *)dateByAddingDays:(NSUInteger)days toDate:(NSDate *)date
//{
//    NSDateComponents *c = [NSDateComponents new];
//    [c setDay:days];
//    return [self dateByAddingComponents:c toDate:date options:0];
//}


#pragma mark - menu slider method

- (IBAction)menuButtonPressed:(id)sender
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

#pragma mark scrollView Delegate -
/*
 - (void)scrollViewDidScroll:(UIScrollView *)_scrollView
 {
 // NSLog(@"page no :%f ",(_scrollView.contentOffset.x / _scrollView.frame.size.width));
 //  NSLog(@"scrollViewDidScroll");
 
 
 }
 - (void)scrollViewDidEndDecelerating:(UIScrollView *)_scrollView
 {
 NSLog(@"scrollViewDidEndDecelerating");
 NSLog(@"page no :%f ",(_scrollView.contentOffset.x / _scrollView.frame.size.width));
 self.currentPage = (_scrollView.contentOffset.x / _scrollView.frame.size.width);
 
 if (self.currentPage < self.previousPage) {
 self.previousPage = self.currentPage;
 //  [self btnPreviousTapped];
 
 NSCalendar *calendar = [NSCalendar currentCalendar];
 if (!self.date) {
 self.date = [NSDate date];
 }
 
 self.date =[calendar dateBySubtractingDays:1 fromDate:self.date];
 NSLog(@"%@",self.date);
 NSString *bookingDate = [Helper getCurrentDateTime:self.date dateFormat:@"MM/dd/YY"];
 self.labelDateHeader.text = [NSString stringWithFormat:@"%@",bookingDate];
 
 }else if (self.currentPage > self.previousPage){
 self.previousPage = self.currentPage;
 //[self btnNextTapped];
 
 NSCalendar *calendar = [NSCalendar currentCalendar];
 if (!self.date) {
 self.date = [NSDate date];
 }
 
 self.date =[calendar dateByAddingDays:1 toDate:self.date];
 NSLog(@"%@",self.date);
 NSString *bookingDate = [Helper getCurrentDateTime:self.date dateFormat:@"MM/dd/YY"];
 self.labelDateHeader.text = [NSString stringWithFormat:@"%@",bookingDate];
 
 }
 
 }
 
 - (void)scrollViewDidEndDragging:(UIScrollView *)_scrollView willDecelerate:(BOOL)decelerate
 {
 NSLog(@"scrollViewDidEndDragging");
 
 
 }
 */




//- (void)didReceiveMemoryWarning
//{
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}

@end
