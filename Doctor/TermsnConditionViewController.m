//
//  TermsnConditionViewController.m
//  privMD
//
//  Created by Rahul Sharma on 27/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "TermsnConditionViewController.h"
#import "WebViewController.h"
@interface TermsnConditionViewController ()
@property(nonatomic,strong)IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSArray *termsOptions;
@end

@implementation TermsnConditionViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)cancelButtonClicked{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) createNavLeftButton
{
    // UIView *navView = [[UIView new]initWithFrame:CGRectMake(0, 0,50, 44)];
    
    
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];
    
    
    [Helper setButton:navCancelButton Text:@"BACK" WithFont:Robot_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navCancelButton setTitle:@"BACK" forState:UIControlStateNormal];
    [navCancelButton setTitle:@"BACK" forState:UIControlStateSelected];
    [navCancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    navCancelButton.titleLabel.font = [UIFont fontWithName:ZURICH_LIGHTCONDENSED size:14];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
    
    
   

  
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   // [self createNavLeftButton];
    
    PriveMdAppDelegate *delegate = (PriveMdAppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate customizeNavigationLeftButton:self];
    
    self.title = @"TERMS & CONDITIONS";
    
   // _termsOptions = @[@"Terms & Conditions",@"Privacy Policy",@"Security Policy",@"Medical Policy"];
    _termsOptions = @[@"Terms & Conditions",@"Privacy Policy"];
}

-(void)linkButtonClicked :(id)sender
{
    [self performSegueWithIdentifier:@"gotoWebView" sender:self];
    
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"gotoWebView"])
    {
        NSIndexPath *iPath = (NSIndexPath*)sender;
        WebViewController *webView = (WebViewController*)[segue destinationViewController];
        webView.title = _termsOptions[iPath.row];
        
        if (iPath.row == 0) {
            webView.weburl = @"http://www.privemd.com/terms_of_use.php";
        }
        else if (iPath.row == 1) {
            webView.weburl = @"http://www.privemd.com/privacy_policy.php";
        }
        else{
            webView.weburl = @"https://www.google.com/";
        }
        
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Other Methods -
- (void)buttonBackTapped{
    NSLog(@"button back taped");
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _termsOptions.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:17];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    
    cell.textLabel.text = _termsOptions[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

#pragma mark - UITableViewDataSource Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //    UITableViewCell *tableViewCell = [tableView cellForRowAtIndexPath:indexPath];
    //	[tableViewCell setSelected:NO animated:YES];
    
    [self performSegueWithIdentifier:@"gotoWebView" sender:indexPath];
    
}

@end
