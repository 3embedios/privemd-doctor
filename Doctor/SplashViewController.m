//
//  PatientViewController.m
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "SplashViewController.h"
#import "HelpViewController.h"
#import "HomeViewController.h"
#import "MenuViewController.h"


@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
   // [Helper getCurrentDateTime];
       
}


-(void)viewDidAppear:(BOOL)animated
{
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"default-568h.png"]];
    
    
    NSString *deviceId;
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
    deviceId = [oNSUUID UUIDString];
} else
    {
        deviceId = [oNSUUID UUIDString];
    
    }
    [[NSUserDefaults standardUserDefaults] setObject:deviceId forKey:kPMDDeviceIdKey];

    
    [NSTimer scheduledTimerWithTimeInterval:0.0f target:self selector:@selector(removeSplash) userInfo:Nil repeats:NO];

}


-(void)removeSplash
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken])
    {
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        //[(NSMutableArray*)self.navigationController.viewControllers removeAllObjects];
        MenuViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    }
    else
    {
        // [self performSegueWithIdentifier:@"Help" sender:self];
        HelpViewController *help = [self.storyboard instantiateViewControllerWithIdentifier:@"helpVC"];
        [[self navigationController ] pushViewController:help animated:NO];
        
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"HomeView"])
    {
        
        MenuViewController *MPC = [[MenuViewController alloc]init];
        
        MPC =[segue destinationViewController];

        
    }
    else
    {
        HelpViewController *HVC = [[HelpViewController alloc]init];
    
        HVC =[segue destinationViewController];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getDirection
{
    
}

@end
