//
//  PMDReachabilityWrapper.m
//  privMD
//
//  Created by Surender Rathore on 16/04/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "PMDReachabilityWrapper.h"
#import "Reachability.h"


@interface PMDReachabilityWrapper ()

@property (nonatomic, strong) Reachability *hostReach;
@property (nonatomic, strong) Reachability *internetReach;
@property (nonatomic, strong) Reachability *wifiReach;
@property (nonatomic, assign) int fireSelectorOnce;
@end

@implementation PMDReachabilityWrapper
@synthesize hostReach;
@synthesize internetReach;
@synthesize wifiReach;
@synthesize target;
@synthesize selector;

static PMDReachabilityWrapper *reachabilityWrapper = nil;


+(instancetype)sharedInstance
{
    if (!reachabilityWrapper) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            reachabilityWrapper = [[self alloc] init];
        });
    }
    
    return reachabilityWrapper;
}


#pragma Reachability

- (BOOL)isNetworkAvailable {
    
    return _networkStatus != NotReachable;
}

// Called by Reachability whenever status changes.

- (void)reachabilityChanged:(NSNotification* )note {
    

    
    Reachability *curReach = (Reachability *)[note object];
    
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    
    _networkStatus = [curReach currentReachabilityStatus];
    
    if (_networkStatus == NotReachable) {
        NSLog(@"Network not reachable.");
        _fireSelectorOnce = 0;
        [self addNetworkNotAvaliableView];
    }
    else {
        NSLog(@"Network Reachable");
           [self removeNetworkNotAvaliableView];
        if ([target respondsToSelector:selector]) {
            if (_fireSelectorOnce == 0) {
                [target performSelector:selector withObject:nil afterDelay:1];
                _fireSelectorOnce = 1;
                
             
            }
            
        }
        
    }
    
}

- (void)addNetworkNotAvaliableView{
    
  
    
   UILabel  *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 64, 320, 40)];
    label.text = @"Could Not Connect To Server";
    label.font = [UIFont fontWithName:@"OpenSans-Bold" size:12];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.8];
    label.textAlignment = NSTextAlignmentCenter;
    
    
    
  
    
  //UINavigationController *nav=(UINavigationController *)((PriveMdAppDelegate *)[[UIApplication sharedApplication] delegate]).window.rootViewController;
  //UIViewController *vc = [nav.viewControllers lastObject];
    
    
    
    UIWindow *window = [[[UIApplication sharedApplication]delegate] window];
    for (UIView *subView in [window subviews]) {
        if ([subView isKindOfClass:[UILabel class]]) {
            [subView removeFromSuperview];
        }
    }
    [window addSubview:label];

}

- (void)removeNetworkNotAvaliableView{
    
    UIWindow *window = [[[UIApplication sharedApplication]delegate] window];
    for (UIView *subView in [window subviews]) {
        if ([subView isKindOfClass:[UILabel class]]) {
            [subView removeFromSuperview];
        }
        
    }
}


- (void)openAnimation:(UILabel *)label{
 
    CGRect frame = label.frame;
    frame.origin.y = 64;
    label.alpha = 0.0;
    [UIView animateWithDuration:.4f
                     animations:^{
                         
                         label.frame = frame;
                         label.alpha = 1.0;
                     }
                     completion:^(BOOL finished){
                         
                     }];

}

- (void)closeAnimation{
    
}
- (void)monitorReachability {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    self.hostReach = [Reachability reachabilityWithHostName:@"www.google.com"];
    [self.hostReach startNotifier];
    
    self.internetReach = [Reachability reachabilityForInternetConnection];
    [self.internetReach startNotifier];
    
    self.wifiReach = [Reachability reachabilityForLocalWiFi];
    [self.wifiReach startNotifier];
}

@end
