//
//  RaiseInVoiceViewController.m
//  Doctor
//
//  Created by Rahul Sharma on 27/03/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import "RaiseInVoiceViewController.h"
#import "UpdateBookingStatus.h"
#import "DirectionService.h"
#import "XDKAirMenuController.h"
#import "SplashViewController.h"

@interface RaiseInVoiceViewController ()

@end

@implementation RaiseInVoiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Raise Invoice";
    [self.txtViewRaiseInvoice becomeFirstResponder];
    
    PriveMdAppDelegate *delegate = (PriveMdAppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate customizeNavigationLeftButton:self];
    
    [self addNavRightButton];
}

- (void)viewWillDisappear:(BOOL)animated{
    
    UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
    [updateBookingStatus stopUpdatingStatus];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark Other method -

- (void)backToHomeScreen{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)buttonBackTapped{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)addNavRightButton{
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    rightButton.frame = CGRectMake(0, 0, 66.5, 100);
    [rightButton setTitle:@"Done" forState:UIControlStateNormal];
    
    [rightButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
    
    [rightButton setTitleColor:BLACK_COLOR forState:UIControlStateNormal];
    
    [rightButton addTarget:self action:@selector(riaseInvoice) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = barButton;
    
}


- (void)updateDoctorStatus:(int)state{
    
    
    UpdateBookingStatus *updateBookingStatus = [UpdateBookingStatus sharedInstance];
    updateBookingStatus.driverState = state;
    updateBookingStatus.dictPatientDetails = self.dictAppointmentDetails;
    [updateBookingStatus updateToPassengerForDriverState];
    [updateBookingStatus startUpdatingStatus];
    
    
}


- (void)riaseInvoice{
    
    [[ProgressIndicator sharedInstance]showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:@"Loading.."];
    
    
    [self updateDoctorStatus:7];
    
    
    NSDictionary *parameters = @{kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId:[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 @"ent_appnt_dt":self.dictAppointmentDetails[@"apntDt"],
                                 KSMPPatientEmail:self.dictAppointmentDetails[@"email"],
                                 kSMPRespondResponse:@"7",
                                 kSMPRespondDocNotes:self.txtViewRaiseInvoice.text,
                                 @"ent_date_time":[Helper getCurrentDateTime]};
    
    
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    
    
    [httpClient postPath:MethodupdateApptStatus parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self riaseInvoiceResponse:operation.responseString.JSONValue];
        // NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        //NSLog(@"Request Successful, response '%@'", responseStr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];
    
    
}

-(void)riaseInvoiceResponse:(NSDictionary *)response
{
    
    // NSLog(@"response:%@",response);
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }else if ([[response objectForKey:@"errFlag"] intValue] == 1 && ([[response objectForKey:@"errNum"] intValue] == 6 || [[response objectForKey:@"errNum"] intValue] == 7)) {   // session token expire
        
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        
        
        [self userSessionTokenExpire];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Expired" message:@"Your session with privMD is expired. Please Login again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
        
    }
    else
    {
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
            if ([self.dictAppointmentDetails[@"status"] intValue] == 6) {
                [self backToHomeScreen];
            }
            
            
            
        }
        else
        {
            
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
    
}

-(void)userSessionTokenExpire{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    [[[XDKAirMenuController sharedMenu] view] removeFromSuperview];
    
    if ([XDKAirMenuController relese]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        SplashViewController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
    }
    
}


#pragma mark UITextView Delegate -

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]) {
        [self riaseInvoice];
        return NO;
    }
    return YES;
}
@end
