//
//  User.m
//  privMD
//
//  Created by Surender Rathore on 19/04/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "User.h"
//#import "Database.h"
#import "PMDReachabilityWrapper.h"
#import "NetworkHandler.h"
#import "TELogger.h"

@implementation User
@synthesize delegate;
@synthesize blurredImage;
@synthesize profileImage;
@synthesize name;
@synthesize email;
@synthesize phone;


static User  *user;

+ (id)sharedInstance {
	if (!user) {
		user  = [[self alloc] init];
	}
	
	return user;
}


- (void)logout
{
   
   
  //  ResKitWebService * rest = [ResKitWebService sharedInstance];
    
    NSDictionary *queryParams;
    queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                   [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                   [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                   [NSNumber numberWithInt:1],kSMPPassengerUserType,
                   [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    
    TELogInfo(@"param%@",queryParams);
    
    /*
    [rest composeRequestForLogOut:MethodLogout
                                   paramas:queryParams
                              onComplition:^(BOOL success, NSDictionary *response){
                                  
                                  if (success) { //handle success response
                                      [self userLogoutResponse:(NSArray*)response];
                                  }
                                  else{//error
                                      
                                  }
                              }];
    
*/
    
    NSURL *url = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient postPath:MethodLogout parameters:queryParams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self userLogoutResponse:operation.responseString.JSONValue];
        // NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        //NSLog(@"Request Successful, response '%@'", responseStr);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];

    
}



- (void) userLogoutResponse:(NSDictionary *)response
{
    
   // TELogInfo(@"_response%@",response);
    
    
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    
    if ([[response objectForKey:@"errFlag"] intValue] == 0){

        if (delegate && [delegate respondsToSelector:@selector(userDidLogoutSucessfully:)]) {
            [delegate userDidLogoutSucessfully:YES];
           
        }
        
        
    }
    else if ([[response objectForKey:@"errFlag"] intValue] ==1)
    {
        if (delegate && [delegate respondsToSelector:@selector(userDidLogoutSucessfully:)]) {
            [delegate userDidLogoutSucessfully:YES];
        }
        
    }

    else
    {
        [Helper showAlertWithTitle:@"Message" Message:[response objectForKey:@"errMsg"]];
    }
    

    
   }


-(void)deleteUserSavedData{
    
    //delete all saved cards
    //[Database deleteAllCard];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
