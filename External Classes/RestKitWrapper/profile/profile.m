//
//  profile.m
//  Roadyo
//
//  Created by Rahul Sharma on 09/05/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "profile.h"

@implementation profile
@synthesize fName;
@synthesize lName;
@synthesize email;
@synthesize type;
@synthesize mobile;
@synthesize status;
@synthesize about;
@synthesize zip;
@synthesize pPic;
@synthesize expertise;
@synthesize licNo;
@synthesize licExp;
@synthesize avgRate;
@synthesize totRats;
@synthesize rejApts;
@synthesize cmpltApts;

@end
