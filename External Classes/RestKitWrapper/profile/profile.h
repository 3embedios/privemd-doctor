//
//  profile.h
//  Roadyo
//
//  Created by Rahul Sharma on 09/05/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface profile : NSObject
@property (nonatomic, strong) NSString *fName;
@property (nonatomic, strong) NSString *lName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *about;
@property (nonatomic, strong) NSString *zip;
@property (nonatomic, strong) NSString *pPic;
@property (nonatomic, strong) NSString *expertise;
@property (nonatomic, strong) NSString *licNo;
@property (nonatomic, strong) NSString *licExp;
@property (nonatomic, strong) NSString *avgRate;
@property (nonatomic, strong) NSString *totRats;
@property (nonatomic, strong) NSString *rejApts;
@property (nonatomic, strong) NSString *cmpltApts;

@end
