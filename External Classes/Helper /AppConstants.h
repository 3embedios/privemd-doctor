//
//  AppConstants.h
//  privMD
//
//  Created by Surender Rathore on 22/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//




#pragma mark - enums

typedef enum {
    
    kNotificationTypeBookingAccept1 = 2,
    kNotificationTypeBookingReject1 = 3,
    kNotificationTypeBookingOnTheWay = 5,
    kNotificationTypeBookingArrived = 6,
    kNotificationTypeBookingDropped = 7
    
}BookingNotificationType;

//#define BASE_IP @"http://107.170.105.66/app/"
#define BASE_IP @"http://www.privemd.com/services_v3.php/"

typedef enum {
    kPubNubStartStreamAction = 1,
    kPubNubStopStreamAction = 2,
    kPubNubUpdatePatientAppointmentLocationAction = 3,
    kPubNubStartUploadLocation = 4,
    kPubNubStartDoctorLocationStreamAction = 5,
    kPubNubStopDoctorLocationStreamAction  = 6
}PubNubStreamAction;




typedef enum {
    kAppointmentTypeNow = 3,
    kAppointmentTypeLater = 4
}AppointmentType;

typedef enum {
    kMedicalSpecialistDoctor = 1,
    kMedicalSpecialistNurse = 2
}MedicalSpecialistType;


typedef enum {
    kResponseFlagSuccess = 0,
    kResponseFlagFailure = 1
}ResponseErrorFlagCodes;

typedef enum {
    kResponseErrorCode = 0,
   
}ResponseErrorCode;

#pragma mark - Constants
//eg: give prifix kPMD

extern NSString *const kPMDPublishStreamChannel;
extern NSString *const kPMDPubNubPublisherKey;
extern NSString *const kPMDPubNubSubcriptionKey;
extern NSString *const kPMDGoogleMapsAPIKey;
extern NSString *const kPMDCrashLyticsAPIKey;

extern NSString *const kPMDTestDeviceidKey;
extern NSString *const kPMDDeviceIdKey;

#pragma mark - mark URLs

//Base URL
extern NSString *BASE_URL_RESTKIT;
extern NSString *BASE_URL;
extern NSString *const   baseUrlForXXHDPIImage;
extern NSString *const   baseUrlForOriginalImage;
extern NSString *const   baseUrlForThumbnailImage;
extern NSString *const   baseUrlForUploadImage;


#pragma mark - ServiceMethods
// eg : prifix kSM
extern NSString *const kSMLiveBooking;
extern NSString *const kSMGetAppointmentDetial;
extern NSString *const kSMUpdateSlaveReview;
extern NSString *const kSMGetMasters ;



//Request Params For Logout the user

extern NSString *KDALogoutSessionToken;
extern NSString *KDALogoutUserId;
extern NSString *KDALogoutDateTime;

//Parsms for checking user loged out or not

extern NSString *KDAcheckUserLogedOut;
extern NSString *KDAcheckUserSessionToken;
extern NSString *KDAgetPushToken;

//Params to store the Country & City.

extern NSString *KDACountry;
extern NSString *KDACity;
extern NSString *KDALatitude;
extern NSString *KDALongitude;

//params for firstname
extern NSString *KDAFirstName;
extern NSString *KDALastName;
extern NSString *KDAEmail;
extern NSString *KDAPhoneNo;
extern NSString *KDAPassword;




#pragma mark - NSUserDeafults Keys
//eg : give prefix kNSU

extern NSString *const kNSURoadyoPubNubChannelkey;
extern NSString *const kNSUAppoinmentDoctorDetialKey;
extern NSString *const kNSUDoctorEmailAddressKey;
extern NSString *const kNSUDoctorProfilePicKey;
extern NSString *const kNSUMongoDataBaseAPIKey;
extern NSString *const kNSUDoctorNameKey;
extern NSString *const kNSUPatientPubNubChannelkey;
extern NSString *const kNSUDoctorPhonekey;
extern NSString *const kNSUDoctorTypekey;
extern NSString *const kNSUDoctorSubscribeChanelKey;
extern NSString *const kNSUDoctorServerChanelKey ;

#pragma mark - PushNotification Payload Keys
//eg : give prefix kPN
extern NSString *const kPNPayloadDoctorNameKey;
extern NSString *const kPNPayloadAppoinmentTimeKey;
extern NSString *const kPNPayloadDistanceKey;
extern NSString *const kPNPayloadEstimatedTimeKey;
extern NSString *const kPNPayloadDoctorEmailKey;
extern NSString *const kPNPayloadDoctorContactNumberKey;
extern NSString *const kPNPayloadProfilePictureUrlKey;
extern NSString *const kPNPayloadAppoinmentDateStringKey;
extern NSString *const kPNPayloadAppoinmentLatitudeKey;

#pragma mark - Controller Keys

#pragma mark - Notification Name keys
extern NSString *const kNotificationNewCardAddedNameKey;
extern NSString *const kNotificationCardDeletedNameKey;

#pragma mark - Network Error
extern NSString *const kNetworkErrormessage;
